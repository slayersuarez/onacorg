<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" >
<link rel="stylesheet" type="text/css" media="screen" href="css/screen.css" />
<link rel="stylesheet" href="js/validationEngine.jquery.css" type="text/css"/>
 <!--Para validar el Formulario-->
	
	<script src="js/jquery-1.7.2.min.js" type="text/javascript"></script>
	<script src="js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
	<script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
 
 <script>
		jQuery(document).ready(function(){
			// binds form submission and fields to the validation engine
			jQuery("#formID").validationEngine();
		});

		/**
		*
		* @param {jqObject} the field where the validation applies
		* @param {Array[String]} validation rules for this field
		* @param {int} rule index
		* @param {Map} form options
		* @return an error string if validation failed
		*/
		function checkHELLO(field, rules, i, options){
			if (field.val() != "HELLO") {
				// this allows to use i18 for the error msgs
				return options.allrules.validate2fields.alertText;
			}
		}
		</script>

<style>
body{ background-color:none; background-color:transparent;}
* { font-family: 'Oswald', sans-serif;}

.error { color:#C00; }

em { color:#C00; }

label {}

input[type='text'] { margin:2px 0 0 5px; color:#d5d3c7; float:left; width:190px; background-color:transparent;font-family: 'Oswald', sans-serif; font-size:15px; font-weight:normal; border:none;}

input[type='submit'] { width:108px; height:60px;background-image:url(../images/btn_enviar.png); background-repeat:no-repeat; background-position:top center; cursor:pointer; border:none; background-color:transparent; margin-top:-9px;}

input[type='reset'] { width:108px; height:60px;background-image:url(../images/btn_borrar.png); background-repeat:no-repeat; background-position:top center; cursor:pointer; border:none; background-color:transparent; margin-top:-9px;}

textarea { width:214px; height:93px; font-family: 'Oswald', sans-serif; font-size:12px; font-weight:normal; margin-left:5px; color:#d5d3c7; background:none; border:none;}

#commentForm { width:404px; }

#commentForm label {}

.bots {float:right; margin-right:27px;}

#commentForm label.error { margin-left: 6px; }

#cuadro {background-color:#f2f2f2; width:74px; height:21px; float:left; font-family:Calibri; font-weight:normal; font-size:14px; color:#0076C1; padding-left:15px;  border:1px solid #C0C1C7; margin-right:12px;}

#cuadrog {background-color:#f2f2f2; width:74px; height:62px; float:left; font-family:Calibri; font-weight:normal; font-size:14px; color:#0076C1; padding-left:15px;  border:1px solid #C0C1C7; margin-right:12px;}

select{width:184px; height:auto;}

.disabled input[disabled="disabled"] { background-color:#F2F2F2;}

.disabled textarea[disabled="disabled"] { background-color:#F2F2F2;}

</style>
<script>
	//function activar_form(){
	//	if(document.getElementById("check").checked)
//		{
//			
//			document.getElementById("cname").disabled =true;
//			document.getElementById("cempresa").disabled =true;
//			document.getElementById("ctelefono").disabled =true;
//			document.getElementById("cciudad").disabled =true;
//			document.getElementById("cemail").disabled =true;
//			document.getElementById("asunto").disabled =true;
//			document.getElementById("mensaje").disabled =true;
//			document.getElementById("btn").disabled =true
//		}
//		else
//			document.getElementById("cname").disabled =false;
//			document.getElementById("cempresa").disabled =false;
//			document.getElementById("ctelefono").disabled =false;
//			document.getElementById("cciudad").disabled =false;
//			document.getElementById("cemail").disabled =false;
//			document.getElementById("asunto").disabled =false;
//			document.getElementById("mensaje").disabled =false;
//			document.getElementById("btn").disabled =false
//	}
</script>
</head>

<body><br />
 <form id="formID" name="contacto1" method="post" action="procesar.php">
  <table style="font-size: 15px; color: #d5d3c7; font-family: 'Oswald',sans-serif; width: 322px; height: 314px; margin-top:3px;">
  <tbody>
  <tr>
  <td height="24" width="51">Nombre</td>
  <td width="69"></td>
  <td colspan="2" style="background-color:#1F201F; border-radius: 5px;">
    <input name="nombre" type="text" id="nom" class="validate[required] text-input"  minlength="2" width="175"/>
  </td>
  </tr>
  <tr>
  <td colspan="5" height="10"></td>
  </tr>
  <tr>
  <td height="24">E-mail;</td>
  <td></td>
  <td colspan="2" style="background-color:#1F201F; border-radius: 5px;">
    <input name="email" type="text" id="mail" class="validate[required,custom[email]] text-input" minlength="2" width="175"/>
  </td>
  </tr>
  <tr>
  <td colspan="5" height="10"></td>
  </tr>
  <tr>
  <td height="24">Teléfono</td>
  <td></td>
  <td colspan="2" style="background-color:#1F201F; border-radius: 5px;">
    <input name="telefono" type="text" id="tel" class="validate[required] text-input"  minlength="2" width="175"/>
  </td>
  </tr>
  <tr>
  <td colspan="4" height="10"></td>
  </tr>
  <tr>
  <td height="24">Ciudad</td>
  <td></td>
  <td colspan="2" style="background-color:#1F201F; border-radius: 5px;">
    <input name="ciudad" type="text" id="ciu" class="validate[required] text-input"  minlength="2" width="175"/>
  </td>
  </tr>
  <tr>
  <td colspan="5" height="10"></td>
  </tr>
  <tr>
  <td height="93" valign="top">Mensaje</td>
  <td></td>
  <td colspan="2" valign="top" style="background-color:#1F201F; border-radius: 5px;">
    <textarea cols="23" name="mensaje"></textarea>
  </td>
  </tr>
  <tr>
  <td colspan="2"></td>
  <td align="center" valign="top">
  <input id="btn" type="submit" value="" name="Submit"/>
 </td>
  <td align="center" valign="top"><input type="reset" name="Submit" value="" onclick="activar_form()" /></td>
  </tr>
  </tbody>
  </table>
  </form>

</body>
</html>
