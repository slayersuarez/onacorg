<?php
 /**
 * @package		logTrazabilidad Integration
 * @subpackage	com_logsessions
 * @copyright	Copyright (C) 2014 Sainet Ingenieria Ltda. All rights reserved.
 * @license		License Sainet Ingenieria Ltda.
 * @author 		Sainet Ingenieria Ltda.
 * @author 		Desarrollador : Robison Perdomo Lugo
 * @link		http://www.creandopaginasweb.com
 */

defined( '_JEXEC' ) or die( 'Restricted access' );

require_once( JPATH_COMPONENT . DS . 'helpers' . DS .  'style.php' );
require_once( JPATH_COMPONENT . DS . 'helpers' . DS . 'logtrazabilidad.php');
// require_once( JPATH_COMPONENT . DS . 'helpers' . DS . 'paginador.php');
require_once( JPATH_COMPONENT . DS . 'helpers' . DS . 'misc.php');
require_once( JPATH_COMPONENT . DS . 'controller.php' );

//Load styles and javascripts
LogTrazabilidadHelperStyle::load();

// Access check.
if (!JFactory::getUser()->authorise('core.admin', 'com_logtrazabilidad')) {
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}

$controller = JControllerLegacy::getInstance('logtrazabilidad');
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();
// No closing tag