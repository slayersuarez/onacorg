<?php
/**
 * @copyright	Copyright (C) 2013 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * configuracion pqrs Controller
 *
 * @package		Joomla.Administrator
 * @subpackage	com_pqrs
 */
class LogTrazabilidadControllerLogAcredita extends JControllerLegacy {
	

	/**
	 * Get a list of filter options for the blocked state of a user.
	 *
	 * @return  array  An array of JHtmlOption elements.
	 */
	public function getNamesUser(){
		
		$Utilities = 'Misc';
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );
		$dataName = JRequest::getVar( 'q' );
		$response = array();

		$query->select( 'b.name' );
		$query->from( $db->quoteName( 'log_carga_organismos' ).' AS a' );
		$query->innerJoin( $db->quoteName( '#__users' ).' AS b ON a.id_usuario = b.id' );
		$query->where( $db->quoteName( 'b.name' ) .' LIKE '. $db->quote('%'. $db->escape( $dataName ) .'%') );
		$query->group( 'a.id_usuario' );

		$db->setQuery( $query );
		$results = $db->loadAssocList();

		foreach ( $results as $key => $value) {
			
			$response[] = $value[ 'name' ];
		}

		if( count($response) <= 0 )$response[] = '';

		if( $Utilities::isAjax() ){

			echo json_encode( $response );
			die;
		}

		return $response;
	}

	/**
	 * Get a list of filter options for the blocked state of a user.
	 *
	 * @return  array  An array of JHtmlOption elements.
	 */
	public function getDescription(){
		
		$Utilities = 'Misc';
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );
		$dataDescription = JRequest::getVar( 'q' );
		$response = array();

		$query->select( 'a.descripcion_actividad' );
		$query->from( $db->quoteName( 'log_carga_organismos' ).' AS a' );
		$query->where( $db->quoteName( 'a.descripcion_actividad' ) .' LIKE '. $db->quote('%'. $db->escape( $dataDescription ) .'%') );
		$query->group( 'a.descripcion_actividad' );

		$db->setQuery( $query );
		$results = $db->loadAssocList();

		foreach ( $results as $key => $value) {
			
			$descripcion = preg_replace('/(\n|\r|\n\r|\t|\0|\x0B)/i', '', $value[ 'descripcion_actividad' ]);
			$descripcion = trim($descripcion);	
			$response[] = $descripcion;
		}

		if( count($response) <= 0 )$response[] = '';

		if( $Utilities::isAjax() ){

			echo json_encode( $response );
			die;
		}

		return $response;
	}

	/**
	* Exporta el layout exportacsv de la vista logsession a archivo de hoja de calculo .csv
	*/
	public function exportaCsv(){

		$model =& $this->getModel( 'logacredita' ); 
		$view =& $this->getView( 'logacredita' , 'html');

		header('Content-type: application/vnd.ms-excel; name="excel"');
		header("Content-Disposition: attachment; filename=logacredita_actividad_".time().".xls");
		header("Pragma: no-cache");
		header("Expires: 0");

		JRequest::setVar( 'start', 0 );
		JRequest::setVar( 'limit', 0 );
		/*$model->setState( 'list.start', 0);
		$model->setState( 'list.limit', 0);*/

		$view->setModel( $model, true );
		$view->setLayout( 'exportacsv' );
		$view->display();

		return;
	}

}