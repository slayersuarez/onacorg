<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );

//error_reporting(E_ALL);
//set_time_limit(0);

ini_set('memory_limit','1024M');
ini_set('post_max_size','36M');
ini_set('upload_max_filesize','36M');
ini_set('max_execution_time','300'); //300 seconds = 5 minutes
ini_set('max_input_time','30');

//establecer la zona horaria
date_default_timezone_set('America/Bogota');

// Begining of the controller
class DirectorioControllerOrganismo extends JControllerLegacy{


		protected $type;

		protected $type_organismos = array(

				'Lab1' => 'Laboratorios de Ensayo'
			,	'Lac1' => 'Laboratorios de Calibración'
			,	'Lcl' => 'Laboratorios Clínico'
			,	'Cda' => 'Centros de Diagnóstico Automotor'
			,	'Ocp' => 'Organismos de Certificación de Personas'
			,	'Cpr' => 'Organismos de Certificación de Producto'
			,	'Csg' => 'Organismos de Certificación de Sistemas de Gestión'
			,	'Oin' => 'Organismos de Inspección'
			,	'Cepplus' => 'Centros de Reconocimiento de Conductores'
			,	'Cdf' => 'Organismos de Certificación de Documentos y Firmas'
		);


		/**
		* Searchs by key
		*
		*/
		public function createTables(){

			$model = $this->getModel( 'organismo' );
			$model_load = $this->getModel( 'CargaOrganismo' ); 

			$args = array(
					'model_lab1' => $this->getModel( 'Lab1' )
				,	'model_lac1' => $this->getModel( 'Lac1' )
				,	'model_lcl' => $this->getModel( 'Lcl' )
				,	'model_cda' => $this->getModel( 'Cda' )
				,	'model_ocp' => $this->getModel( 'Ocp' )
				,	'model_cpr' => $this->getModel( 'Cpr' )
				,	'model_csg' => $this->getModel( 'Csg' )
				,	'model_oin' => $this->getModel( 'Oin' )
				,	'model_cepplus' => $this->getModel( 'Cepplus' )
				,	'model_cdf' => $this->getModel( 'Cdf' )
			);

			//$model->instance( $args );
		}

		/**
		* Gets the params from organismo selected
		*
		*/
		public function getOrganismoParams( $model = NULL, $format = 'json' ){

			$response = ( object ) array();

			if( $model == NULL )
				$model = JRequest::getVar( 'model' );


			$_model = $this->getModel( $model );

			if( $format == 'json' ){

				$response->model = $_model;
				$response->params = $_model->getParametros();
				$response->natParams = $_model->natural_names;

				echo json_encode( $response );
				die();
			}
		}

		/**
		* Gets the cities
		*
		*/
		public function getCities(){

			$model_lab1 = $this->getModel( 'Lab1' );
			$model_lac1 = $this->getModel( 'Lac1' );
			$model_cda = $this->getModel( 'Cda' );
			$model_cdf = $this->getModel( 'Cdf' );
			$model_cepplus = $this->getModel( 'Cepplus' );
			$model_cpr = $this->getModel( 'Cpr' );
			$model_csg = $this->getModel( 'Csg' );
			$model_lcl = $this->getModel( 'Lcl' );
			$model_ocp = $this->getModel( 'Ocp' );
			$model_oin = $this->getModel( 'Oin' );

			$all_model_cities = array(

				0 => $model_lab1->getCities(),
				1 => $model_lac1->getCities(),
				2 => $model_cda->getCities(),
				3 => $model_cdf->getCities(),
				4 => $model_cepplus->getCities(),
				5 => $model_cpr->getCities(),
				6 => $model_csg->getCities(),
				7 => $model_lcl->getCities(),
				8 => $model_ocp->getCities(),
				9 => $model_oin->getCities()
			);

			$cities = array();

			// get the attributes map of the models
			foreach ( $all_model_cities as $key => $array_cities ) {

				$cities = array_merge( $cities, $array_cities );
			}

			$cities = array_unique( $cities );

			return $cities;
		}


		/**
		* Search terms by criteria in specific model or any
		*
		*/
		public function search(){

			// Initialize
			$response = ( object )array();

			$criteria = JRequest::getVar( 'criteria' );

			// search through specific model
			if( $criteria['tipo-organismo'] != '' ){

				$model = $this->getModel( $criteria[ 'tipo-organismo' ] );

				// construct the wheres
				$filter = $this->buildWheres( $model, $criteria );

				$response->wheres = $filter->wheres;

				// search into razon_social with RegExp so the query will be exactly
				$results = $model->getObjects( $filter->wheres );

				$response->noFilterResponse = $results;
				$response->black_list = $filter->black_list;

				// Match in black list
				foreach ( $filter->black_list as $key => $term ) {

					$_results = $this->match( $term, $criteria[ $term ], $results );
					$results = array_merge( $results, $_results );
				}

				$response->status = 200;
				$response->message = 'Búsqueda finalizada.';
				$response->results = $results;

				echo json_encode( $response );
				die();

			} else {

				// search through all models
				$results = array();

				$model_lab1 = $this->getModel( 'Lab1' );
				$model_lac1 = $this->getModel( 'Lac1' );
				$model_cda = $this->getModel( 'Cda' );
				$model_cdf = $this->getModel( 'Cdf' );
				$model_cepplus = $this->getModel( 'Cepplus' );
				$model_cpr = $this->getModel( 'Cpr' );
				$model_csg = $this->getModel( 'Csg' );
				$model_lcl = $this->getModel( 'Lcl' );
				$model_ocp = $this->getModel( 'Ocp' );
				$model_oin = $this->getModel( 'Oin' );

				$all_model_results = array(

					0 => $this->getObjects( $model_lab1, $criteria ),
					1 => $this->getObjects( $model_lac1, $criteria ),
					2 => $this->getObjects( $model_cda, $criteria ),
					3 => $this->getObjects( $model_cdf, $criteria ),
					4 => $this->getObjects( $model_cepplus, $criteria ),
					5 => $this->getObjects( $model_cpr, $criteria ),
					6 => $this->getObjects(  $model_csg, $criteria ),
					7 => $this->getObjects( $model_lcl, $criteria ),
					8 => $this->getObjects( $model_ocp, $criteria ),
					9 => $this->getObjects( $model_oin, $criteria )
				);

				// get the attributes map of the models
				foreach ( $all_model_results as $key => $array_results ) {

					$results = array_merge( $results, $array_results );
				}

				$results = array_unique( $results );

				$response->status = 200;
				$response->message = 'Búsqueda finalizada.';
				$response->results = $results;

				echo json_encode( $response );
				die();

			}

			$response->status = 500;
			$response->message = 'Parámetros en la búsqueda no válidos.';
			echo json_encode( $response );
			die();
		}

		/**
		* Match and string in key and return the array of results
		*
		* @param { string } the key of the array to look for
		* @return { array } the array filtered
		*
		*/
		public function match( $key = NULL, $term = NULL, $list = NULL ){

			if( is_string( $key ) == false || is_string( $term ) == false || is_array( $list ) == false ){
				return array();
			}

			$results = array();

			foreach ( $list  as $_key => $object ) {

				if( $term != '' ){
					if ( preg_match("/" . $term . "/i", $object->$key ) ) {
				    	array_push( $results, $object );
					}
				}
			}

			return $results;
			
		}

		/**
		* Build the wheres clausules to condition the results
		*
		* @param { stdclass } model to filter
		* @param { array } criteria
		* @return { object } black list and wheres clausules
		*/
		public function buildWheres( $model = NULL, $criteria = NULL ){

			if( $model == NULL || $criteria == NULL )
				return array();


			// Params must be query by match, create a black list
			$black_list = $model->parametros;

			array_push( $black_list, 'razon_social' );

			$wheres = array();

			foreach ( $criteria as $key => $clausule ) {
				
				if( $key != 'tipo-organismo' and $clausule != '' ){

					if( ! in_array( $key, $black_list ) ){
						
						$condition = ( object ) array();
						$condition->key = $key;
						$condition->value = '"' . $clausule . '"';
						$condition->condition = '=';
						$condition->glue = 'AND';
						array_push( $wheres, $condition );
					}
				}
			}

			$filter = ( object )array();
			$filter->wheres = $wheres;
			$filter->black_list = $black_list;

			return $filter;

		}

		/**
		* Gets the results filtered by model
		*
		*/
		public function getObjects( $model, $criteria ){

			// construct the wheres
			$filter = $this->buildWheres( $model, $criteria );

			// search into razon_social with RegExp so the query will be exactly
			$results = $model->getObjects( $filter->wheres );

			// Match in black list
			foreach ( $filter->black_list as $key => $term ) {
				
				$result = $this->match( $term, $criteria[ $term ], $results );
				$results = array_merge( $results, $result );
				$results = array_unique( $results );
			}

			return $results;
		}

		/**
		*
		* Uploads and parse and excel file
		*
		*/
		public function uploadExcel(){

			// list of valid extensions, ex. array("jpeg", "xml", "bmp")
			$allowedExtensions = array( 'xlsx', 'xls', 'ods' );
			// max file size in bytes
			$sizeLimit = 2 * 1024 * 1024;

			$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);

			// Call handleUpload() with the name of the folder, relative to PHP's getcwd()
			$result = $uploader->handleUpload('../excels/');

			// to pass data through iframe you will need to encode all html tags
			echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);

			die();
		}

		/**
		*
		* Uploads and compare the pdf file
		*
		*/
		public function uploadPDF(){

			// list of valid extensions, ex. array("jpeg", "xml", "bmp")
			$allowedExtensions = array( 'pdf' );
			$type = JRequest::getVar( 'type' );

			$type = strtoupper( $type );

			// max file size in bytes
			$sizeLimit = 2 * 1024 * 1024;

			$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);

			// Call handleUpload() with the name of the folder, relative to PHP's getcwd()
			//$result = $uploader->handleUpload('../pdfs/' . $type .'/' );
			$result = $uploader->handleUpload( '../pdfs/tmp/' );
			$result->type = $type;
			//$result->path = '../pdfs/' . JRequest::getVar( 'type' ) .'/';

			// to pass data through iframe you will need to encode all html tags
			echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);

			die();
		}

		/**
		* Parse and excel file and save it into database
		*
		*/
		public function parseExcel(){

			$data = JRequest::getVar( 'data' );
			$response = ( object )array();
			$response->log = '';

			$response->status = 200;
			$response->message = 'Controlador Alcanzado';
			$response->sent = $data;

			$inputFileName = '../excels/' . $data['excel'];
			// // get the file uploaded
			if( ! file_exists($inputFileName) ){

				$response->status = 500;
				$response->log .= 'Archivo no encontrado, controller organismo linea 331.<br>';
				$response->message = 'No se puede acceder al archivo que has subido.';

				echo json_encode( $response );
				die();
			}

			$response->log .= 'Archivo encontrado, dispuesto a parsearse...<br>';
			$this->type = $data[ 'type' ];
			

			// Parse the excel file			
			try {
				$file = PHPExcel_IOFactory::load( $inputFileName );

			} catch(Exception $e) {

				$response->status = 500;
				$response->excel = $data['excel'];
				$response->message = 'Error loading file '.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage();
				
				// delete the file
				unlink( $inputFileName );
				
				echo json_encode( $response );
				die();
			}
			
			$sheetData = $file->getActiveSheet()->toArray(null,true,true,true);

			$array_cambios = $this->comparaInfoCargada( $sheetData, $data['type'] );	

			$response = $this->save( $sheetData, $data['type'], $response, $array_cambios );
			$response->excel = $data['excel'];

			// delete the file
			unlink( $inputFileName );

			echo json_encode( $response );
			die();

		}


		/**
		*
		* Saves a style sheet in the db
		*/
		public function save( $data, $type, $response, $cambios ){

			if( ! is_array( $data ) )
				return false;

			// Instance the model
			$model = $this->getModel( $type );

			// Delete the null cells
			$sheet = array();

			$sheet = $data;

			$sheet[ 1 ] = array_filter( $sheet[ 1 ] );

			// Ensure the model has the same number of attributes than data keys
			$model_keys_count = count( $model->attrs_map ) - 1; // less the id
			$sheet_keys_count = count( $sheet[1] );

			$response->modelCount = $model_keys_count;
			$response->sheetCount = $sheet_keys_count;

			if( $model_keys_count != $sheet_keys_count ){

				$response->status = 500;
				$response->message = 'El archivo no se pudo convertir debido a que no coincide con el tipo de organismo seleccionado. Asegúrese que el archivo tiene el formato correcto, el mismo número de columnas y que el tipo de organismo seleccionado sea el correcto.';

				return $response;
			}

		
			$rows = array();

			if( $this->truncate($type) )
			{
				// set the args for each row and save in db
				for( $i = 2; $i <= count( $sheet ); $i++ ){ 

					$row = $sheet[ $i ];

					$count = 1;

					$args = array();

					foreach ( $row as $key => $value ) {
						
						$value = preg_replace('/(\n|\r|\n\r|\t|\0|\x0B)/i', ' ', $value);
						$value = trim($value);
						$args[ $model->attrs_map[ $count ] ] = $value;

						$count++;
					}
					unset($value);

					// Instance the model
					$_model = $this->getModel( $type );
					$_model->instance( $args );

					// save row
					if( ! $_model->save( 'bool' ) )
							continue;
						
					$response->log .= 'Fila ' . $i . 'Guardada correctamente';								
				}

				//se almacena el registro de actividad de la información

				if( !$this->registraActividad( $cambios, $type ) ){
					$response->status = 500;
					$response->message = "Se insertaron los registros en la base de datos, pero hubo un error al registrar los cambios que se realizaron en la información.";

					return $response;
				}


				$response->status = 200;
				$response->message = "Archivo convertido y guardado correctamente.";

				return $response;
			}
			else{

				$response->status = 500;
				$response->message = "No se pudo borrar los datos de la base de datos para cargar los nuevos registros. Intentelo nuevamente.";

				return $response;
			}

		}

		/**
		* Truncate the table
		*
		*/
		public function truncate( $type = NULL ){

			$response = ( object )array();
			$called_for_ajax = false;

			if( is_null($type) ){

				$type = JRequest::getVar( 'data' );
				$type = $type['type'];
				$called_for_ajax = true;
			}

			$model = $this->getModel( $type );

			if( $model->truncate() !== false ){

				$response->status = 200;
				$response->message = "Datos borrados correctamente.";
				$response->return = true;

			}else{

				$response->status = 500;
				$response->message = "No se pudo truncar la tabla del modelo, ".$type ;
				$response->return = false;
			}
	
			// delete pdf folder
			//system('/bin/rm -rf ' . escapeshellarg( '../pdfs/' . strtoupper( $type ) ));

			mkdir( '../pdfs/' . strtoupper( $type ) );

			if( !$called_for_ajax ){
				
				return $response->return;
			}

			echo json_encode( $response );
			die();
		}

		/**
		* Deletes all temporary excel files
		*
		*/
		public function deleteTemp(){

			$response = (object)array();

			// delete excels folder
			system('/bin/rm -rf ' . escapeshellarg( '../excels' ) );

			// re create excels folder
			mkdir( '../excels' );

			$response->status = 200;
			$response->message = "Archivos temporales borrados correctamente.";

			echo json_encode( $response );
			die();	
		}


		/**
		* Compares the pdf name in the model specified
		*
		*/
		public function comparePDF(){

			// Initialize
			$response = ( object )array();
			$data = JRequest::getVar( 'data' );
			$matches = array();
			$overwritten = false;
			$isNew = false;
			$removedPdfs = array();

			// get the right model
			$model = $this->getModel( $data[ 'type' ] );

			// Get the objects
			$objects = $model->getObjects();
			
			//comprobar que archivo coincide con registros del organismo
			$matches = $this->isInvalidPdf( $objects, $data );
			//comprobar si el archivo sobreescribe el anterior
			$overwritten = $this->isOverwrittenPdf( $data );
			//comprobar si el arcivo es nuevo
			$isNew = $this->isNewPdf( $data );
			//comprobar que los Pdfs del directorio del organismo son los mismos que se cargan
			$this->removedPdf( $data );


			if( !count($matches) ){

				$response->status = 500;
				$response->message = 'Este archivo no se encontró en los registros.';

				echo json_encode( $response );
				die();
			}

			if( $overwritten ){

				$response->status = 200;
				$response->message = 'Sobrescribio el archivo existente.';

				echo json_encode( $response );				
				die();
			}

			if( $isNew ){

				$response->status = 200;
				$response->message = 'Archivo nuevo, asociado correctamente.';

				echo json_encode( $response );				
				die();
			}

			//
			
			$response->status = 200;
			$response->message = 'Archivo asociado correctamente.';

			echo json_encode( $response );
			die();

		}

		/**
		* compara la información del archivo excel con los registros de la base de datos
		* @param { array } list register excel
		* @param { string } model type string
		*
		*/
		private function comparaInfoCargada( $sheetData, $type ){

			//initialize
			$model = $this->getModel( $type );
			$identificadores = array();
			$reg_nuevos = array();		
			$reg_editados = array();
			$reg_borrados = array();

			$estaCol = preg_grep( '/^identificador$/i' , $sheetData[1] );
			$letterCol = key($estaCol);

			foreach ( $sheetData as $idx => $registro ) {
				
				if( $idx == 1 )
					continue;	
									
				$identificadores[ $registro[ $letterCol ] ] = $registro;
			}
			unset( $registro );
			$count_regexcel = count($identificadores);

			$allRegisters = $model->getObjects();
			$count_allReg = count($allRegisters);

			//TIPOS DE CASOS

			//si se añade registros nuevos
			$reg_nuevos = $this->newRegisters( $allRegisters, $identificadores );

			//si se elimino registros
			$reg_borrados = $this->wasDeleted( $allRegisters, $identificadores );

			//si se edito registros
			$reg_editados = $this->wasEdited( $allRegisters, $identificadores );

			return array( 'nuevos' => $reg_nuevos, 'borrados' => $reg_borrados, 'editados' => $reg_editados );

		}

		/**
		*	 	
		* comprobar los registros que son nuevos y se retornan en un array
		* 
		* @param { array } array de objectos de la consulta $allreg
		* @param { array } array de los registros que se cargan del excel
		* @return { array } 
		*/
		private function newRegisters( $allreg, $identificadores ){

			$nuevos = array();

			if( !is_array($allreg) || !is_array($identificadores) )
				return $nuevos;

			foreach ( $identificadores as $idx => $registro ) {

				$countEsta = 0;

				foreach ( $allreg as $key => $obj ) {

					if( preg_match('/^'.preg_quote($idx,'/').'$/i', $obj->identificador) ){

						$countEsta++;
					}
				}	
				unset($obj);

				if( $countEsta <= 0 )
					$nuevos[ $idx ] = $registro;
			}
			

			return $nuevos;
		}

		/**
		*	 	
		* comprobar los registros que fueron borrados y se retornan en un array
		* 
		* @param { array } array de objectos de la consulta $allreg
		* @param { array } array de los registros que se cargan del excel
		* @return { array } 
		*/
		private function wasDeleted( $allreg, $identificadores ){

			$borrados = array();
			$ids_array = array();	

			if( !is_array($allreg) || !is_array($identificadores) )
				return $borrados;

			$ids_array = array_keys($identificadores);

			foreach ( $allreg as $key => $obj ) {	

				$coinciden = preg_grep('/^'.preg_quote($obj->identificador,'/').'$/i', $ids_array);
				if( count($coinciden) <= 0 )
					$borrados[ $obj->identificador ] = $obj;					
			}

			return $borrados;
		}

		/**
		*	 	
		* comprobar los registros que fueron borrados y se retornan en un array
		* 
		* @param { array } array de objectos de la consulta $allreg
		* @param { array } array de los registros que se cargan del excel
		* @return { array } 
		*/
		private function wasEdited( $allreg, $identificadores ){

			$editados = array();			

			if( !is_array($allreg) || !is_array($identificadores) )
				return $editados;

			$alphaIni = range( 'A', 'Z' );
			foreach( $alphaIni as $valor ){
				
				foreach( range( 'A','Z' ) as $letter ){
					
					$alphaIni[] = $valor.$letter;
				}
				unset($letter);					
			}

			foreach( $allreg as $key => $obj ){

				foreach( $identificadores as $idx => $registro ) {
					
					$posLetter = 0;
					if( preg_match( '/^'.preg_quote($obj->identificador,'/').'$/i', $idx) ){

						foreach ( $obj->attrs_map as $pos => $attr ) {					

							if( $pos <= 0 )
								continue;
							
							//valor de la columna de excel	
							$cadena = $registro[ $alphaIni[$posLetter] ];
							$valorxls = preg_replace('/(\n|\r|\n\r|\t|\0|\x0B)/i', ' ', $cadena);
							$valorxls = trim($valorxls);	

							//valor del campo de la tabla de la DB
							$valordb = trim($obj->$attr);
							//comparar los dos valores para comprobar si han sido modificados
							if( preg_match( '/^'.preg_quote($valordb,'/').'$/i', $valorxls) == 0 ){

								$editados[$idx][] = array(
									'identificador' => $idx,
									'namefieldtbl' => $attr,
									'namefieldxls' => $alphaIni[$posLetter],
									'dataOld' => $valordb,
									'dataNew' => $valorxls,
									'registrotbl' => $obj,
									'registroxls' => $registro
								);
							}
							$posLetter++;
						}
						unset($attr);
					}
				}
				unset($registro);
			}

			return $editados;	
		}

		private function registraActividad( $cambios = null, $type ){

			if( !is_array($cambios) || empty($type) )
				return false;

			$user = JFactory::getUser();
			$model_org = $this->getModel( $type );
			$fieldsCombine = $model_org->combineFields();
			$return = true;
			$cambios = array_change_key_case( $cambios, CASE_LOWER );
			$type_organismos = array_change_key_case( $this->type_organismos, CASE_LOWER );
						

			foreach ( $cambios as $key => $cambio ) {
				
				switch ( strtolower($key) ) {

					case 'nuevos':
							
								$cambio = array_change_key_case( $cambio, CASE_LOWER );	
								$dateNow = date( 'Y-m-d H:i:s' );

								foreach ( $cambio as $idx => $nuevo ) {

									$model = $this->getModel( 'CargaOrganismo' );

									$parametros = array(
											'typeModel' => $type
										,	'identificador' => $nuevo[ $fieldsCombine['identificador'] ]
										,	'tabla_organismo' => $model_org->table
									);
									$descripcion_act = 'Se ha insertado un nuevo registro en ('.$type_organismos[ strtolower($type) ].'), Razón Social '.$nuevo[ $fieldsCombine['razon_social'] ].' -Nit '.$nuevo[ $fieldsCombine['nit'] ].' y Código de acreditación '.$nuevo[ $fieldsCombine['codigo_acreditacion'] ].', con el Identificador '.$nuevo[ $fieldsCombine['identificador'] ];

									$args = array(
											'tabla_organismo' => $model_org->table
										,	'identificador'	=> $nuevo[ $fieldsCombine['identificador'] ]
										,	'id_organismo' => ''
										,	'id_usuario' => $user->get( 'id' )
										,	'tipo_gestion' => 'nuevo'
										,	'descripcion_actividad' => $descripcion_act
										,	'razon_social_oec' => $nuevo[ $fieldsCombine['razon_social'] ]
										,	'nit_oec' => $nuevo[ $fieldsCombine['nit'] ]
										,	'created_on' => $dateNow
										,	'created_by' => $user->get( 'id' )
										,	'modified_on' => $dateNow
										,	'modified_by' => $user->get( 'id' )
										,	'parametros' => json_encode($parametros)
									);	

									$model->instance( $args );									
									if( !$model->save('bool') )
										continue;
								}
								unset($nuevo);								 					

						break;

					case 'borrados':
							
							$cambio = array_change_key_case( $cambio, CASE_LOWER );	
							$dateNow = date( 'Y-m-d H:i:s' );

							foreach ( $cambio as $idx => $borrado ) {

								$model = $this->getModel( 'CargaOrganismo' );
						
								$parametros = array(
										'typeModel' => $type
									,	'identificador' => $borrado->identificador
									,	'tabla_organismo' => $model_org->table
								);
								$descripcion_act = 'Ha eliminado el registro con el Identificador '.$borrado->identificador.', del tipo de organismo ('.$type_organismos[ strtolower($type) ].') -Nit. '.$borrado->nit.'.';

								$args = array(
										'tabla_organismo' => $model_org->table
									,	'identificador'	=> $borrado->identificador
									,	'id_organismo' => $borrado->id
									,	'id_usuario' => $user->get( 'id' )
									,	'tipo_gestion' => 'borrado'
									,	'descripcion_actividad' => $descripcion_act
									,	'razon_social_oec' => $borrado->razon_social
									,	'nit_oec' => $borrado->nit
									,	'created_on' => $dateNow
									,	'created_by' => $user->get( 'id' )
									,	'modified_on' => $dateNow
									,	'modified_by' => $user->get( 'id' )
									,	'parametros' => json_encode($parametros)
								);	

								$model->instance( $args );									
								if( !$model->save('bool') )
									continue;
							}
							unset($borrado);

						break;	

					case 'editados':

							$cambio = array_change_key_case( $cambio, CASE_LOWER );	
							$dateNow = date( 'Y-m-d H:i:s' );

							foreach ( $cambio as $idx => $editado ) {

								$descripcion_act = '';
								$datosModificados = array();
								$parametros = array(
											'typeModel' => $type
										,	'identificador' => $idx
										,	'tabla_organismo' => $model_org->table
									);


								foreach ( $editado as $editfield ) {

									$datosModificados[] = array(
										'namefieldtbl' => $editfield[ 'namefieldtbl' ],
										'dataOld' => $editfield[ 'dataOld' ],
										'dataNew' => $editfield[ 'dataNew' ]
									);	

									$descripcion_act .= '- Se ha modificado el registro con el Identificador '.$idx.', -Nit. '.$editfield[ 'registrotbl' ]->nit.', Tipo de organismo ('.$type_organismos[ strtolower($type) ].'), Dato anterior (Campo: '.$editfield[ 'namefieldtbl' ].' Dato: '.$editfield[ 'dataOld' ].') y Dato nuevo(Campo: '.$editfield[ 'namefieldtbl' ].' Dato: '.$editfield[ 'dataNew' ].").";
								}	

								$parametros[ 'datasmodified' ] = $datosModificados;

								$model = $this->getModel( 'CargaOrganismo' );

								$args = array(
											'tabla_organismo' => $model_org->table
										,	'identificador'	=> $idx
										,	'id_organismo' => $editado[0]['registrotbl']->id
										,	'id_usuario' => $user->get( 'id' )
										,	'tipo_gestion' => 'editado'
										,	'descripcion_actividad' => $descripcion_act
										,	'razon_social_oec' => $editado[0]['registrotbl']->razon_social
										,	'nit_oec' => $editado[0]['registrotbl']->nit
										,	'created_on' => $dateNow
										,	'created_by' => $user->get( 'id' )
										,	'modified_on' => $dateNow
										,	'modified_by' => $user->get( 'id' )
										,	'parametros' => json_encode($parametros)
									);	

									$model->instance( $args );									
									if( !$model->save('bool') )
										continue;	
															
							}
							unset($editado);
							
						break;
				}
			}

			return $return;
		}

		/**
		* comprobar si el pdf que se carga esta en los registros del organismo
		* @param { array } $objects array de objetos sobre registros del organismos
		* @param { array } $data array de argumentos solicitados por get	
		* @return { array } array de los registros que coincidieron
		*/
		private function isInvalidPdf( $objects, $data ){

			$PDFS = array();
			$coinciden = array();
			$now = date( 'Y-m-d H:i:s' );

			if( !is_array($objects) || !is_array($data) )
				return $coinciden;

			$pathOfficial = '../pdfs/'. strtoupper($data['type']);
			$pathfile = '../pdfs/tmp/'.$data[ 'pdfFile' ];
			
			$model_org = $this->getModel( $data['type'] );
			$user = JFactory::getUser();

			// Create array of PDF column values
			foreach ( $objects as $key => $object ) {
				
				$PDFS[] = $object->PDF;
			}

			// delete the .pdf extension
			$pdfFile = preg_replace( '/\.pdf$/i', '', $data[ 'pdfFile' ] );			

			// Test if the PDF file name exists in PDFS array list
			$coinciden = preg_grep( '/^'. preg_quote($pdfFile, '/') .'$/i', $PDFS );

			if( !count($coinciden) ){

				if( file_exists($pathfile) )
					unlink($pathfile);

				$model_cargaOrg = $this->getModel( 'CargaOrganismo' );

				$params = array(
					"typeModel" => $data['type'],
					"tabla_organismo" => $model_org->table,
					"archivo" => $data[ 'pdfFile' ]
				);
				$description = 'El usuario '. ucwords($user->get( 'name' )) .'ha intentado subir un PDF inválido. PDF:'. $data[ 'pdfFile' ] .', Organismo: '. $this->type_organismos[ $data['type'] ];

				$args = array(
						'tabla_organismo' => $model_org->table
					,	'identificador' => 'none'
					,	'id_usuario' => $user->get( 'id' )
					,	'tipo_gestion' => 'invalido'
					,	'descripcion_actividad' => $description
					,	'created_on' => $now
					,	'created_by' => $user->get( 'id' )
					,	'modified_on' => $now
					,	'modified_by' => $user->get( 'id' )
					,	'parametros' => json_encode($params)
				);	

				$model_cargaOrg->instance($args);
				if( $model_cargaOrg->save( 'bool' ) ){

					if( file_exists($pathOfficial.'/'.$data[ 'pdfFile' ]) ){

						$model_deleteOrg = $this->getModel( 'CargaOrganismo' );

						$parametros = array(
							"typeModel" => $data['type'],
							"tabla_organismo" => $model_org->table,
							"archivo" => $data[ 'pdfFile' ]
						);
						$description = 'El usuario '. ucwords($user->get('name')) .' ha borrado el PDF '. $data[ 'pdfFile' ] .', Organismos: '. $this->type_organismos[ $data['type'] ];

						$args = array(
								'tabla_organismo' => $model_org->table
							,	'identificador' => 'none'
							,	'id_usuario' => $user->get( 'id' )
							,	'tipo_gestion' => 'borrado'
							,	'descripcion_actividad' => $description
							,	'created_on' => $now
							,	'created_by' => $user->get( 'id' )
							,	'modified_on' => $now
							,	'modified_by' => $user->get( 'id' )
							,	'parametros' => json_encode($parametros)
						);	

						$model_deleteOrg->instance($args);
						if( $model_deleteOrg->save( 'bool' ) ){

							unlink( $pathOfficial.'/'.$data[ 'pdfFile' ] );
						}
					}						
				}		
			}

			return $coinciden;
		}

		/**
		* comprobar si el PDF se sobreescribe
		* @param { array } $data array de argumentos solicitados por get	
		* @return { boolean }
		*/
		private function isOverwrittenPdf( $data ){

			if( !is_array($data) )
				return false;

			$now = date( 'Y-m-d H:i:s' );	
			$model_org = $this->getModel( $data['type'] );
			$user = JFactory::getUser();
			$return = false;			

			$pathOfficial = '../pdfs/'. strtoupper($data['type']) .'/'. $data['pdfFile'];
			$pathTmp = '../pdfs/tmp/'. $data[ 'pdfFile' ];

			if( file_exists($pathOfficial) ){				

				if( rename( $pathTmp, $pathOfficial ) ){

					$model_cargaOrg = $this->getModel( 'CargaOrganismo' );

					$params = array(
						"typeModel" => $data['type'],
						"tabla_organismo" => $model_org->table,
						"archivo" => $data['pdfFile']
					);
					$description = 'El usuario '. ucwords($user->get('name')) .' ha sobrescrito el PDF:'. $data['pdfFile'] .', Organismo: '. $this->type_organismos[ $data['type'] ];

					$args = array(
							'tabla_organismo' => $model_org->table
						,	'identificador' => 'none'
						,	'id_usuario' => $user->get( 'id' )
						,	'tipo_gestion' => 'sobrescrito'
						,	'descripcion_actividad' => $description
						,	'created_on' => $now
						,	'created_by' => $user->get( 'id' )
						,	'modified_on' => $now
						,	'modified_by' => $user->get( 'id' )
						,	'parametros' => json_encode($params)
					);	

					$model_cargaOrg->instance($args);
					$model_cargaOrg->save( 'bool' );

					$return = true;
				}				
				
			}

			 return $return;
		}

		/**
		* comprobar si el PDF es nuevo
		* @param { array } $data array de argumentos solicitados por get	
		* @return { array } array de los registros que coincidieron
		*/
		private function isNewPdf( $data ){

			if( !is_array($data) )
				return false;

			$now = date( 'Y-m-d H:i:s' );	
			$model_org = $this->getModel( $data['type'] );
			$user = JFactory::getUser();
			$return = false;

			$pathOfficial = '../pdfs/'. strtoupper($data['type']) .'/'. $data['pdfFile'];
			$pathTmp = '../pdfs/tmp/'. $data[ 'pdfFile' ];

			if( ! file_exists($pathOfficial) ){

				if( rename( $pathTmp, $pathOfficial ) ){

					$model_cargaOrg = $this->getModel( 'CargaOrganismo' );

					$params = array(
						"typeModel" => $data['type'],
						"tabla_organismo" => $model_org->table,
						"archivo" => $data['pdfFile']
					);
					$description = 'El usuario '. ucwords($user->get('name')) .' ha subido un nuevo PDF. PDF:'. $data['pdfFile'] .', Organismo: '. $this->type_organismos[ $data['type'] ];

					$args = array(
							'tabla_organismo' => $model_org->table
						,	'identificador' => 'none'
						,	'id_usuario' => $user->get( 'id' )
						,	'tipo_gestion' => 'nuevo'
						,	'descripcion_actividad' => $description
						,	'created_on' => $now
						,	'created_by' => $user->get( 'id' )
						,	'modified_on' => $now
						,	'modified_by' => $user->get( 'id' )
						,	'parametros' => json_encode($params)
					);	

					$model_cargaOrg->instance($args);
					$model_cargaOrg->save( 'bool' );

					$return = true;
				}				
				
			}

			return $return;
		}

		/**
		* comprobar que los PDFs en el directorio de organismos son los mismos que se cargan
		* @param { array } $data array de argumentos solicitados por get
		*/
		private function removedPdf( $data ){

			$params = $data[ 'params' ];

			if( !is_array( $data ) )
				return;

			$now = date( 'Y-m-d H:i:s' );	
			$model_org = $this->getModel( $data['type'] );
			$user = JFactory::getUser();
			$return = false;

			ksort( $params[ 'countPdf' ] );
			end( $params[ 'countPdf' ] );
			$ultElm = key( $params['countPdf'] );

			$pathOfficial = '../pdfs/'. strtoupper($data['type']);

			//saber si ha llegado al ultimo elemento de los archivos cargados
			if( $ultElm == $params[ 'utlIdnx' ] ){

				$ficheros = scandir( $pathOfficial );

				foreach ( $ficheros as $key => $fichero ) {
					
					if( !is_file($pathOfficial.'/'.$fichero) )
						continue;

					$coincidencias = array();

					$coincidencias = preg_grep( '/^'. preg_quote( $fichero , '/') .'$/i', $params[ 'countPdf' ] );

					if( !count($coincidencias) && is_array($coincidencias) ){

						$model_cargaOrg = $this->getModel( 'CargaOrganismo' );

						$parametros = array(
							"typeModel" => $data['type'],
							"tabla_organismo" => $model_org->table,
							"archivo" => $fichero
						);
						$description = 'El usuario '. ucwords($user->get('name')) .' ha borrado el PDF '. $fichero .', Organismos: '. $this->type_organismos[ $data['type'] ];

						$args = array(
								'tabla_organismo' => $model_org->table
							,	'identificador' => 'none'
							,	'id_usuario' => $user->get( 'id' )
							,	'tipo_gestion' => 'borrado'
							,	'descripcion_actividad' => $description
							,	'created_on' => $now
							,	'created_by' => $user->get( 'id' )
							,	'modified_on' => $now
							,	'modified_by' => $user->get( 'id' )
							,	'parametros' => json_encode($parametros)
						);	

						$model_cargaOrg->instance($args);
						if( $model_cargaOrg->save( 'bool' ) ){

							unlink( $pathOfficial.'/'.$fichero );
						}

					}

				}
			}
		}

		/**
		* conocer si una peticion es de tipo ajax
		* @param {}
		* return { bool }
		*/	
		private static function isAjax(){
			
			if( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
			{
				return true;
			}
			
			return false;
		}
}
?>