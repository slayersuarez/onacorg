<?php

/**
 * General View for "glosario" "lista" layout
 * 
 */

// Joomla calls and runtimes
defined( '_JEXEC' ) or die();
jimport( 'joomla.application.component.view' );

class GlosarioViewLista extends JViewLegacy {

	protected $terms;
	
	// Function that initializes the view
	function display( $tpl = null ){

		// Gets all the terms in databse where clave will be A
		$this->getTerms();

		parent::display( $tpl );
	
	}

	/**
	* Get all terms
	*
	*/
	public function getTerms(){

		$model = new GlosarioModelTermino();

		$wheres = array(
			0 => ( object )array(

				'key' => 'clave',
				'condition' => ' = ',
				'value' => '"A"',
				'glue' => ''
			)
		);
		$this->terms = $model->getObjects( $wheres );
	}

}
?>