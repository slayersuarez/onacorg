/**
*
* Model for { Directorio }
*
*/

( function( $, window, document, Utilities ){

	var DirectorioModel = function( a ){

	};

	DirectorioModel.prototype = {


			/**
			* Triggers a method in backend, sending data and returning the response
			* Searchs a list of terms by key
			*/
			getParams: function( success, _data){

				var aOptions = {
						dataType: "json"
					,	async: true
					,   success: success
				}
				
				,   aData = {
						option: "com_directorio"
					,	task: "organismo.getOrganismoParams"
					,   model: _data
				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return Utilities.ajaxHandler( aOptions, aData );
				
			}

			/**
			* Triggers a method in backend, sending data and returning the response
			* Searchs a list of terms by key
			*/
		,	search: function( success, _data, page ){

				var aOptions = {
						dataType: "json"
					,	async: true
					,   success: success
				}
				
				,   aData = {
						option: "com_directorio"
					,	task: "organismo.search"
					,   criteria: _data
					,	page: page
				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return Utilities.ajaxHandler( aOptions, aData );
				
			}

	};

	// Expose to global scope
	window.DirectorioModel = new DirectorioModel();

})( jQuery, this, this.document, this.Misc, undefined );