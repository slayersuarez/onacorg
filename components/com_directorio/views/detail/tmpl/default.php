<?php

/**
* Default Template for object
*
*/

// Initialize
defined('_JEXEC') or die;
$app = JFactory::getApplication(); // Joomla application
$uri = &JURI::getInstance(); // base url object
$url = $uri->root(); // url root
$component_images_url = $url . 'components/com_directorio/assets/images/';

$document = JFactory::getDocument();

// styles
$document -> addStyleSheet( $url . 'components/com_directorio/assets/css/style.css' );
$document -> addStyleSheet( $url . 'components/com_directorio/assets/css/loader.css' );

// scripts
$document -> addScript( $url . 'components/com_directorio/assets/js/libs/jquery-1.10.2.min.js');
$document -> addScript( $url . 'components/com_directorio/assets/js/libs/ejs.js');
$document -> addScript( $url . 'components/com_directorio/assets/js/misc/misc.js');
$document -> addScript( $url . 'components/com_directorio/assets/js/views/directorio.js');
$document -> addScript( $url . 'components/com_directorio/assets/js/models/directorio.js');
$document -> addScript( $url . 'components/com_directorio/assets/js/controllers/directorio.js');
$document -> addScript( $url . 'components/com_directorio/assets/js/handlers/directorio.js');
$document -> addScriptDeclaration( 'var url = "' . $url .'"; var urlTemplates = "' . $url . 'components/com_directorio/assets/js/templates/";' );
?>
<div class="wrapper-component-directorio">

	<div class="box-directorio">
		<h2 class="directorio-header">Directorio de Acreditación</h2>

		<div class="wrapper-empresa">
			<h2>DATOS DE EMPRESA</h2>

			<a href="index.php?option=com_directorio&template=onac-component" class="onac-button">Nueva Búsqueda</a>

			<div class="wrapper-data-empresa">
				<ul>
					<?php 
					foreach ( $this->acreditado->natural_names as $key => $name ):
						if( ! in_array( $key, $this->acreditado->parametros ) ):
					?>
						<li class="data-title"><?php echo $name; ?></li>
						<li><?php echo ( $this->acreditado->$key == '' ) ? 'N/A': $this->acreditado->$key; ?></li>
					<?php
						endif;
					endforeach;
					?>

					<li class="data-title spaced-top">Acreditaciones:</li>
					<li class="spaced-top">
						<ul>
							<li>
								<?php foreach ( $this->acreditaciones as $key => $acreditacion ):?>
									<?php
 
										$url_acreditacion = ( $acreditacion->existPDF ) ? $url .'index.php?option=com_directorio&task=organismo.downloadPDF&type=' . $this->acreditado->type . '&id=' . $acreditacion->id : '#';

										$icon_style = ( $acreditacion->existPDF ) ? '': 'style="opacity:.3;"';

									?>
										<a href="<?php echo $url_acreditacion; ?>"><img <?php echo $icon_style; ?> src="<?php echo $component_images_url; ?>pdf-icon.png">Acreditación <?php echo $acreditacion->codigo; ?></a>
									<?php
									?>
								<?php endforeach; ?>
							</li>
						</ul>
					</li>
				</ul>
			</div>

			<div class="wrapper-alcances-empresa">
				<ul class="table-alcances">

					<?php

						// $count = 1; 
						// foreach ( $this->acreditado->parametros  as $key => $parametro ):

						// 	if( $this->acreditado->$parametro != '' ):
					?>
							<!-- <li class="alcance-item">
								<div class="number-item"><?php //echo $count++; ?></div>
								<div class="description-item">
									<?php //echo $this->acreditado->$parametro; ?>
								</div>
							</li> -->
					<?php
						// 	endif;
						// endforeach;
					?>
				</ul>
			</div>
		</div>
	</div>
</div>