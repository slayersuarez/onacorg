<?php
/**
 * @copyright	Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Component Controller
 *
 * @package		Joomla.Administrator
 * @subpackage	com_si
 */
class GlosarioController extends JControllerLegacy
{
	/**
	 * @var		string	The default view.
	 * @since	1.6
	 */
	protected $default_view = 'lista';

	/**
	 * Method to display a view.
	 *
	 * @param	boolean			If true, the view output will be cached
	 * @param	array			An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return	JController		This object to support chaining.
	 * @since	1.5
	 */
	public function display( $cachable = false, $urlparams = false)
	{

	
		
		$render = JRequest::getVar('render', '');
		
		if(!empty($render)){
			
			$metodo = "render".$render;
			$this->$metodo();
			
			return;
		}
		
		
		$this->renderDefault();

		parent::display($cachable,$urlparams);
		
		
		return $this;
	}
	
	private function renderDefault(){
	
		
		$view =& $this->getView( 'lista', 'html' );
		
		// Call the model
		$model =& $this->getModel( 'termino' );

		$model->setParams();

		// Get all the Informes Seguimiento
		$terminos = $model->getObjects();
		
		// Assign informes as reference to the view
		
		$view->assignRef( 'terminos', $terminos );
		
	
	}
	
	
	public function renderEdit(){
		
	
		$view =& $this->getView( 'termino', 'html' );
		
		
		// Call the model
		$model =& $this->getModel( 'termino' );
		
		// Get all the Informes Seguimiento
		$terminos = $model->getObjects();
		
		// Assign terminos as reference to the view
		$view->assignRef( 'terminos', $terminos );
		
		// Display the view
		$view->display("new");
		
	}
}