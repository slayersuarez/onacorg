/**
*
* Controller for { Glosario }
*
*
**/

( function( $, window, document, model, view, Utilities ){
	
	var GlosarioController = function( a ){
		
		// atributes, selector or global vars

		
	};
	
	GlosarioController.prototype ={
			

			/**
			 * Calls the view method to adjust the tooltips positions
			 *
			 */
			setTooltip: function(){
				
				view.setTooltip();
			}


			/**
			 * Call the model function and render to the view the response
			 * This method will manage the communication between view and model
			 *
			 */
		,	searchByKey: function( data ){
				
				var success = function( data ){

					view.onProcess( '.wrapp-lista-content-terminos', 1000, function(){ view.onCompleteSearch( data ) } );
				};

				return model.searchByKey( success, data );
			}

			/**
			 * Call the model function and render to the view the response
			 * This method will manage the communication between view and model
			 *
			 */
		,	searchBy: function( data ){
				
				var success = function( data ){

					view.onProcess( '.wrapp-lista-content-terminos', 1000, function(){ view.onCompleteSearch( data ) } );
				};

				return model.searchBy( success, data );
			}

		
			
	};
	

	// Use this way when script is loaded at the end of the page
	// Expose to global scope
	window.GlosarioController = new GlosarioController();
	
})( jQuery, this, this.document, this.GlosarioModel, this.GlosarioView, this.Misc, undefined );