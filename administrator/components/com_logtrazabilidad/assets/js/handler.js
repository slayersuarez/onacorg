/**
*
* Event Handler for { LogTrazabilidad }
*
*
**/

( function( $, window, document, Utilities ){
	
	var LogTrazabilidadHandler = function( a ){
		
		// atributes, selector or global vars
		this.textdatetime = '.plg-datatime';
		this.btnLimpia = '.limpiar-filtros';		
	};
	
	LogTrazabilidadHandler.prototype = {
			
			/**
			 * Inializes the functions when DOM ready
			 */	
			initialize: function(){
				
				this.addDateTimePicker();
				this.setAutocomplete();
				this.limpiarFiltros();
				return true;
				
			}

			/**
			 * add datetimepicker to date text-fileds
			 *
			 */
		,	addDateTimePicker: function(){
				

				 Utilities.initDateTimePicker( this.textdatetime, { 
				 	lang : 'es', 
				 	mask : true,
				 	format : 'Y-m-d H:i'
				 });	
			}

			/**
			 * autocompletes filters datas
			 *
			 */
		,	setAutocomplete: function(){

				var _this = this;

				//autocompletado para el campo de nombre de usuario log_session
				var paramsNameUser = {

					optionsAjax : {
						url : 'index.php',
						type : 'post',
						dataType: "json",
						error : _this.errorResponseXHR,
						complete : _this.completeResponseXHR,
						async : true

					},

					datasAjax : {													
						option : 'com_logtrazabilidad',
						task : 'logsession.getNamesUser'
					}, 
				}
				this.addAutoComplete( "#nombre_usuario" , paramsNameUser );

				//autocompletado para el campo de id de usuario log_session
				var paramsIdUser = {

					optionsAjax : {
						url : 'index.php',
						type : 'post',
						dataType: "json",
						error : _this.errorResponseXHR,
						complete : _this.completeResponseXHR,
						async : true

					},

					datasAjax : {													
						option : 'com_logtrazabilidad',
						task : 'logsession.getIdUser'
					}, 
				}
				this.addAutoComplete( "#id_usuario" , paramsIdUser);

				//autocompletado para el campo de nombre de usuario log_acredita
				var paramsNameUser = {

					optionsAjax : {
						url : 'index.php',
						type : 'post',
						dataType: "json",
						error : _this.errorResponseXHR,
						complete : _this.completeResponseXHR,
						async : true

					},

					datasAjax : {													
						option : 'com_logtrazabilidad',
						task : 'logacredita.getNamesUser'
					}, 
				}
				this.addAutoComplete( "#nom_user" , paramsNameUser );

				//autocompletado para el campo de nombre de usuario log_acredita
				var paramsDescription = {

					optionsAjax : {
						url : 'index.php',
						type : 'post',
						dataType: "json",
						error : _this.errorResponseXHR,
						complete : _this.completeResponseXHR,
						async : true

					},

					datasAjax : {													
						option : 'com_logtrazabilidad',
						task : 'logacredita.getDescription'
					}, 
				}
				this.addAutoComplete( "#descripcion_act" , paramsDescription );
								
			}

		/**
		* código a ejecutar si la petición falla;
		* @param { object } object jqXHR
		* @param { object } object status
		* @param { object } oobjtec error
		*/	
		,	errorResponseXHR : function(jqXHR, status, error) {

				//console.log( jqXHR , status, error );
            	alert('Hubo un problema, Por favor recargue la pagina');
       		}

       	/**
		* código a ejecutar sin importar si la petición falló o no
		* @param { object } object jqXHR
		* @param { object } object status
		*/	
        ,	completeResponseXHR : function(jqXHR, status) {

				//console.log( jqXHR, status );
            	//alert('Petición realizada');
        	}

        ,	addAutoComplete : function( selector, params ){

        	if( typeof selector == 'undefined' || typeof params == 'undefined' )
        		return false;

        	var _this = this;

    		$( selector ).autocomplete({

				source: function( request, response ) {

					//extender las opciones de ajax que se le pasan
					params.optionsAjax = $.extend({}, params.optionsAjax, {
						success : function( data ){

							response( data );

							$( selector ).removeClass('no-results');
							if( data.length < 1 || data[ 0 ] == '' )
								$( selector ).addClass('no-results');
						}
					});
					
					//extender los parametros de envio que se le pasan
					params.datasAjax = $.extend( {}, params.datasAjax, { q : request.term } );
					
					Utilities.ajaxHandler( params.optionsAjax , params.datasAjax );						
				},

				minLength: 2,

				select: function( event, ui ) {

					// log( ui.item ?
					// "Selected: " + ui.item.label :
					// "Nothing selected, input was " + this.value);
				},

				open: function() {
					$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
				},

				close: function() {
					$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
				}
			});
        }	

        /**
        * Limpiar filtros
        */
        ,	limpiarFiltros : function(){

        	$( this.btnLimpia ).click(function(e) {

        		e.preventDefault();
        		$( this.form ).find( ':input' ).val( '' );
        		$( this.form ).find('[name="filter_order_Dir"]').val('DESC');
        		this.form.submit();
        		
        	});
        }

	};
	
	// Use this when the script is loaded in <head> tag
	$( document ).ready( function(){
		// Expose to global scope
		window.LogTrazabilidadHandler = new LogTrazabilidadHandler();
		window.LogTrazabilidadHandler.initialize();	
	});
	
})( jQuery, this, this.document, this.Misc, undefined );