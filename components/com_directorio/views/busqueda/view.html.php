<?php

/**
 * General View for "Directorio" "busqueda" layout
 * 
 */

// Joomla calls and runtimes
defined( '_JEXEC' ) or die();
jimport( 'joomla.application.component.view' );

class DirectorioViewBusqueda extends JViewLegacy {

	protected $terms;
	protected $cities;
	
	// Function that initializes the view
	function display( $tpl = null ){

		// Gets all the terms in databse where clave will be A
		$this->getOrganismos();

		$this->getCities();

		parent::display( $tpl );
	
	}

	/**
	* Get all terms
	*
	*/
	public function getOrganismos(){

		/*$model = new DirectorioModelOrganismo();

		$wheres = array(
			0 => ( object )array(

				'key' => 'clave',
				'condition' => ' = ',
				'value' => '"A"',
				'glue' => ''
			)
		);
		$this->terms = $model->getObjects( $wheres );*/
	}

	/**
		* Gets the cities
		*
		*/
		public function getCities(){

			$model_lab1 = new DirectorioModelLab1();
			$model_lac1 = new DirectorioModelLac1();
			$model_cda = new DirectorioModelCda();
			$model_cdf = new DirectorioModelCdf();
			$model_cepplus = new DirectorioModelCepplus();
			$model_cpr = new DirectorioModelCpr();
			$model_csg = new DirectorioModelCsg();
			$model_lcl = new DirectorioModelLcl();
			$model_ocp = new DirectorioModelOcp();
			$model_oin = new DirectorioModelOin();

			$all_model_cities = array(

				0 => $model_lab1->getCities(),
				1 => $model_lac1->getCities(),
				2 => $model_cda->getCities(),
				3 => $model_cdf->getCities(),
				4 => $model_cepplus->getCities(),
				5 => $model_cpr->getCities(),
				6 => $model_csg->getCities(),
				7 => $model_lcl->getCities(),
				8 => $model_ocp->getCities(),
				9 => $model_oin->getCities()
			);

			$cities = array();

			// get the attributes map of the models
			foreach ( $all_model_cities as $key => $array_cities ) {

				$cities = array_merge( $cities, $array_cities );
			}

			$cities = array_unique( $cities );

			natcasesort( $cities );

			$this->cities = $cities;
		}

}
?>