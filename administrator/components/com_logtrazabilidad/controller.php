<?php
 /**
 * @package		LogTrazabilidad Integration
 * @subpackage	com_logtrazabilidad
 */
 
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

class LogTrazabilidadController extends JController{

	protected $viewDefault = 'logsession';

	public function display($cachable = false, $urlparams = false){

		// Load the submenu.
		LogTrazabilidadHelper::addSubmenu( JRequest::getCmd('view', $this->viewDefault) );

		$view		= JRequest::getCmd('view', 'logsession');
		$layout 	= JRequest::getCmd('layout', 'default');
		$id			= JRequest::getInt('id');		

		JRequest::setVar( 'view', $view );
		JRequest::setVar( 'layout', $layout );

		parent::display($cachable, $urlparams);
	}
}
