<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die();

//get the hosts name
jimport('joomla.environment.uri' );
$host = JURI::root();
$document =& JFactory::getDocument();

$document->addStyleSheet($host.'administrator/components/com_glosario/assets/css/style.css');
?>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<form action="index.php" method="post" name="adminForm">
	
	<div class="editcell">
		<table class="adminlist">
			<!-- Encabezado -->
			<thead>
				<tr>
					<th width="20">
						<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $this->items ); ?>);" />
					</th>
					<th width="5">ID</th>
					<th>Razon Social</th>
					<th>Código Acreditación</th>
					<th>Dirección</th>
				</tr>
			</thead>
			
			<!-- Elementos -->
			<tbody>
				<?php
					
					$n = 0;
					
					//loop for the elements
					foreach ( $this->items as $item ):
						
						// Create the checkboxes
						$checked = JHtml::_( 'grid.id', $n, $item->id );
				?>
				<tr>
					<td><?php echo $checked; ?></td>
					<td><?php echo $item->id; ?></td>
					<td><?php echo $item->razon_social; ?></td>
					<td><?php echo $item->codigo_acreditacion; ?></td>
					<td><?php echo $item->direccion_sede_principal; ?></td>
				</tr>
				<?php
						$n++;
					endforeach;
				?>
			</tbody>
		</table>
	</div>
	<input type="hidden" name="option" value="com_directorio" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="controller" value="lista" />
</form>