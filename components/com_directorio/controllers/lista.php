
<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );

// Begining of the controller
class GlosarioControllerLista extends JController{


		/**
		* Searchs by key
		*
		*/
		public function searchByKey(){

			// Initialize
			$data = JRequest::getVar( 'data' );
			$clave = $data;
			$response = ( object ) array();

			// validate clave is string
			if( ! is_string( $clave ) ){

				$response->status = 500;
				$response->message = 'Hubo un error al intentar obtener los terminos. Intente de nuevo.';

				echo json_encode( $response );
				die();
			}

			$clave = strip_tags( $clave );

			// get the model
			$model = $this->getModel( 'termino' );

			// Create the condition
			$wheres = array(
				0 => ( object )array(

						'key' => 'clave'
					,	'value' => "'$clave'"
					,	'condition' => ' = '
					,	'glue' => 'AND'
				)
			);

			// Get the objects
			$response->status = 200;
			$response->data = $model->getObjects( $wheres );
			$response->clave = $data;
			$response->message = 'Consulta satisfactoria';

			echo json_encode( $response );
			die();
		}


		/**
		* Searchs by term
		*
		*/
		public function searchByTerm(){

			// Initialize
			$data = JRequest::getVar( 'data' );
			$term = $data[ 'termino' ];
			$response = ( object ) array();

			// validate term is string
			if( ! is_string( $term ) ){

				$response->status = 500;
				$response->message = 'Hubo un error al intentar obtener los terminos. Intente de nuevo.';

				echo json_encode( $response );
				die();
			}

			$term = strip_tags( $term );

			// get the model
			$model = $this->getModel( 'termino' );

			// get objects
			$objects = $model->getObjects();
			$results = array();

			// Compare the string
			foreach ( $objects as $key => $termino ) {
				
				if ( preg_match("/" . $term . "/i", $termino->descripcion ) ) {
				    array_push( $results, $termino );
				}

				if ( preg_match("/" . $term . "/i", $termino->termino ) ) {
				    array_push( $results, $termino );
				}
			}

			// Get the objects
			$response->status = 200;
			$response->data = array_unique( $results );
			$response->clave = $data;
			$response->message = 'Consulta satisfactoria';

			echo json_encode( $response );
			die();
		}
}
?>