/**
*
* File uploading
*
*/

( function( $, window, document, directorio ){

	var Uploader = function( a ){

		this.Excelparams = {

				element: document.getElementById( 'excel-uploader' )
			,   action: 'index.php'
			,   params: {

			    	option: 'com_directorio',
			    	task: 'organismo.uploadExcel'
			    }

			,   allowedExtensions: [ 'xlsx', 'xls', 'ods' ]
			,   debug: false
			,	onSubmit: this.onSubmitExcel
			,	onProgress: this.onProgressExcel
			,	onComplete: this.onCompleteExcel
			,   onError: this.onErrorExcel
		    
		};

		this.PDFparams = {

				element: document.getElementById( 'pdf-uploader' )
			,   action: 'index.php'
			,   params: {

			    	option: 'com_directorio',
			    	task: 'organismo.uploadPDF',
			    	type: $( '#select-tipo_organismo' ).find( ':selected' ).val()
			    }

			,   allowedExtensions: [ 'pdf' ]
			,	loadFiles : null
			,	countPdf : new Array()
			,   debug: false
			,	onSubmit: this.onSubmitPDF
			,	onProgress: this.onProgressPDF
			,	onComplete: this.onCompletePDF
			,   onError: this.onErrorPDF
		    
		};

	};

	Uploader.prototype = {

			initialize: function(){

				this.createUploaderExcel();
				this.createUploaderPDF();
			}

		,	createUploaderExcel: function(){

				var _this = this;

				var uploader = new qq.FileUploader( _this.Excelparams );

			}

		,	onSubmitExcel: function( id, fileName ){

				var li = $( '<li>' )
				,	i = $( '<i>' )
				,	spanName = $( '<span>' )
				,	spanBar = $( '<span>' )
				,	spanPerc = $( '<span>' );

				i.addClass( 'icon-excel' );
				spanName.text( fileName );
				spanName.addClass('filename');
				spanBar.addClass( 'progress-bar' );
				spanPerc.addClass( 'percentage' );

				i.appendTo( li );
				spanName.appendTo( li );
				spanBar.appendTo( li );
				spanPerc.appendTo( li );

				li.attr( 'id', 'item-' + fileName.replace( /(.xlsx|.xls|.ods)$/gi, '' ) );

				li.appendTo( $( '.excel-list' ) );
			}

		,	onProgressExcel: function( id, fileName, loaded, total ){

				var perc = Math.floor( loaded / total * 100 );

				$( '#item-' + fileName.replace( /(.xlsx|.xls|.ods)$/gi, '' ) + ' .percentage' ).text( perc + '%' );
				$( '#item-' + fileName.replace( /(.xlsx|.xls|.ods)$/gi, '' ) + ' .progress-bar' ).width( perc + 'px' );

			}

		,	onCompleteExcel: function(id, fileName, responseJSON){

				if( ! responseJSON.success ){
					$( '.qq-upload-list' ).remove();
					return;
				}

				// Save the file name in array to be processed at the end
				directorio.excels.push( fileName );

			}

		,   onErrorExcel: function(id, fileName, xhr){
				alert( 'Falló subida del archivo de excel ' + fileName );
				return;
			}

		,	createUploaderPDF: function(){

				var _this = this;

				_this.PDFparams.params.type = $( '#select-tipo_organismo' ).find( ':selected' ).val();

				this.pdfUploader = new qq.FileUploader( _this.PDFparams );

			}

		,	onSubmitPDF: function( id, fileName ){

				var li = $( '<li>' )
				,	i = $( '<i>' )
				,	spanName = $( '<span>' )
				,	spanBar = $( '<span>' )
				,	spanPerc = $( '<span>' );

				i.addClass( 'icon-pdf' );
				spanName.text( fileName );
				spanBar.addClass( 'progress-bar' );
				spanPerc.addClass( 'percentage' );

				i.appendTo( li );
				spanName.appendTo( li );
				spanBar.appendTo( li );
				spanPerc.appendTo( li );

				li.attr( 'id', 'item-' + fileName.replace( /\.pdf$/gi, '' ) );

				var queue = $( "input[name='file']" )[1].files;
				this.loadFiles = queue;

				li.appendTo( $( '.pdf-list' ) );
			}

		,	onProgressPDF: function( id, fileName, loaded, total ){

				var perc = Math.floor( loaded / total * 100 );

				$( '#item-' + fileName.replace( /\.pdf$/gi, '' ) + ' .percentage' ).text( perc + '%' );
				$( '#item-' + fileName.replace( /\.pdf$/gi, '' ) + ' .progress-bar' ).width( perc + 'px' );

			}

		,	onCompletePDF: function(id, fileName, responseJSON){				

				if( ! responseJSON.success ){

					if( $( '.pdf-list > li:eq('+id+')' ).length ){
						
						$( '.pdf-list > li:eq('+id+')' ).remove();
					}					
					return;
				}

				this.countPdf.push(responseJSON.filename);

				var utlIdnx = this.loadFiles.length-1;
				var params = {
						'id' : id
					,	'countPdf' : this.countPdf
					,	'utlIdnx' : utlIdnx
				};

				window.DirectorioController.pdf = responseJSON.filename;
				window.DirectorioController.comparePDF(params);

				$( '.qq-upload-list' ).remove();


			}
		,   onErrorPDF: function(id, fileName, xhr){
				alert( 'Falló subida del archivo de pdf ' + fileName );
				return;
			}
	};

	$( document ).ready( function(){
		window.Uploader = new Uploader();
		window.Uploader.initialize();
	});

})( jQuery, this, this.document, this.DirectorioController, undefined );







