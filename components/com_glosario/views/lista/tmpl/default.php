<?php

/**
* Default Template for object
*
*/

// Initialize
defined('_JEXEC') or die;
$app = JFactory::getApplication(); // Joomla application
$uri = &JURI::getInstance(); // base url object
$url = $uri->root(); // url root

$document = JFactory::getDocument();

// styles
$document -> addStyleSheet( $url . 'components/com_glosario/assets/css/style.css' );
$document -> addStyleSheet( $url . 'components/com_glosario/assets/css/loader.css' );

// scripts
$document -> addScript( $url . 'components/com_glosario/assets/js/libs/jquery-1.10.2.min.js');
$document -> addScript( $url . 'components/com_glosario/assets/js/libs/ejs.js');
$document -> addScript( $url . 'components/com_glosario/assets/js/misc/misc.js');
$document -> addScript( $url . 'components/com_glosario/assets/js/views/glosario.js');
$document -> addScript( $url . 'components/com_glosario/assets/js/models/glosario.js');
$document -> addScript( $url . 'components/com_glosario/assets/js/controllers/glosario.js');
$document -> addScript( $url . 'components/com_glosario/assets/js/handlers/glosario.js');
$document -> addScriptDeclaration( 'var url = "' . $url .'"; var urlTemplates = "' . $url . 'components/com_glosario/assets/js/templates/";' );

// do stuff
?>
<div class="wrapper-component-glosario">
	<h2>SERVICIOS DE ACREDITACION</h2>

	<div class="box-glosario">
		<h2 class="glosario-header">Glosario</h2>

		<div class="wrapper-buscador">
			<form class="buscador-glosario" id="buscador-glosario">
				<input type="text" name="termino" placeholder="Escriba un término o palabra" />
				<a href="#" class='search-term-button'>
					<img class="icon-search" src="<?php echo $url; ?>components/com_glosario/assets/images/lupa.png" />
				</a>
			</form>
		</div>

		<div class="alphabet-pagination">
			<ul>
				<li><a class="alphabet-letter active" href="#">A</a></li>
				<li><a class="alphabet-letter" href="#">B</a></li>
				<li><a class="alphabet-letter" href="#">C</a></li>
				<li><a class="alphabet-letter" href="#">D</a></li>
				<li><a class="alphabet-letter" href="#">E</a></li>
				<li><a class="alphabet-letter" href="#">F</a></li>
				<li><a class="alphabet-letter" href="#">G</a></li>
				<li><a class="alphabet-letter" href="#">H</a></li>
				<li><a class="alphabet-letter" href="#">I</a></li>
				<li><a class="alphabet-letter" href="#">J</a></li>
				<li><a class="alphabet-letter" href="#">K</a></li>
				<li><a class="alphabet-letter" href="#">L</a></li>
				<li><a class="alphabet-letter" href="#">M</a></li>
				<li><a class="alphabet-letter" href="#">N</a></li>
				<li><a class="alphabet-letter" href="#">Ñ</a></li>
				<li><a class="alphabet-letter" href="#">O</a></li>
				<li><a class="alphabet-letter" href="#">P</a></li>
				<li><a class="alphabet-letter" href="#">Q</a></li>
				<li><a class="alphabet-letter" href="#">R</a></li>
				<li><a class="alphabet-letter" href="#">S</a></li>
				<li><a class="alphabet-letter" href="#">T</a></li>
				<li><a class="alphabet-letter" href="#">U</a></li>
				<li><a class="alphabet-letter" href="#">V</a></li>
				<li><a class="alphabet-letter" href="#">W</a></li>
				<li><a class="alphabet-letter" href="#">X</a></li>
				<li><a class="alphabet-letter" href="#">Y</a></li>
				<li><a class="alphabet-letter" href="#">Z</a></li>
			</ul>
		</div>

		<div class="wrapper-lista-terminos">

			<h2 class="lista-terminos-header"><span>A</span></h2>

			<div class="wrapp-lista-content-terminos">
				<?php if( ! empty( $this->terms ) ): ?>
				<ul class="lista-terminos">
					<?php foreach( $this->terms as $term ): ?>
					<li>
						<a href="#" class="glosario-termino">
							<span><?php echo $term->termino; ?></span>
							<div class="tooltip-descripcion-termino">
								<div class="tooltip-inset-content">
									<span><?php echo $term->descripcion; ?></span>
									<span class="tooltip-triangle-gray"><span class="tooltip-triangle-white"></span></span>
								</div>
							</div>
						</a>
					</li>
					<?php endforeach; ?>
				</ul>
				<?php 
					else:
				?>
				<h3>No hay términos para esta sección</h3>
				<?
				endif; ?>
			</div>
		</div>
	</div>
</div>