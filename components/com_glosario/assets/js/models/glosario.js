/**
*
* GlosarioModel for { Glosario }
*
*/

( function( $, window, document, Utilities ){

	var GlosarioModel = function( a ){

	};

	GlosarioModel.prototype = {


			/**
			* Triggers a method in backend, sending data and returning the response
			* Searchs a list of terms by key
			*/
			searchByKey: function( success, _data ){

				var aOptions = {
						dataType: "json"
					,	async: true
					,   success: success
				}
				
				,   aData = {
						option: "com_glosario"
					,	task: "lista.searchByKey"
					,   data: _data
				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return Utilities.ajaxHandler( aOptions, aData );
				
			}

			/**
			* Triggers a method in backend, sending data and returning the response
			* Searchs a list of terms by term
			*/
		,	searchBy: function( success, _data ){

				var aOptions = {
						dataType: "json"
					,	async: true
					,   success: success
				}
				
				,   aData = {
						option: "com_glosario"
					,	task: "lista.searchByTerm"
					,   data: _data
				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return Utilities.ajaxHandler( aOptions, aData );
				
			}

	};

	// Expose to global scope
	window.GlosarioModel = new GlosarioModel();

})( jQuery, this, this.document, this.Misc, undefined );