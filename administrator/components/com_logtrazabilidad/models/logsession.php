<?php
/**
 * @copyright	Copyright (C) 2013 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

/**
 * configuracion pqrs Controller
 *
 * @package		Joomla.Administrator
 * @subpackage	com_logsessions
 */


jimport('joomla.application.component.modellist');

// Initializes the Class
class LogTrazabilidadModelLogSession extends JModelList{

	
	/**
	 * pobject Id
	 * @var int
	 */
	public $id_log_session;

	/**
	 * Attribute
	 * @var int
	 */
	public $id_usuario;

	/**
	 * Attribute
	 * @var string
	 */
	public $nombre_usuario;

	/**
	 * Attribute
	 * @var date
	 */
	public $fecha_acceso;

	/**
	 * Attribute
	 * @var date
	 */
	public $fecha_cierre;


	/**
	 * Attribute
	 * @var float
	 */
	public $duracion;

	/**
	 * Attribute
	 * @var char
	 */
	public $tipo_log;

	/**
	 * Attribute
	 * @var char
	 */
	public $tipo_user;
	

	/**
	* array filter
	*/
	public $filters = array();

	protected $counts = 0;
	
	/**
	* Attribute id inserted
	* @var int
	*/
	var $insertId;
	

	/**
	 * Constant for table
	 * @var string
	 */
	const TABLE = 'log_sessions_user';
	
	//constante base para los filtros
	const FILTER_STATE = 'logsessions.state.';
	
	/**
	 * Attributes Map
	 * @var array
	 */
	protected $attrs_map = array( 
			'id_log_session'	
		,	'id_usuario'
		,	'nombre_usuario'
		,	'fecha_acceso'
		,	'fecha_cierre'
		,	'duracion'
		,	'tipo_log'
		,	'tipo_user'	
	);
	
	/**
	 * Constructor
	 * 
	 * @param { array || int } the args to instance the model or the single id
	 * 
	 */

	public function __construct()
	{

	    if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id_log_session', 'a.id_log_session',
				'id_usuario', 'a.id_usuario',
				'duracion', 'a.duracion',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Adjust the context to support modal layouts.
		if ($layout = JRequest::getVar('layout', 'default'))
		{
			$this->context .= '.'.$layout;
		}

		// Load the filter state.
		$search = $this->getUserStateFromRequest($this->context.'.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$search = $this->getUserStateFromRequest($this->context.'.filter.id_usuario', 'id_usuario');
		$this->setState('filter.id_usuario', $search);

		$search = $this->getUserStateFromRequest($this->context.'.filter.nombre_usuario', 'nombre_usuario');
		$this->setState('filter.nombre_usuario', $search);

		$search = $this->getUserStateFromRequest($this->context.'.filter.fecha_acceso', 'fecha_acceso');
		$this->setState('filter.fecha_acceso', $search);

		$search = $this->getUserStateFromRequest($this->context.'.filter.fecha_cierre', 'fecha_cierre');
		$this->setState('filter.fecha_cierre', $search);

		// Load the parameters.
		$params = JComponentHelper::getParams('com_logtrazabilidad');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('id_log_session', 'DESC');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return  string  A store id.
	 *
	 * @since   1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id	.= ':'.$this->getState('filter.search');
		$id	.= ':'.$this->getState('filter.id_usuario');
		$id	.= ':'.$this->getState('filter.nombre_usuario');
		$id	.= ':'.$this->getState('filter.fecha_acceso');
		$id .= ':'.$this->getState('filter.fecha_cierre');

		return parent::getStoreId($id);
	}
		
	public function instance( $config = NULL ){
		
		if( is_numeric( $config ) )
			$config = array( $this->attrs_map[ 0 ] => $config );
		
		if( ! is_array( $config ) )
			return;
		
		// Get existing object if the id was passed through
		return $this->fill( $config );
	}
	
	/**
	 * Fill the model attributes with the passed arguments.
	 *
	 * @param { arr } Object arguments
	 */
	protected function fill( $args = NULL ){


		if ( ! is_array( $args ) )
			return false;
		
	
		// Get object in DB			
		if ( is_numeric( $args[ $this->attrs_map[ 0 ] ] ) ){

			$object = $this->getObject( $args[ $this->attrs_map[ 0 ] ] );
			
			if( is_object( $object ) ){
				foreach ( $this->attrs_map as $attr ) {
					
					if ( isset( $object->$attr ) )
						$this->$attr = $object->$attr;
				}
			}

		}

	
		// Merge attributes	when id is not passed through
		foreach ( $this->attrs_map as $attr ) {
			if ( isset( $args[ $attr ] ) )
				$this->$attr = $args[ $attr ];
		}

		// Set exists to true.
		$this->exists = true;
	
	}
	
	
	/**
	 * Get a single Object
	 * 
	 * @param { int } the id or attributes of the Object.
	 * @return { bool/object } the object returned or false otherwise
	 */
	public function getObject( $id = 1 ){
		
		if( ! is_numeric( $id ) )
			return false;
		
		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );
		
		$c = get_called_class();  

		$query->select( '*' );
		$query->from( $c::TABLE );
		$query->where( $this->attrs_map[ 0 ].' = ' . $id );
		
		$db->setQuery( $query );
		
		return $db->loadObject();
		
	}
	
	/**
	 * Get Objects collection
	 *
	 * @param { int } the id or attributes of the Object.
	 * @return { bool/object } the object returned or false otherwise
	 **/
	public function getObjects( $wheres = NULL , $prlimits = array() , $params ){
		
		if( !isset( $prlimits[ 'limitstart' ] ) )
			$prlimits[ 'limitstart' ] = $this->getState('list.start');
			
		if( !isset( $prlimits[ 'limit' ] ) )	
			$prlimits[ 'limit' ] = $this->getState('list.limit');
		
		$result = array();
		
		if( ! is_array( $wheres ) )
			$wheres = array();
			
		if( ! is_array( $params ) )	
			$params = array();
		
		$query = $this->buildQuery( $wheres , $params );
		//fb( $query->__toString() );
		$this->data = $this->_getList( $query , $prlimits[ 'limitstart' ] , $prlimits[ 'limit' ] );
		$this->counts = $this->_getListCount($query); 
		
		foreach ( $this->data as $obj ){

			$args = array();
			
			foreach ( $obj as $key => $attr ) {
				
				$args[ $key ] = $obj->$key;
			}
			
			$estaClase = get_class( $this );
	
			$object = new $estaClase();
			$object->fill( $args );
			array_push( $result, $object );
			
		}
	
		return $result;
	
	}

	/**
	 * Save a new object or update an exist.
	 *
	 * @param { string } the type of the response
	 * ( string returns the error string or "" )
	 * ( bool return false or true )
	 * ( object returns the response with error_message, status true or false )
	 * @return { string  } the query string calling the insert query
	 *
	 */
	public function save( $return = 'string' ){

		// Initialize
		$db = JFactory::getDbo();
		$response = ( object ) array();
		$model = ( object ) array();

		if( ! is_string( $return ) )
			$return = 'string';

		// Fetch the model attributes with the $model's var
		foreach ( $this->attrs_map as $attribute ) {
			$model->$attribute = $this->$attribute;
		}
		
		$c = get_called_class(); 
		
		
		$id =  $this->attrs_map[0];

		// If id exists, update the model
		// If id doesn't exist, insert a new row in database
		if( $model->$id == NULL || $model->$id == "" ){

			if (! $db->insertObject( $c::TABLE, $model ) ) {

				if( $return == 'string' ){
					return "No se pudo guardar el registro. " . $db->stderr();
				}

				if( $return == 'bool' ){
					return false;
				}

				if( $return == 'object' ){
					$response->status = false;
					$response->error = "No se pudo guardar el registro. " . $db->stderr();
					return $response;
				}
			}
			$this->insertId = $db->insertid();

			if( $return == 'string' ){
				return "";
			}

			if( $return == 'bool' ){
				return true;
			}

			if( $return == 'object' ){
				$response->status = true;
				$response->error = "";
				return $response;
			}
		}

		// Update
		if ( ! $db->updateObject( $c::TABLE, $model, $this->attrs_map[ 0 ], false ) ) {
			
			if( $return == 'string' ){
				return "No se pudo actualizar el object. " . $db->stderr();
			}

			if( $return == 'bool' ){
				return false;
			}

			if( $return == 'object' ){
				$response->status = false;
				$response->error = "No se pudo guardar el object. " . $db->stderr();
				return $response;
			}
		}

		if( $return == 'string' ){
			return "";
		}

		if( $return == 'bool' ){
			return true;
		}

		if( $return == 'object' ){
			$response->status = true;
			$response->error = "";
			return $response;
		}

	}
	
	/**
	 * Delete object from database
	 *
	 * @param { array || int } the id of the object or array that contains the id
	 * @return { bool/object } the object returned or false otherwise
	 */
	public function delete(){
		
		$firstkey = $this->attrs_map[ 0 ];
	
		if( ! is_numeric( $this->$firstkey ) )
			return false;
	
	
		// Delete existing object if the id was passed through
		$c = get_called_class();  

		$query = "DELETE FROM ". $c::TABLE ." WHERE ". $this->attrs_map[ 0 ] . " = " . $this->$firstkey;
		$db = JFactory::getDbo();
		$db->setQuery( $query );
	
		return $db->query();
	
	}

	/**
	 * Publish or unpublish the object
	 * @param { string } the type of the response
	 * ( string returns the error string or "" )
	 * ( bool return false or true )
	 * ( object returns the response with error_message, status true or false )
	 * @return { string  } the state after change
	 *
	 */
	public function publish( $return = 'string' ){

		$db = JFactory::getDbo();
		
		$firstkey = $this->attrs_map[ 0 ];
		$secondkey = $this->attrs_map[ 1 ];
		
		$this->$secondkey = ( $this->$secondkey == 1 ) ? 0 : 1;

		// Update
		$std = new stdClass();
		$std->$firstkey = $this->$firstkey;
		$std->$secondkey = $this->$secondkey;
		
		$c = get_called_class();  

		if (! $db->updateObject( $c::TABLE, $std, $this->attrs_map[ 0 ], false ) ) {
			
			if( $return == 'string' ){
				return "No se pudo actualizar el object. " . $db->stderr();
			}

			if( $return == 'bool' ){
				return false;
			}

			if( $return == 'object' ){
				$response->status = false;
				$response->error = "No se pudo actualizar el object. " . $db->stderr();
				return $response;
			}
		}

		if( $return == 'string' ){
			return "";
		}

		if( $return == 'bool' ){
			return true;
		}

		if( $return == 'object' ){
			$response->status = true;
			$response->error = "";
			return $response;
		}

	}
	
	/**
	 * Build a query for collection. Filters query are included.
	 *
	 * @param { array } wheres clausule. Clausule must be { key: 'value', value: 'value', condition:'=', glue: 'AND || OR' }
	 * @return { string } the query string calling the collection
	 *
	 */
	protected function buildQuery( $wheres = NULL , $params = array() ){

		// Validation
		if( ! is_array( $wheres ) )
			$wheres = array();

		// Get a storage key.
		$store = $this->getStoreId();

		// Initialize
		$db = JFactory::getDbo();
		$query  = $db->getQuery(true);
		
		$c = get_called_class();  
		
		// Query base		
		$query->select( "*" );
		$query->from( $c::TABLE );

		
		// Wheres appending
		foreach ( $wheres as $key => $clausule ) {
			
			if( ! is_object( $clausule ) )
				continue;

			$query->where( $db->quoteName($clausule->key) . $clausule->condition . $db->quote($clausule->value) . ' '.trim($clausule->glue).' ' );

		}
		
		//query order
		if( isset($params[ 'order' ]) ){
			$query->order( $params[ 'order' ] );
		}else{
			// Add the list ordering clause.
			$query->order($db->escape($this->getState('list.ordering', 'id_log_session')).' '.$db->escape($this->getState('list.direction', 'DESC')));
		}
		
		//query group	
		if( isset($params['group']) )
			$query->group( $params[ 'group' ] );

		//Filters appending

		//filter for id user
		$idusuario  = $this->getState('filter.id_usuario');
		if( is_numeric($idusuario) ){
			$query->where( $db->quoteName( 'id_usuario' ) .' = '. $db->quote( $idusuario ) );
		}

		//filter for name user
		$nombreusuario = $this->getState('filter.nombre_usuario');
		if( !empty($nombreusuario) ){
			$query->where( $db->quoteName( 'nombre_usuario' ) .' LIKE '. $db->quote('%'.$db->escape($nombreusuario).'%') );
		}

		//filter for range date
		$patternDate = '/\d{1,2}\-\d{1,2}\-\d{2,4}(\s)\d{1,2}\:\d{1,2}/';

		$fecha_acceso = $this->getState('filter.fecha_acceso');

		$res_regexp = preg_match( $patternDate, $fecha_acceso); 
		if( !empty($fecha_acceso) && $res_regexp ){

			$fecha_acceso = date( $db->getDateFormat(), strtotime($fecha_acceso) );
			$query->where( $db->quoteName( 'fecha_acceso' ) .' >= '. $db->quote( $fecha_acceso ) );
		}

		$fecha_cierre = $this->getState('filter.fecha_cierre');
		$res_regexp = preg_match( $patternDate, $fecha_cierre); 
		if( !empty($fecha_cierre) && $res_regexp ){

			$fecha_cierre = date( $db->getDateFormat(), strtotime($fecha_cierre) );
			$query->where( $db->quoteName( 'fecha_cierre' ) .' <= '. $db->quote( $fecha_cierre ) );
		}

		//var_dump($query->__toString() );
		
		return $query;
	}
	
	/**
	* generar Paginador
	*/
	public function getPagination(){
		// Load the content if it doesn't already exist
        jimport('joomla.html.pagination');
        $_pagination = new JPagination( $this->counts , $this->getState('list.start'), $this->getState('list.limit') );

        return $_pagination;
	}
	
	 /*
	 * obtener usuario que creo o el que actualizo el registro
	 * @param { array } un array de objetos con las consultas
	 * @return { object , array } objeto o arrray de objectos
	 */
	 public function getUserCreate( $wheres = NULL ){
		 
		 if( ! is_array( $wheres ) )
		 	$wheres = array();
		 
		 $db = JFactory::getDbo();
		 $query = $db->getQuery(true);
		 
		 $query->select('*');
		 $query->from( '#__users' );
		 
		 $query->where( 'id = '. $this->created_by );
		 
		 foreach( $wheres as $key=>$where ){
			 
			 if( !is_object( $where ) )
			 	continue;
			 
			 $query->where( $where->key . $where->condition . $where->value . $where->glue );
		 }
		 
		 $db->setQuery( $query );
		 
		 
		return $db->loadObject();
	 }

	/**
	 * API of the class
	 * 
	 * @return { void }
	 */
	protected function API(){
		
		// Instance an object with defaults
		$Object = new ComponentModelObject();
		
		// Instance an object with args not ID
		$args = array(
				'attribue'	=> 'any'
		);
		
		$Object = new ComponentModelObject();
		$Object->instance( $args );
		$Object->save(); // saves a new item
		
		//---------------
		// Instance an object with args with the ID
		// Result will be the model from DB merge the fields passed
		$args = array(
					'id' 		=> 1
				,	'tema' 		=> 1
				,	'titulo'		=> 'example'
				,	'ruta' 		=> 'pdf_test.pdf'
				,	'ano'		=> '2013'
				,	'state'		=>	1 // state pusblished
		);
		
		$Object = new ComponentModelObject();
		$Object->instance( $args );
		$Object->save(); // It will update the object with the id
		$Object->delete(); // It will delete the object with the id

		// Instance an object with args with the ID		
		$Object = new ComponentModelObject( 1 );

		// To get the objects with the table
		$Object = new ComponentModelObject();
		$object->getObjects();


		// To get the objects with conditions
		$wheres = array(
			0 => ( object ) array(
					'key' => 'id'
				,	'value' => '3'
				,	'condition' => '='
				,	'glue' => 'AND'
			)
		);

		$Object = new ComponentModelObject();
		$Object->getObjects( $wheres );
	}

}