<?php
/**
 * @package LeoClock for Joomla 2.5
 * @version 1.2.5
 * @author Hampus Jensen (http://leonick.se)
 * @copyright (C) 2012-2014 Hampus Jensen
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
 
// Include the syndicate functions only once
if(!defined('DS')){
    define('DS',DIRECTORY_SEPARATOR);
}
require_once( dirname(__FILE__).DS.'helper.php' );
 
$time = modLeoClockHelper::getTime( $params );
$format = $params->get('format');
$seconds = $params->get('seconds');
$date = $params->get('date');
$leadingZeros = $params->get('leadingZeros');
$outTimezone = modLeoClockHelper::getOutputTimezone( $params );
require( JModuleHelper::getLayoutPath( 'mod_leoclock' ) );
?>