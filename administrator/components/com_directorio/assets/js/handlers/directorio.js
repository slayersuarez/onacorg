/**
*
* Handler for { Directorio }
*
*
**/

( function( $, window, document, controller, Utilities ){
	
	var DirectorioHandler = function( a ){
		
	};
	
	DirectorioHandler.prototype ={
			
				initialize: function(){

					this.parseExcel();
					this.createUploader();
				}

				/**
				 * Function header when calls the model method and gives the view the response
				 *
				 * @param {}
				 * @return {}
				 */
			,	parseExcel: function(){

					$( '#importar-datos' ).click( function( e ){
						
						e.preventDefault();

						var data = {

							type: $( '#select-tipo_organismo' ).find( ':selected' ).val()
						}

						if( controller.excels.length <= 0 ){
							alert( 'Aún no ha cargado ningún archivo excel.' );
							return;
						}

						controller.parseExcel( data );
						//controller.truncate( data );
					});

				
				}

				/**
				* Creates the uploader when slect option changes
				*
				*/
			,	createUploader: function(){

					$( '#select-tipo_organismo' ).change( function( e ){

						console.log('selected');
						window.Uploader.createUploaderPDF();

					});
				}
		
			
	};
	

	// Use this way when script is loaded at the end of the page
	// Expose to global scope
	$( document ).ready( function( e ){

		window.DirectorioHandler = new DirectorioHandler();
		window.DirectorioHandler.initialize();
	});
	
})( jQuery, this, this.document, this.DirectorioController, this.Misc, undefined );