
<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );

// Begining of the controller
class GlosarioControllerLista extends JController{
	
	
	/**
	 *  Task that executes the list of Informes Seguimiento
	 */
	public function show(){
		
		// Call the view
		$view =& $this->getView( 'lista', 'html' );
		
		// Call the model
		$model =& $this->getModel( 'termino' );
		
		// Get all the Informes Seguimiento
		$terminos = $model->getObjects();
		
		// Assign terminos as reference to the view
		$view->assignRef( 'terminos', $terminos );
		
		// Display the view
		$view->display();
	}

	/**
	 *  Task that deletes the list of terminos
	 */
	public function delete(){
		
		//Het the items picked
		$pks = JRequest::getVar( 'cid' );

		//Instance a model for each item and delete
		foreach ( $pks as $id ) {
			
			// Call the model
			$model = $this->getModel( 'termino' );
			
			$model->instance( $id );

			$model->delete();
			
		}

		$this->setRedirect( "index.php?option=com_glosario" );

		return;
	}

	public function buscar(){


		$modelo = $this->getModel( 'termino' );

		$modelo->setParams();

		$this->setRedirect( "index.php?option=com_glosario" );

		return;

	}

	public function limpiar(){


		$modelo = $this->getModel('termino');

		$modelo->cleanParams();

		$this->setRedirect( "index.php?option=com_glosario" );

		return;

	}
}
?>