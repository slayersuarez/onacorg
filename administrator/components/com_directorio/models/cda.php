<?php

/**
 * Model for "Lab1"
 * 
 */

// Joomla calls and runtimes
defined( '_JEXEC' ) or die();
jimport( 'joomla.application.component.model' );

// Initializes the Class
class DirectorioModelCda extends JModel {
	
	/**
	 * Object Id
	 * @var int
	 */
	var $id;

	/**
	 * Object identification
	 * @var string
	 */
	var $identificador;

	/**
	 * Clave
	 * @var string
	 */
	var $codigo_acreditacion;

	/**
	 * Termino: la palabra del diccionario
	 * @var string
	 */
	var $estado;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $observaciones;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $razon_social;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $nit;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $criterios_calificacion;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $nombre_establecimiento_comercial;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $numero_matricula_mercantil;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $direccion_sede_principal;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $ciudad;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $direccion_correspondencia;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $ciudad_correspondencia;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $departamento_correspondencia;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $pais_correspondencia;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $fijo;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $movil;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $fax;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $pagina_web;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $representante_legal;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $documento_representante_legal;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $correo_representante;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $fijo_representante;


	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $movil_representante;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $fax_representante;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $representante_onac;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $representante_onac_identificacion;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $representante_onac_correo;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $representante_onac_direccion;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $representante_onac_ciudad;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $representante_onac_fijo;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $representante_onac_movil;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $representante_onac_fax;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $numero_personas_oec_empleado;


	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $numero_personas_oec_externo;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $denominacion_unidad;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $unidad_direccion;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $unidad_ciudad;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $unidad_responsable;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $unidad_responsable_cargo;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $unidad_responsable_correo;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $unidad_responsable_fijo;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $unidad_responsable_movil;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $unidad_responsable_fax;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $dias_evaluacion_vigilancia;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $dias_evaluacion_renovacion;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $fecha_otorgamiento_sitio_cubierto;


	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $fecha_vencimiento_sitio_cubierto;


	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $fecha_renovacion_sitio_cubierto;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $fecha_ultima_modificacion_sitio_cubierto;


	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $numero_sitios_cubiertos_acreditacion;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $direccion_sitio_cubierto;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $ciudad_sitio_cubierto;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $departamento_sitio_cubierto;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $lineas_sitio_cubierto;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $pistas_sitio_cubierto;


	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $capacidad_sitio_cubierto;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $alcance_acreditacion_aprobado;


	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $documento_normativo;


	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $PDF;

	
	/**
	 * Get if the object exists
	 * @var bool
	 */
	var $exists = false;
	
	/**
	 * Cache of results, data, queries
	 * @var unknown
	 */
	var $data;

	/**
	 * Filters array
	 * @var unknown
	 */
	var $filters = array();

	/**
	 * Table var
	 * @var unknown
	 */
	var $table = '#__organismo_cda';

	/**
	 * Model Name
	 * @var string
	 */
	var $type = 'Cda';

	/**
	* auto-incremented value from the last INSERT statement.
	* @var int
	*/
	var $insertid = NULL;

	/**
	 * Constant for table
	 * @var string
	 */
	const TABLE = '#__organismo_cda';

	/**
	 * Constant for filters states
	 * @var string
	 */
	const FILTER_STATE = 'filter.cda.';

	
	/**
	 * Attributes Map
	 * @var array
	 */
	var $attrs_map = array(
			'id'
		,	'identificador'	
		,	'codigo_acreditacion'
		,	'estado'
		,	'observaciones'
		,	'razon_social'
		,	'nit'
		,	'criterios_calificacion'
		,	'nombre_establecimiento_comercial'
		,	'numero_matricula_mercantil'
		,	'direccion_sede_principal'
		,	'ciudad'
		,	'direccion_correspondencia'
		,	'ciudad_correspondencia'
		,	'departamento_correspondencia'
		,	'pais_correspondencia'
		,	'fijo'
		,	'movil'
		,	'fax'
		,	'pagina_web'
		,	'representante_legal'
		,	'documento_representante_legal'
		,	'correo_representante'
		,	'fijo_representante'
		,	'movil_representante'
		,	'fax_representante'
		,	'representante_onac'
		,	'representante_onac_identificacion'
		,	'representante_onac_correo'
		,	'representante_onac_direccion'
		,	'representante_onac_ciudad'
		,	'representante_onac_fijo'
		,	'representante_onac_movil'
		,	'representante_onac_fax'
		,	'numero_personas_oec_empleado'
		,	'numero_personas_oec_externo'
		,	'denominacion_unidad'
		,	'unidad_direccion'
		,	'unidad_ciudad'
		,	'unidad_responsable'
		,	'unidad_responsable_cargo'
		,	'unidad_responsable_correo'
		,	'unidad_responsable_fijo'
		,	'unidad_responsable_movil'
		,	'unidad_responsable_fax'
		,	'dias_evaluacion_vigilancia'
		,	'dias_evaluacion_renovacion'
		,	'fecha_otorgamiento_sitio_cubierto'
		,	'fecha_vencimiento_sitio_cubierto'
		,	'fecha_renovacion_sitio_cubierto'
		,	'fecha_ultima_modificacion_sitio_cubierto'
		,	'numero_sitios_cubiertos_acreditacion'
		,	'direccion_sitio_cubierto'
		,	'ciudad_sitio_cubierto'
		,	'departamento_sitio_cubierto'
		,	'lineas_sitio_cubierto'
		,	'pistas_sitio_cubierto'
		,	'capacidad_sitio_cubierto'
		,	'alcance_acreditacion_aprobado'
		,	'documento_normativo'
		,	'PDF'
	);	

	/**
	 * Natural Names
	 * @var array
	 */
	var $natural_names = array(
			'codigo_acreditacion' => 'Código de Acreditación'
		,	'estado' => 'Estado'
		,	'observaciones' => 'Observaciones'
		,	'razon_social' => 'Nombre de la razón social'
		,	'nit' => 'NIT'
		,	'criterios_calificacion' => 'Criterios de calificación'
		,	'direccion_sede_principal' => 'Dirección sede principal'
		,	'ciudad' => 'Ciudad'
		,	'fijo' => 'Fijo'
		,	'fax' => 'Fax'
		,	'pagina_web' => 'Página Web'
		,	'fecha_otorgamiento_sitio_cubierto' => 'Fecha Otorgamiento'
		,	'fecha_vencimiento_sitio_cubierto' => 'Fecha Vencimiento'
		,	'direccion_sitio_cubierto' => 'Dirección'
		,	'ciudad_sitio_cubierto' => 'Ciudad'
		,	'departamento_sitio_cubierto'
		,	'lineas_sitio_cubierto' => 'Lineas'
		,	'pistas_sitio_cubierto' => 'Pistas'
		,	'capacidad_sitio_cubierto' => 'Capacidad'
		,	'alcance_acreditacion_aprobado' => 'Alcance de la acreditación aprobado'
		,	'documento_normativo' => 'Documento normativo'
		,	'PDF' => 'PDF'
	);

	/**
	* Parametros
	*/
	var $parametros = array(
			'direccion_sitio_cubierto'
		,	'ciudad_sitio_cubierto'
		,	'lineas_sitio_cubierto'
		,	'alcance_acreditacion_aprobado'
		,	'documento_normativo'
	);
	
	
	/**
	 * Methods
	 * 
	 */
	
	/**
	 * Constructor
	 * 
	 * @param { array || int } the args to instance the model or the single id
	 * 
	 */
	public function instance( $config = NULL ){
		
		if( is_numeric( $config ) )
			$config = array( 'id' => $config );
		
		if( ! is_array( $config ) )
			return;
		
		// Get existing object if the id was passed through
		return $this->fill( $config );
	}
	
	/**
	 * Fill the model attributes with the passed arguments.
	 *
	 * @param { arr } Object arguments
	 */
	protected function fill( $args = null ){


		if ( ! is_array( $args ) )
			return false;
		
	
		// Get object in DB			
		if ( is_numeric( $args[ 'id' ] ) ){

			$object = $this->getObject( $args[ 'id' ] );

			foreach ( $this->attrs_map as $attr ) {
				
				if ( isset( $object->$attr ) )
					$this->$attr = $object->$attr;
			}

		}

	
		// Merge attributes	when id is not passed through
		foreach ( $this->attrs_map as $attr ) {
			if ( isset( $args[ $attr ] ) )
				$this->$attr = $args[ $attr ];
		}

		// Set exists to true.
		$this->exists = true;
	
	}
	
	
	/**
	 * Get a single Object
	 * 
	 * @param { int } the id or attributes of the Object.
	 * @return { bool/object } the object returned or false otherwise
	 */
	protected function getObject( $id = 1 ){
		
		if( ! is_numeric( $id ) )
			return false;
		
		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );

		$query->select( '*' );
		$query->from( self::TABLE );
		$query->where( 'id = ' . $id );
		$db->setQuery( $query );
		
		return $db->loadObject();
		
	}
	
	/**
	 * Get Objects collection
	 *
	 * @param { int } the id or attributes of the Object.
	 * @return { bool/object } the object returned or false otherwise
	 **/
	public function getObjects( $wheres = NULL ){
		
		$result = array();
		
		if( ! is_array( $wheres ) )
			$wheres = array();
		
		// Verifies if data already contains a collection
		if( empty( $this->data ) ){
			
			$query = $this->buildQuery( $wheres );
			$this->data = $this->_getList( $query );
		}

		
		foreach ( $this->data as $obj ){

			$args = array();

			foreach ( $this->attrs_map as $key => $objattr ) {
				
				$args[ $objattr ] = $obj->$objattr;
			}
			
			$object = new DirectorioModelCda();
			$object->fill( $args );
			array_push( $result, $object );
			
		}
	
		return $result;
	
	}

	/**
	 * Save a new object or update an exist.
	 *
	 * @param { string } the type of the response
	 * ( string returns the error string or "" )
	 * ( bool return false or true )
	 * ( object returns the response with error_message, status true or false )
	 * @return { string  } the query string calling the insert query
	 *
	 */
	public function save( $return = 'string' ){

		// Initialize
		$db = JFactory::getDbo();
		$response = ( object ) array();
		$model = ( object ) array();

		if( ! is_string( $return ) )
			$return = 'string';

		// Fetch the model attributes with the $model's var
		foreach ( $this->attrs_map as $attribute ) {
			$model->$attribute = $this->$attribute;
		}

		// If id exists, update the model
		// If id doesn't exist, insert a new row in database
		if( $model->id == NULL || $model->id == "" ){

			if (! $db->insertObject( self::TABLE, $model ) ) {

				if( $return == 'string' ){
					return "No se pudo guardar el object. " . $db->stderr();
				}

				if( $return == 'bool' ){
					return false;
				}

				if( $return == 'object' ){
					$response->status = false;
					$response->error = "No se pudo guardar el object. " . $db->stderr();
					return $response;
				}
			}

			$this->insertid = $db->insertid();
			if( $return == 'string' ){
				return "";
			}

			if( $return == 'bool' ){
				return true;
			}

			if( $return == 'object' ){
				$response->status = true;
				$response->error = "";
				return $response;
			}
		}

		// Update
		if ( ! $db->updateObject( self::TABLE, $model, 'id', false ) ) {
			
			if( $return == 'string' ){
				return "No se pudo actualizar el object. " . $db->stderr();
			}

			if( $return == 'bool' ){
				return false;
			}

			if( $return == 'object' ){
				$response->status = false;
				$response->error = "No se pudo guardar el object. " . $db->stderr();
				return $response;
			}
		}

		if( $return == 'string' ){
			return "";
		}

		if( $return == 'bool' ){
			return true;
		}

		if( $return == 'object' ){
			$response->status = true;
			$response->error = "";
			return $response;
		}

	}
	
	/**
	 * Delete object from database
	 *
	 * @param { array || int } the id of the object or array that contains the id
	 * @return { bool/object } the object returned or false otherwise
	 */
	public function delete(){
	
		if( ! is_numeric( $this->id ) )
			return false;
	
	
		// Delete existing object if the id was passed through

		$query = "DELETE FROM ". self::TABLE ." WHERE id = $this->id";
		$db = JFactory::getDbo();
		$db->setQuery( $query );
	
		return $db->query();
	
	}


	/**
	 * Truncates table from database
	 *
	 * @param { array || int } the id of the object or array that contains the id
	 * @return { bool/object } the object returned or false otherwise
	 */
	public function truncate(){
	
		// Restart the table, clean the rows and restart id = 0
		$query = "TRUNCATE TABLE ". self::TABLE;
		$db = JFactory::getDbo();
		$return = false;

		try{

			$db->transactionStart();

			$db->setQuery( $query );
			$return = $db->execute();

			$db->transactionCommit();

		}catch( Exception $e ){

			// catch any database errors.
		    $db->transactionRollback();
		    return false;
		    //JErrorPage::render($e);
		}

		return $return;
	
	}

	/**
	 * Get the params from params array definition
	 * @return { array } the array of params and their values
	 */
	public function getParametros(){
	
		if( ! is_array( $this->parametros ) )
			return false;


		$db = JFactory::getDbo();
		$query = $db->getQuery( true );
		$select = '';

		foreach ( $this->parametros as $key => $parametro ) {
			
			$select .= $parametro . ', ';

		}
		
		$select .= rtrim( $select, ', ' );
	
		// Delete existing object if the id was passed through
		$query->select( $select );
		$query->from( self::TABLE );
		$db->setQuery( $query );
	
		$list = $db->loadObjectList();

		$parametros = array();

		// Initialize parametros values as array
		foreach ( $this->parametros as $key => $param ) {
			$parametros[ $param ] = array();
		}

		//var_dump( $parametros );

		// Recover params values
		foreach ( $list as $key => $params_row ){
			
			foreach ( $params_row as $_key => $_parametro ) {

				//var_dump( $_key );

				array_push( $parametros[ $_key ], $_parametro );
			}
		}


		// Unique values
		foreach ( $parametros as $key => $value) {
			
			$parametros[ $key ] = array_unique( $parametros[ $key ] );
		}


		return $parametros;
	
	}

	// Get natural name for key
	public function getNaturalKey( $key = NULL ){

		if( ! is_string( $key ) ){
			return $key;
		}


		if( ! array_key_exists( $key, $this->natural_names ) ){
			return $key;
		}

		return $this->natural_names[ $key ];
	}

	/**
	* Get all the cities in unique array
	*
	*/
	public function getCities(){

		$db = JFactory::getDbo();
		$query = $db->getQuery( true );

		// Select the cities
		$query->select( 'ciudad' );
		$query->from( self::TABLE );

		$db->setQuery( $query );

		$cities = $db->loadResultArray();

		if( empty( $cities ) ){
			return array();
		}

		$cities = array_unique( $cities );

		return $cities;

	}
	
	// Helpers for the same Model
	// Next methods are utilities no core methods, therefore they all are protected
	
	public function getParams(){

		$mainframe =& JFactory::getApplication();

		foreach ( $this->attrs_map as $key => $param ) {

			$_param = ( object ) array();
			$_param->key = $param;
			$_param->value = $mainframe->getUserState( self::FILTER_STATE . $param );
			$_param->type = gettype( $_param->value );
			array_push( $this->filters, $_param );
		}
	}


	public function setParams(){

		$mainframe =& JFactory::getApplication();

		foreach ( $this->attrs_map as $key => $param ) {

			$mainframe->setUserState( self::FILTER_STATE . $param, JRequest::getVar( $param ) );
		}

		$this->getParams();
	}

	public function cleanParams(){

		$mainframe =& JFactory::getApplication();

		foreach ( $this->attrs_map as $key => $param ) {

			$mainframe->setUserState( self::FILTER_STATE . $param, " " );
		}

		$this->getParams();
	}
	
	/**
	 * Build a query for collection. Filters query are included.
	 *
	 * @param { array } wheres clausule. Clausule must be { key: 'value', value: 'value', condition:'=', glue: 'AND || OR' }
	 * @return { string } the query string calling the collection
	 *
	 */
	protected function buildQuery( $wheres = NULL ){

		// Validation
		if( ! is_array( $wheres ) )
			$wheres = array();

		// Initialize
		$db = JFactory::getDbo();
		$query  = $db->getQuery(true);

		// Get the filters
		$this->getParams();
		
		// Query base		
		$query->select( "*" );
		$query->from( self::TABLE);

		
		// Wheres appending
		foreach ( $wheres as $key => $clausule ) {
			
			if( ! is_object( $clausule ) )
				break;	

			$query->where( $clausule->key . $clausule->condition . $clausule->value, $clausule->glue );

		}


		// Filters appending
		/*foreach ( $this->filters as $key => $filter ) {

			if( ! empty( $filter->value ) ){
				if( $filter->type == 'string' ){
					$query->where( $filter->key . "like '%{ $filter->value }%'" );
					break;
				}
			
				$query->where( $filter->key . " = " . $filter->value );	
			}
		}*/
		
		return $query;
	}

	/**
	* combina array de atributos con array de campos de hoja excel para crear array clave/valor
	*/
	public function combineFields(){

		$alphaIni = range( 'A', 'Z' );
		foreach( $alphaIni as $valor ){
			
			foreach( range( 'A','Z' ) as $letter ){
				
				$alphaIni[] = $valor.$letter;
			}
			unset($letter);
				
		}
		$attrs_map = array_slice( $this->attrs_map, 1);
		$alphaIni = array_slice( $alphaIni, 0, count($attrs_map) );
		$fields_combine = array_combine( $attrs_map, $alphaIni );

		return $fields_combine;
	}

	/**
	 * API of the class
	 * 
	 * @return { void }
	 */
	
	protected function API(){
		
		// Instance an object with defaults
		$Object = new DirectorioModelCda();
		
		// Instance an object with args not ID
		$args = array(
				'attribue'	=> 'any'
		);
		
		$Object = new DirectorioModelCda();
		$Object->instance( $args );
		$Object->save(); // saves a new item
		
		//---------------
		// Instance an object with args with the ID
		// Result will be the model from DB merge the fields passed
		$args = array(
					'id' 			=> 1
				,	'termino' 		=> 'sample'
				,	'descripcion'	=> 'example'
				,	'clave' 		=> 'E'
		);
		
		$Object = new DirectorioModelCda();
		$Object->instance( $args );
		$Object->save(); // It will update the object with the id
		$Object->delete(); // It will delete the object with the id

		// Instance an object with args with the ID		
		$Object = new DirectorioModelCda( 1 );

		// To get the objects with the table
		$Object = new DirectorioModelCda();
		$object->getObjects();


		// To get the objects with conditions
		$wheres = array(
			0 => ( object ) array(
					'key' => 'id'
				,	'value' => '3'
				,	'condition' => '='
				,	'glue' => 'AND'
			)
		);

		$Object = new DirectorioModelCda();
		$Object->getObjects( $wheres );
		
		
	}
}
?>