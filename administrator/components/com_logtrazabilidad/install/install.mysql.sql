
-- -----------------------------------------------------
-- Table `log_sessions_user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `log_sessions_user` (
  `id_log_session` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL DEFAULT '0',
  `nombre_usuario` varchar(200) DEFAULT NULL,
  `fecha_acceso` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `fecha_cierre` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `duracion` float(10,2) NOT NULL DEFAULT '0.00' COMMENT 'en minutos',
  `tipo_log` char(1) NOT NULL COMMENT 'cierre = 2 o apertura = 1',
  `tipo_user` varchar(100) DEFAULT NULL COMMENT ' “backend o frontend”',
  PRIMARY KEY (`id_log_session`),
  KEY `id_usuario` (`id_usuario`)
) ENGINE=InnoDB;

-- -----------------------------------------------------
-- Table `log_carga_organismos`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `log_carga_organismos` (
  `id_log_acredita` int(11) NOT NULL AUTO_INCREMENT,
  `tabla_organismo` varchar(200) DEFAULT NULL COMMENT 'nombre de la tabla del organismo',
  `identificador` varchar(200) NOT NULL COMMENT 'identificador de registro de autenticacion',
  `id_organismo` int(11) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `tipo_gestion` varchar(150) DEFAULT NULL COMMENT 'nuevo, editado, borrado',
  `descripcion_actividad` text,
  `razon_social_oec` varchar(200) DEFAULT NULL,
  `nit_oec` varchar(200) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `parametros` text,
  PRIMARY KEY (`id_log_acredita`),
  KEY `tabla_organismo` (`tabla_organismo`,`id_organismo`,`id_usuario`,`tipo_gestion`),
  KEY `identificador` (`identificador`)
) ENGINE=InnoDB;