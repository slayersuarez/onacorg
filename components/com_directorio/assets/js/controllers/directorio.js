/**
*
* Controller for { Directorio }
*
*
**/

( function( $, window, document, model, view, Utilities ){
	
	var DirectorioController = function( a ){
		
		// atributes, selector or global vars

		
	};
	
	DirectorioController.prototype ={
			

			/**
			 * Calls the view method to adjust the tooltips positions
			 *
			 */
			setTooltip: function(){
				
				view.setTooltip();
			}

			/** 
			* call the model and get the params according to the tipo de organismo selected.
			* Then passed the data to the view to render the results
			*
			* @param { string } tipo de organismo
			* @return { object } the data with the results
			*
			*/
		,	getParams: function( tipoOrganismo ){

				view.onProcess( '.wrapper-buscador-avanzado', 2000 );

				var success = function( data  ){

					view.onCompleteLoadParams( data );
				};


				return model.getParams( success, tipoOrganismo );
			}

			/**
			 * Call the model function and render to the view the response
			 * This method will manage the communication between view and model
			 *
			 */
		,	search: function( data, page ){

				view.onProcess( '.wrapp-lista-content-organismos', 1000 );
				
				var success = function( data ){

					 view.onCompleteSearch( data );
				};

				return model.search( success, data, page );
			}

		
			
	};
	

	// Use this way when script is loaded at the end of the page
	// Expose to global scope
	window.DirectorioController = new DirectorioController();
	
})( jQuery, this, this.document, this.DirectorioModel, this.DirectorioView, this.Misc, undefined );