<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	com_logtrazabilidad
 */

// No direct access.
defined('_JEXEC') or die;
jimport( 'joomla.environment.uri' );

// Load the tooltip behavior.
//JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');
//JHtml::_('behavior.modal');
JHtml::_('behavior.framework');
$doc = JFactory::getDocument();

$doc->addScriptDeclaration( '
		var url = "'.JURI::base().'"
	' );

$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));

$canDo = LogTrazabilidadHelper::getActions();
$user = JFactory::getUser();
$loggeduser = JFactory::getUser();

?>

<form action="<?php echo JRoute::_('index.php?option=com_logtrazabilidad&view=logsession');?>" method="post" name="adminForm" id="adminForm">
	<fieldset id="filter-bar">
		<div class="filter-search fltlft">
			<label class="filter-search-lbl" for="id_usuario">Id de usuario:</label>
			<input type="text" name="id_usuario" id="id_usuario" value="<?php echo $this->state->get( 'filter.id_usuario' );?>">
			<label class="filter-search-lbl" for="nombre_usuario">Nombre de usuario:</label>
			<input type="text" name="nombre_usuario" id="nombre_usuario" value="<?php echo $this->state->get( 'filter.nombre_usuario' );?>">
		</div>
		<div class="filter-select fltlft">
			<label class="filter-search-lbl" for="fecha_acceso">Fecha y hora de inicio de sessión:</label>
			<input type="text" name="fecha_acceso" id="fecha_acceso" class="plg-datatime" readonly="readonly" value="<?php echo $this->state->get( 'filter.fecha_acceso' );?>">

			<label class="filter-search-lbl" for="fecha_cierre">Fecha y hora de cierre de sessión:</label>
			<input type="text" name="fecha_cierre" id="fecha_cierre" class="plg-datatime" readonly="readonly" value="<?php echo $this->state->get( 'filter.fecha_cierre' );?>">
			
		</div>
		<div class="filter-search fltrt">
			<button type="submit"><?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?></button>
			<button type="button" class="limpiar-filtros" ><?php echo JText::_('Limpiar'); ?></button>
		</div>
	</fieldset>
	<div class="clr"> </div>

	<table class="adminlist">
		<thead>
			<tr>
				<th width="1%">
					<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
				</th>
				<th class="nowrap">
					<?php echo JHtml::_('grid.sort', 'Id del usuario', 'id_usuario', $listDirn, $listOrder); ?>						
				</th>
				<th class="nowrap">
					Nombre del usuario
				</th>
				<th class="nowrap">
					Fecha y hora &#47; inicio de sesión
				</th>
				<th class="nowrap">
					Fecha y hora &#47; cierre de sesión.
				</th>
				<th class="nowrap">
					<?php echo JHtml::_('grid.sort', 'Duración de la sesión (Min)', 'duracion', $listDirn, $listOrder); ?>					
				</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="7">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
		<?php /*fb( $this->items );*/ foreach ($this->items as $i => $item) :

			// si no es super usuario, no puede ver los registros
			if ( $canDo->get('core.admin') ) {
		?>
			<tr class="row<?php echo $i % 2; ?>">
				<td class="center">
					<?php echo JHtml::_('grid.id', $i, $item->id_log_session); ?>
				</td>
				<td class="center"><?php echo $item->id_usuario?></td>
				<td class="center"><?php echo ucwords($item->nombre_usuario)?></td>
				<td class="center"><?php echo $item->fecha_acceso?></td>
				<td class="center"><?php echo $item->fecha_cierre?></td>
				<td class="center"><?php echo $item->duracion?></td>
			</tr>
		<?php 
			}
		endforeach; 
		?>
		</tbody>
	</table>

	<div>
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
