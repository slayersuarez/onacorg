/**
*
* Event Handler for { Glosario }
*
*
**/

( function( $, window, document, controller, Utilities ){
	
	var GlosarioHandler = function( a ){
		
		// atributes, selector or global vars
		this.keyButton = '.alphabet-letter';
		this.searchButton = '.search-term-button';

		
	};
	
	GlosarioHandler.prototype ={
			
			/**
			 * Inializes the functions when DOM ready
			 */	
			initialize: function(){
				
				this.setTooltip();
				this.searchByKey();
				this.searchByEnter();
				this.searchByClick();
			}

			/**
			 * Finds a term by a letter
			 *
			 */
		,	setTooltip: function(){
			
				controller.setTooltip();
			}

			/**
			* Search a term by clave
			*
			*/
		,	searchByClick: function(){

				var _this = this;

				$( this.searchButton ).click( function( e ){

					e.preventDefault();

					_this.searchByTerm();
				});

			}

			/**
			* Search a term by clave
			*
			*/
		,	searchByEnter: function(){

				var _this = this;

				$( document ).keypress( function( e ){

					var key = e.which;

					if( key == 13 ){

						e.preventDefault();
						_this.searchByTerm();

					}
				});

			}

			/**
			* Search a term by clave
			*
			*/
		,	searchByKey: function(){

				var _this = this;

				$( this.keyButton ).click( function( e ){

					e.preventDefault();

					$( _this.keyButton ).removeClass( 'active' );
					$( this ).addClass( 'active' );

					var clave = $( this ).text();

					controller.searchByKey( clave );
				});

			}

			/**
			* Search a term by any word
			*
			*/
		,	searchByTerm: function(){

				var data = Utilities.formToJson( '#buscador-glosario' )
					,	required = [ 'termino' ]
					,	errors = [];

				Utilities.validateEmptyFields( required, data, errors );

				if( errors.length > 0 ){

					Utilities.showNotification( 'error', 'Escribe una palabra para realizar la consulta', 5000 );
					return;
				}

				controller.searchBy( data );

			}
		
			
	};
	
	// Use this when the script is loaded in <head> tag
	$( document ).ready( function(){
		// Expose to global scope
		window.GlosarioHandler = new GlosarioHandler();
		window.GlosarioHandler.initialize();
	});
	
})( jQuery, this, this.document, this.GlosarioController, this.Misc, undefined );