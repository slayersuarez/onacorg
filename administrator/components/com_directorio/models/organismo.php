<?php

/**
 * Model for "Lab1"
 * 
 */

// Joomla calls and runtimes
defined( '_JEXEC' ) or die();
jimport( 'joomla.application.component.model' );

// Initializes the Class
class DirectorioModelOrganismo extends JModel {
	
	
	/**
	 * lab 1 Model
	 * @var bool
	 */
	var $model_lab1;

	/**
	 * lac 1 Model
	 * @var bool
	 */
	var $model_lac1;

	/**
	 * lcl Model
	 * @var bool
	 */
	var $model_lcl;

	/**
	 * cda Model
	 * @var bool
	 */
	var $model_cda;

	/**
	 * ocp Model
	 * @var bool
	 */
	var $model_ocp;

	/**
	 * cpr Model
	 * @var bool
	 */
	var $model_cpr;

	/**
	 * csg Model
	 * @var bool
	 */
	var $model_csg;


	/**
	 * oin Model
	 * @var bool
	 */
	var $model_oin;


	/**
	 * cepplus Model
	 * @var bool
	 */
	var $model_cepplus;

	/**
	 * cdf Model
	 * @var bool
	 */
	var $model_cdf;
	
	/**
	 * Get if the object exists
	 * @var bool
	 */
	var $exists = false;
	
	/**
	 * Cache of results, data, queries
	 * @var unknown
	 */
	var $data;


	/**
	* Models Attrs map
	*
	*/
	var $attrs_map = array(

			'model_lab1'
		,	'model_lac1'
		,	'model_lcl'
		,	'model_cda'
		,	'model_ocp'
		,	'model_cpr'
		,	'model_csg'
		,	'model_oin'
		,	'model_cepplus'
		,	'model_cdf'

	);


	/**
	* Instance
	*
	*/
	public function instance( $args = NULL ){

		if( ! is_array( $args ) )
			return NULL;


		foreach ( $this->attrs_map as $key => $attr ) {
			
			$this->$attr = $args[ $attr ];
		}
	}

	
	/**
	 * Build a query for collection. Filters query are included.
	 *
	 * @param { array } wheres clausule. Clausule must be { key: 'value', value: 'value', condition:'=', glue: 'AND || OR' }
	 * @return { string } the query string calling the collection
	 *
	 */
	protected function buildQuery( $wheres = NULL ){

		// Validation
		if( ! is_array( $wheres ) )
			$wheres = array();

		// Initialize
		$db = JFactory::getDbo();
		$query  = $db->getQuery(true);
		
		// Query base		
		$query->select( "*" );
		$query->from( self::TABLE);

		
		// Wheres appending
		foreach ( $wheres as $key => $clausule ) {
			
			if( ! is_object( $clausule ) )
				break;

			$query->where( $clausule->key . $clausule->condition . $clausule->value, $clausule->glue );

		}
		
		return $query;
	}

	/**
	* Create the tables into db
	*
	*/
	public function createTables(){


		// get the attributes map of the models
		foreach ( $this->attrs_map as $key => $model ) {
			
			$attributes = $this->$model->attrs_map;
			$table_attributes = '';
			$attributes_types = '';

			foreach ( $attributes as $key => $attr ) {
				
				if( $attr == 'id' ){
					$table_attributes .= $attr.' INT(10) AUTO_INCREMENT NOT NULL, ';
				} else{
					$table_attributes .= $attr . ' VARCHAR( 200 ), ';
				}
			}

			//$table_attributes = rtrim( $table_attributes, ', ' );

			$table = str_replace( '#__', 'jos_', $this->$model->table );

			// Create the table
			$sql = 'CREATE TABLE ' . $table . ' (' . $table_attributes . " PRIMARY KEY (id));<br><br><br><br>";

			echo $sql;
		}

	}

	/**
	* Get an unique array of cities
	*
	*/
	public function getCities(){

		$cities = array();

		// get the attributes map of the models
		foreach ( $this->attrs_map as $key => $model ) {
			
			$_cities = $model->getCities();

			$cities = array_merge( $cities, $_cities );
		}

		return $cities;
	}

	/**
	 * API of the class
	 * 
	 * @return { void }
	 */
	
	protected function API(){
		
	}
}
?>