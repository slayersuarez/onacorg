<?php
/**
* TorTags component for Joomla 1.6, Joomla 1.7, Joomla 2.5
* @package TorTags
* @author Zjmainstay
* @Copyright Copyright (C) Tormix, www.tormix.com
* @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
*
*/
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.plugin.plugin' );

if(!defined('_FirePHP')){
	define('_FirePHP', 1);
	require dirname(__FILE__) .'/FirePHPCore/fb.php';	
}

class plgSystemFirephp extends JPlugin
{
	function __construct(& $subject, $config)
	{
		parent::__construct($subject, $config);
		$this->loadLanguage('plg_system_firephp');
	}
}