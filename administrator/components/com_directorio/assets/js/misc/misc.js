/**
 * Utilities for seg component
 * 
 * 
 */
( function( $, window, document ){
	
	var Misc = function( a ){
		
		// attributes or global vars here
		
	};
	
	Misc.prototype = {
			
			/**
			 * Inializes the functions when DOM ready
			 */			
			initialize: function(){

				
			}
			
			/**
			 *  Serialize form into json format
			 *  
			 *  @param { string } name class or id of the html element to embed the loader
			 *  @return { object } form into json
			 *  
			 */
		,	formToJson: function( selector ){
			
				var o = {};
			    var a = $( selector ).serializeArray();
			    
			    $.each( a, function() {
			        if ( o[ this.name ] !== undefined ) {
			            if ( ! o[this.name].push ) {
			                o[ this.name ] = [ o[ this.name ] ];
			            }
			            
			            o[ this.name ].push( this.value || '' );
			            
			        } else {
			            o[ this.name ] = this.value || '';
			        }
			    });
			    
			    return o;	
				
			}
		
	       /**
	         * Helps in the process of making a ajax requests
	         *
	         * @param { object } Options for configuring the ajax request
	         * @param { object } data object to be sent
	         */
		,	ajaxHandler: function( options, data ) {
	
	             var result
	             ,   defaults = {
	                     type: 'post'
	                 ,   url: 'index.php'
	                 ,   data: data
	                 ,   async: false
	                 ,   success: function( data ) {
	                             result = data;
	                     }
	
	                 ,   error: function ( XMLHttpRequest, textStatus, errorThrown ) {
	                             console.log( "error :" + XMLHttpRequest.responseText );
	                             console.log(textStatus);
	                             console.log(errorThrown);
	                     }
	                 }
	
	             // Merge defaults and options
	             options = $.extend( {}, defaults, options );
	
	             // Do the ajax request
	             $.ajax( options );
	
	             // Return the response object
	             return result;
	
	        }

	        /**
            * Given an array of required fields, this function
            * checks whether the second argument have them
            */
        ,   validateEmptyFields: function( required, objectData, errors ) {


                $.each( required, function( key, value ) {

                    if ( objectData[ value ] == null || objectData[ value ] == "" ) {

                        errors.push( value );

                    }

                });

                return errors;

            }

            /**
			*
			* Validate only numbers
			* @param { string } the string to validate
			* 
			*/
		,	justNumbers: function( value ){

				var pattern = /^\d+$/
				, 	exp = new RegExp( pattern );


				if( typeof value == 'undefined' )
					return false;


				return exp.test( value );

			}

        	/**
            * Check whether an string is a correct email
            * @param { str } String to test
            * @return { bool }
            */
        ,   isEmail: function( string ) {

                var emailExpression = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

                return emailExpression.test( string );
            }

            /**
            * Sets a countdown
            *
            * @param { object } arguments to set: count, limit, selector, callback
            * @return { function } the callback passed, return otherwise
            *  
            */
        ,	setCountdown: function( args ){

				var counter=setInterval( reverse, 1000); //1000 will  run it every 1 second

				function reverse(){
					
					args.count = args.count - 1;

					if ( args.count <= args.limit	 ) {
						
						clearInterval( counter);

						if ( args.callback !== null ) {

                            args.callback.call();
                        }

						return;
					}

					$( args.selector ).text( args.count );
				}
        	}

    	,	showNotification: function( type, message, time ){

	    		$( '.notification' ).removeClass( 'error' );
	    		$( '.notification' ).removeClass( 'warning' );
	    		$( '.notification' ).removeClass( 'success' );
	    		$( '.notification' ).removeClass( 'info' );
	
	    		$( '.notification' ).addClass( type );
	    		$( '.notification' ).text( message );
	    		$( '.notification' ).fadeIn();
	    		$( 'body' ).css( 'opacity', '.5' );
	
	    		setTimeout( function(){ $( '.notification' ).fadeOut(); $( 'body' ).css( 'opacity', '1' ); }, time )

    	}
	};
		
	window.Misc = new Misc();
	window.Misc.initialize();
		
})( jQuery, this, this.document, undefined );