<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );


// Begining of the controller
class GlosarioControllertermino extends JController{
	
	/**
	* Save or update a register
	* Call the model passing args
	*
	*/
	public function save(){


		$args = array(
				'id' 		=> JRequest::getVar( 'id' )
			,	'termino' 	=> JRequest::getVar( 'termino' )
			,	'descripcion' => JRequest::getVar( 'descripcion' )
			,	'clave'		=> substr( JRequest::getVar( 'termino' ), 0, 1 )
		);


		// Get the model
		$model = $this->getModel( 'termino' );

		// Fill the model attributes with args
		$model->instance( $args );

		//save the model into DB
		$respuesta = $model->save();
		
	
		if( empty( $respuesta ) ){
				
			$this->setRedirect("index.php?option=com_glosario");
			return;
				
		}else{
				
			$this->setRedirect( "index.php?option=com_glosario&render=Edit", $respuesta , "error" );
			return;
				
		}
	
	}

	public function test(){

		echo 'hello world';
		die();
	}
}
?>