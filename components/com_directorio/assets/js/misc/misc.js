/**
 * Utilities for seg component
 * 
 * 
 */
( function( $, window, document ){
	
	var Misc = function( a ){
		
		// attributes or global vars here
		
	};
	
	Misc.prototype = {
			
			/**
			 * Inializes the functions when DOM ready
			 */			
			initialize: function(){

				
			}
			
			/**
			 *  Serialize form into json format
			 *  
			 *  @param { string } name class or id of the html element to embed the loader
			 *  @return { object } form into json
			 *  
			 */
		,	formToJson: function( selector ){
			
				var o = {};
			    var a = $( selector ).serializeArray();
			    
			    $.each( a, function() {
			        if ( o[ this.name ] !== undefined ) {
			            if ( ! o[this.name].push ) {
			                o[ this.name ] = [ o[ this.name ] ];
			            }
			            
			            o[ this.name ].push( this.value || '' );
			            
			        } else {
			            o[ this.name ] = this.value || '';
			        }
			    });
			    
			    return o;	
				
			}
		
	       /**
	         * Helps in the process of making a ajax requests
	         *
	         * @param { object } Options for configuring the ajax request
	         * @param { object } data object to be sent
	         */
		,	ajaxHandler: function( options, data ) {
	
	             var result
	             ,   defaults = {
	                     type: 'post'
	                 ,   url: 'index.php'
	                 ,   data: data
	                 ,   async: false
	                 ,   success: function( data ) {
	                             result = data;
	                     }
	
	                 ,   error: function ( XMLHttpRequest, textStatus, errorThrown ) {
	                             console.log( "error :" + XMLHttpRequest.responseText );
	                     }
	                 }
	
	             // Merge defaults and options
	             options = $.extend( {}, defaults, options );
	
	             // Do the ajax request
	             $.ajax( options );
	
	             // Return the response object
	             return result;
	
	        }

	        /**
            * Given an array of required fields, this function
            * checks whether the second argument have them
            */
        ,   validateEmptyFields: function( required, objectData, errors ) {


                $.each( required, function( key, value ) {

                    if ( objectData[ value ] == null || objectData[ value ] == "" ) {

                        errors.push( value );

                    }

                });

                return errors;

            }

            /**
			*
			* Validate only numbers
			* @param { string } the string to validate
			*
			*/
		,	justNumbers: function( value ){

				var pattern = /^\d+$/
				, 	exp = new RegExp( pattern );


				if( typeof value == 'undefined' )
					return false;


				return value.test( exp );

			}

        	/**
            * Check whether an string is a correct email
            * @param { str } String to test
            * @return { bool }
            */
        ,   isEmail: function( string ) {

                var emailExpression = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

                return emailExpression.test( string );
            }

            /**
            * Check whether an string is a 'Codigo de acreditacion'
            * @param { str } String to test
            * @return { bool }
            */
        ,   isCA: function( string ) {

                var caexpression = /^([0-9_\.\-])+\-(([a-zA-Z0-9\-])+\-)+([0-9]{2,4})+$/;

                return caexpression.test( string );
            }

            /**
            * Check whether an string is a 'NIT'
            * @param { str } String to test
            * @return { bool }
            */
        ,   isNIT: function( string ) {

                var nitexpression = /^([0-9]+\.)+([0-9]+\.)+([0-9]+\-)+[0-9]+$/;

                return nitexpression.test( string );
            }

            /**
            * Sets a countdown
            *
            * @param { object } arguments to set: count, limit, selector, callback
            * @return { function } the callback passed, return otherwise
            *  
            */
        ,	setCountdown: function( args ){

				var counter=setInterval( reverse, 1000); //1000 will  run it every 1 second

				function reverse(){
					
					args.count = args.count - 1;

					if ( args.count <= args.limit	 ) {
						
						clearInterval( counter);

						if ( args.callback !== null ) {

                            args.callback.call();
                        }

						return;
					}

					$( args.selector ).text( args.count );
				}
        	}

        ,	showNotification: function( type, message, time, callback ){
			
				
	    		$( '.global-notification' ).removeClass( 'error, success, warning, info' );
	    		$( '.global-notification' ).addClass( type );
	    		$( '.global-notification' ).text( message );
	    		$( '.global-notification' ).fadeIn();

	    		this.closeOnClickOut( '.global-notification' );
				
				if( time != 0 ){

					if( typeof callback != 'undefined' ){
						setTimeout( function(){ 
							$( '.global-notification' ).fadeOut();
							callback.call();
						}, time )
						return;
					}

					setTimeout( function(){ $( '.global-notification' ).fadeOut(); }, time )
					return;
	    			
				}
    		}

        	/**
        	* Creates an autocomplete for the selector passed
        	*
        	* @param { string } the selector string to be applied
        	*
        	*/
        ,	setAutocomplete: function( selector, _source ){

        		$( selector ).autocomplete( { source: _source } );
        	}

        	/**
        	* Return the uniques in array of strings
        	*
        	*/
        ,	arrayUnique: function( list ){

        		var result = [];
				$.each(list, function(i, e) {
					if ($.inArray(e, result) == -1) result.push(e);
				});
				return result;

        	}

        	/**
        	* validates if object ies empty
        	*
        	*/
        ,	isEmptyObject: function( obj ){

        		// Speed up calls to hasOwnProperty
				var hasOwnProperty = Object.prototype.hasOwnProperty;

			    // null and undefined are "empty"
			    if (obj == null) return true;

			    // Assume if it has a length property with a non-zero value
			    // that that property is correct.
			    if (obj.length && obj.length > 0)    return false;
			    if (obj.length === 0)  return true;

			    // Otherwise, does it have any properties of its own?
			    // Note that this doesn't handle
			    // toString and toValue enumeration bugs in IE < 9
			    for (var key in obj) {
			        if (hasOwnProperty.call(obj, key)) return false;
			    }

			    return true;
        	}
        
	        /**
	         * Init select 2 widget in selector
	         * 
	         * @param { string } jQuery selector
	         * 
	         */
	   , 	initSelect2: function( selector ){
	        		
		   		$( selector ).select2();
	        	 
	   		}

	   		/**
			* Hides an element when it is clicked outiside
			* @param { string } string for the jQuery selector like: ".my-class"
			* @return { null }
			*/
		,   closeOnClickOut: function( selector, callback ) {

				$( document ).mouseup( function( e ) {

					if ( ! $( selector ).is( ":visible" ) ) {
						return;
					}

					if ( $( selector ).has( e.target ).length === 0 ) {

						$( selector ).hide();
						if ( callback != null ) {
							callback.call();
						}
					}
				});

			}
	};
		
	window.Misc = new Misc();
	window.Misc.initialize();
		
})( jQuery, this, this.document, undefined );