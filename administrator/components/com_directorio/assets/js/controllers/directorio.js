/**
*
* Controller for { Directorio }
*
*
**/

( function( $, window, document, model, view, Utilities ){
	
	var DirectorioController = function( a ){
		
		// atributes, selector or global vars
		this.excel = '';
		this.pdf = '';
		this.excels = [];
		
	};
	
	DirectorioController.prototype ={
			

			/**
			 * Function header when calls the model method and gives the view the response
			 *
			 * @param {}
			 * @return {}
			 */
			parseExcel: function( data ){

				view.onBeforeParse( this.excels );

				var count = this.excels.length;

				// For each file in excels array, make a request to save the excel data
				for (var i = 0; i < this.excels.length ; i++ ) {
					
					data.excel = this.excels[i];

					var success = function( data ){

						view.onCompleteParse( data );

						count--;
					};

					var error = function ( XMLHttpRequest, textStatus, errorThrown ) {
                        view.onError( XMLHttpRequest, textStatus, errorThrown );   
                    }

					model.parseExcel( success, error,  data );
				};

				// At the end delete the excels file
				if( count <= 0 ){
					
					view.onBeforeDeleteTemp();

					var completeDeleteTemp = function( data ){

						view.onCompleteDeleteTemp( data );
					};

					return model.deleteTemp( completeDeleteTemp, data  );
				}
				
			}

		,	truncate: function( _data ){

				view.onBeforeTruncate();

				var _this = this;

				var success = function( data ){

					view.onCompleteTruncate( data );

					if( data.status == 200 )
						_this.parseExcel( _data );
				};

				return model.truncate( success, _data );
			}

			/**
			 * Function header when calls the model method and gives the view the response
			 *
			 * @param {}
			 * @return {}
			 */
		,	comparePDF: function( params ){

				var data = {
					pdfFile: this.pdf,
					type: $( '#select-tipo_organismo' ).find( ':selected' ).val(),
					params: params
				}

				
				var success = function( data ){

					data.params = params;
					view.onCompleteComparePDF( data );
				};

				return model.comparePDF( success, data );
			}
		
			
	};
	

	// Use this way when script is loaded at the end of the page
	// Expose to global scope
	window.DirectorioController = new DirectorioController();
	
})( jQuery, this, this.document, this.DirectorioModel, this.DirectorioView, this.Misc, undefined );