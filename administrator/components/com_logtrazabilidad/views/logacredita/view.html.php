<?php

defined('_JEXEC') or die;

/**
 * View class for a list of LogSessions.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_logtrazabilidad
 * @since		1.6
 */

jimport('joomla.application.component.view');

class LogTrazabilidadViewLogAcredita extends JView
{
	protected $items;
	protected $pagination;
	protected $state;

	/**
	 * Display the view
	 */
	public function display($tpl = null)
	{
		 $this->items		= $this->get('Objects');
		 $this->pagination 	= $this->get('Pagination');
		 $this->state		= $this->get('State');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		if( strtolower($this->getLayout()) == 'exportacsv' ){

			parent::display($tpl);
			die;
		}
		
		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since	1.6
	 */
	protected function addToolbar()
	{
		$canDo	= LogTrazabilidadHelper::getActions();

		JToolBarHelper::title( JText::_('COM_LOGTRAZABILIDAD_VIEW_LOGACREDITA_TITLE'), 'user' );

		if ($canDo->get('core.admin')) {

			JToolBarHelper::divider();

			$bar = JToolBar::getInstance('toolbar');
			//$url = JRoute::_('index.php?option=com_logtrazabilidad&view=logsession&layout=exportacsv');
			$url = JRoute::_('index.php?option=com_logtrazabilidad&task=logacredita.exportacsv');
			$bar->appendButton( 'Link', 'export', 'Exportar a Excel', $url );

			JToolBarHelper::divider();
		}
	}
}
