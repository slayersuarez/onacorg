<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die();

//get the hosts name
jimport('joomla.environment.uri' );
$host = JURI::root();

// Get the args when updating an informe
$id = JRequest::getVar( 'id' );

if( ! empty( $id ) ){

	$model = new GlosarioModelTermino();
	$model->instance( $id );
}	
 
//add the links to the external files into the head of the webpage (note the 'administrator' in the path, which is not nescessary if you are in the frontend)
$document =& JFactory::getDocument();
$document->addStyleSheet($host.'administrator/components/com_glosario/assets/css/style.css');
$document->addScript( '//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js');
?>

<script type="text/javascript">

Joomla.submitbutton = function( task ){
	
	
	if( task == 'termino.save' ){

		termino = document.getElementById("termino");

		if( termino.value.length == 0 ){
			alert("El termino es obligatorio");
			return;
		}
	}

	Joomla.submitform(task, document.getElementById('adminForm'));
}

</script>


<form action="index.php" method="post" name="adminForm" id="adminForm" class="form-validate">
	<fieldset class="adminform">
		<legend>Edici&oacute;n de terminos</legend>
		
		<ul>
			<li>
				<label class="required">Termino</label>
				<input type="text" name="termino" id="termino"
				 value="<?php echo $model->termino; ?>"
				 class="inputbox required" style='width: 50%;' required="required" />
			</li>
			<li>
				<label class="required">Descripción</label>
				<textarea name="descripcion" id="descripcion" style="width: 50%; height: 200px;"><?php echo $model->descripcion; ?></textarea>
			</li>
		</ul>
	</fieldset>

	<input type="hidden" name="option" value="com_glosario" />
	<input type="hidden" name="task" value="termino.save" />
	<input type="hidden" name="id" value="<?php echo $model->id; ?>" />
	
</form>