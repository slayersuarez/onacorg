<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" >
<?php JHTML::_('behavior.mootools'); ?>
<jdoc:include type="head" />
<link href='http://fonts.googleapis.com/css?family=Cuprum' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css" type="text/css" />
<link rel="stylesheet" href="css/template.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/template.css" type="text/css" />
</head>

<body>
<div id="content_header_100">
<div id="content_header">
    <div id="header">
      <jdoc:include type="modules" name="header" style="xhtml" />
    </div>
    <div id="cabeza"> </div>
    </div>
  </div>
<div id="content_cab">
<iframe scrolling="no" frameborder="0" style=" height: 490px;
    margin: 0 auto;
    position: relative;
    width: 100%;
    z-index: 100;" src="./jquery/in-nav.html"></iframe>
</div>



<div id="recontent-categorias"> 

<div id="content-categorias"><jdoc:include type="modules" name="content-categorias" style="xhtml" /></div>

</div>
<div id="all"> 
  
  <!--aqui ponemos todo lo de 1000px-->
  
  <div id="content-content">
  

    
  <div id="content_menu">
      <div id="menu">
        <jdoc:include type="modules" name="menu" style="xhtml" />
      </div>
     </div> 
    <div id="content_main">
      
      <jdoc:include type="component" />
      <div id="titulo-noti">
      </div>
  </div>
</div>
<div id="content_pie">
  <div id="content_piedos">
    <div id="pie">
      <jdoc:include type="modules" name="pie" style="xhtml" />
    </div>
 <div id="content_copy">
 <table width="670" style="margin:0px auto;" border="0">
  <tr>
    <td colspan="2" align="center"><div id="copy">Organismo Nacional de Acreditación de Colombia - ONAC      Carrera 7A No. 69-64     PBX: 742 7592 - 321 2938   FAX: Ext. 117
      
      <a href="http://www.creandopaginasweb.com/" target="_blank"></a>
      
    </div></td>
    </tr>
  <tr>
    <td width="494" align="right"><span style="text-align:right;    color: #FFFFFF;
    font-family: arial narrow;
    font-size: 15px;">Bogotá D.C., Colombia      onac@onac.org.co Sitio Web Desarrollado Por </span></td>
    <td width="166" align="center" style="padding-left:10px;" ><div id="logo"><a href="http://www.creandopaginasweb.com/" target="_blank"></a></div></td>
  </tr>
 </table>
    </div>
  </div>
  </div>
</div>
</body>
</html>