<?php
/**
 * @copyright	Copyright (C) 2013 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * configuracion pqrs Controller
 *
 * @package		Joomla.Administrator
 * @subpackage	com_pqrs
 */
class LogTrazabilidadControllerLogSession extends JControllerLegacy {
	

	/**
	 * Get a list of filter options for the blocked state of a user.
	 *
	 * @return  array  An array of JHtmlOption elements.
	 */
	public function getNamesUser(){
		
		$Utilities = 'Misc';
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );
		$dataName = JRequest::getVar( 'q' );
		$response = array();

		$query->select( 'a.nombre_usuario' );
		$query->from( $db->quoteName( 'log_sessions_user' ).' AS a' );
		$query->where( $db->quoteName( 'a.nombre_usuario' ) .' LIKE '. $db->quote('%'. $db->escape( $dataName ) .'%') );
		$query->group( 'a.nombre_usuario' );

		$db->setQuery( $query );
		$results = $db->loadAssocList();

		foreach ( $results as $key => $value) {
			
			$response[] = $value[ 'nombre_usuario' ];
		}

		if( count($response) <= 0 )$response[] = '';

		if( $Utilities::isAjax() ){

			echo json_encode( $response );
			die;
		}

		return $response;
	}

	/**
	 * Get a list of filter options for the blocked state of a user.
	 *
	 * @return  array  An array of JHtmlOption elements.
	 */
	public function getIdUser(){
		
		$Utilities = 'Misc';
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );
		$dataName = JRequest::getVar( 'q' );
		$response = array();

		$query->select( 'a.id_usuario' );
		$query->from( $db->quoteName( 'log_sessions_user' ).' AS a' );
		$query->where( $db->quoteName( 'a.id_usuario' ) .' LIKE '. $db->quote('%'. $db->escape( $dataName ) ) );
		$query->group( 'a.id_usuario' );

		$db->setQuery( $query );
		$results = $db->loadAssocList();

		foreach ( $results as $key => $value) {
			
			$response[] = $value[ 'id_usuario' ];
		}

		if( count($response) <= 0 )$response[] = '';

		if( $Utilities::isAjax() ){

			echo json_encode( $response );
			die;
		}

		return $response;
	}

	/**
	* Exporta el layout exportacsv de la vista logsession a archivo de hoja de calculo .csv
	*/
	public function exportaCsv(){

		$model =& $this->getModel( 'logsession' ); 
		$view =& $this->getView( 'logsession' , 'html');

		header('Content-type: application/vnd.ms-excel; name="excel"');
		header("Content-Disposition: attachment; filename=logSessions_".time().".xls");
		header("Pragma: no-cache");
		header("Expires: 0");

		JRequest::setVar( 'start', 0 );
		JRequest::setVar( 'limit', 0 );
		/*$model->setState( 'list.start', 0);
		$model->setState( 'list.limit', 0);*/

		$view->setModel( $model, true );
		$view->setLayout( 'exportacsv' );
		$view->display();

		return;
	}

}