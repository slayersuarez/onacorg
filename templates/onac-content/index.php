<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" >
<?php JHTML::_('behavior.mootools'); ?>
<jdoc:include type="head" />
<link href='http://fonts.googleapis.com/css?family=Cuprum' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Archivo+Narrow:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css" type="text/css" />
<link rel="stylesheet" href="css/template.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/template.css" type="text/css" />

<!-- jQuery -->

<link href="jquery/css/in-css-file.css" rel="stylesheet" type="text/css" />
<!--[if IE]>
<style>
    #subnav1, #subnav2, #subnav3{
        position:relative;
        top:15px;
        left:2px;
        z-index:10;
        display:none;
	}
</style>
<![endif]-->

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>

<script src="jquery/js/jquery.spritely-0.6.js" type="text/javascript"></script>
<script src="jquery/js/in-scriptsheet.js" type="text/javascript"></script>

<!-- jQuery -->

</head>

<body>
<div id="content_header_100">
<div id="content_header">
    <div id="clock">
      <jdoc:include type="modules" name="clock" style="xhtml" />
    </div>

    <div id="header">
      <jdoc:include type="modules" name="header" style="xhtml" />
    </div>
    <div id="cabeza"> </div>
    </div>
  </div>
<div id="content_cab">

<!-- MENU jQuery-->

<div id="hheader">

    <div id="logoh1"></div>
    <a href="index.php"><div id="logoh2"></div></a>
    
    <div id="btcalendar">
   	  <a href="./index.php/calendario-eventos" target="_top">
          <img src="jquery/img/bg_links.png" width="34" height="33" id="ovcalendar" />
          <img src="jquery/img/bt-calendar.png" width="34" height="33" id="bicalendar" />
      </a>
    </div>
  <div id="bt1">
   	  <a href="./index.php" target="_top">
          <img src="jquery/img/bg_links.png" width="116" height="33" id="ov1" />
          <img src="jquery/img/bt-home.png" width="116" height="33" id="bi1a" />
          <img src="jquery/img/bt-homeh.png" width="116" height="33" id="bi2a" />
      </a>
    </div>
    <div id="bt2">
    	<a href="./index.php/contactenos" target="_top">
            <img src="jquery/img/bg_links.png" width="137" height="33" id="ov2" />
            <img src="jquery/img/bt-contacto.png" width="137" height="33" id="bi1b" />
            <img src="jquery/img/bt-contactoh.png" width="137" height="33" id="bi2b" />
        </a>
    </div>
    <div id="bt3">
    	<a href="./index.php?option=com_directorio&template=onac-component">
            <img src="jquery/img/bg_links.png" width="161" height="34" id="ov3" />
    		<img src="jquery/img/bt-directorio-pymes.png" width="161" height="34" id="bi1c" />
        </a>
    </div>
    <div id="bt-sitemap">
    	<a href="./index.php/mapa-del-sitio" target="_top">
        	<img src="jquery/img/bt-sitemap.png" width="34" height="33" />
        </a>
    </div>
  
<!-- nav -->
    
    <ul id="big-navi">
    	<li id="lia">
            <a href="#">
            <img src="jquery/img/bg_links.png" width="215" height="32" id="big-ov1" />
            <img src="jquery/img/bt-nuestra-corpv.png" width="215" height="32" class="bglia" />
            <img src="jquery/img/t-nuestra-corpv.png" width="215" height="32" id="txa1" />
            <img src="jquery/img/t-nuestra-corpv.png" width="215" height="32" id="txa2" />
            </a>
            
            <ul id="subnn1">
            	<li>
                	<a href="./index.php/presentacion" target="_top">
                    	Quiénes Somos
                    </a>
                </li>
            	<li>
                	<a href="./index.php/asociados-onac" target="_top">
                    	Comunidad ONAC
                    </a>
                </li>
            	<li>
                	<a href="./index.php/personal-interno-convocatoria" target="_top">
                    	Convocatoria de Personal
                    </a>
                </li>
            	<li>
                	<a href="./index.php/contratacion-de-bienes-y-servicios" target="_top">
                    	Contratación de Bienes y Servicios
                    </a>
                </li>
            	<li>
                	<a href="./index.php/calendario-eventos" target="_top">
                    	Actualidad ONAC
                    </a>
                </li>
            </ul>
            
        </li>
        
    	<li id="lib">
            <a href="#">
            <img src="jquery/img/bg_links.png" width="225" height="32" id="big-ov2" />
            <img src="jquery/img/bt-serv-acreditacionv.png" width="225" height="32" class="bglia" />
            <img src="jquery/img/t-serv-acreditacionv.png" width="225" height="32" id="txb1" />
            <img src="jquery/img/t-serv-acreditacionv.png" width="225" height="32" id="txb2" />
            </a>
            
            <ul id="subnn2">
            	<li>
                	<a href="./index.php/portafolio-de-servicios-de-acreditacion" target="_top">
                    	Portafolio de Servicios de Acreditación
                    </a>
                </li>
            	<li>
                	<a href="./index.php/como-acceder-a-nuestros-servicios" target="_top">
                    	Cómo Acceder a los Servicios
                    </a>
                </li>
            	<li>
                	<a href="./index.php/directorio-de-acreditados" target="_top">
                    	Directorio de Acreditados
                    </a>
                </li>
            	<li>
                	<a href="./index.php/laboratorios-de-ensayo-acreditacion" target="_top">
                    	Aspectos Técnicos de la Acreditación
                    </a>
                </li>
            	<li>
                	<a href="./index.php/component/glosario/?template=onac-component" target="_top">
                    	Glosario
                    </a>
                </li>
            </ul>
            
        </li>
        
    	<li id="lic">
            <a href="#">
            <img src="jquery/img/bg_links.png" width="225" height="32" id="big-ov3" />
            <img src="jquery/img/bt-interactuav.png" width="225" height="32" class="bglia" />
            <img src="jquery/img/t-interactuav.png" width="225" height="32" id="txc1" />
            <img src="jquery/img/t-interactuav.png" width="225" height="32" id="txc2" />
            </a>
            
            <ul id="subnn3">
            	<li>
                    <a href="./index.php/contactenos" target="_top">
                        Contáctenos
                    </a>
                </li>
                <li>
                	<a href="./index.php/propuestas-y-recomendaciones" target="_top">
                    	Propuestas y Recomendaciones
                    </a>
                </li>
            	<li>
                	<a href="./index.php/quejas-sobre-onac" target="_top">
                    	Quejas Sobre ONAC
                    </a>
                </li>
            	<li>
                	<a href="./index.php/quejas-sobre-organismos-evaluadores-de-la-conformidad" target="_top">
                    	Quejas de Organismos Evaluadores de la Conformidad
                    </a>
                </li>
            	<li>
                	<a href="./index.php/informacion-sobre-conflicto-de-interes" target="_top">
                    	Información sobre Conflictos de Interés
                    </a>
                </li>
            	<li>
                	<a href="./index.php/participe-en-la-definicion-de-documentos" target="_top">
                    	Participe en la Definición de Documentos
                    </a>
                </li>
            	<li>
                	
                    	Consulta Trámite de Documentos
                    
                </li>
            	<li>
                	<a href="./index.php/ultimas-modificaciones" target="_top">
                    	Ayudas para la Navegación de la Página 
                    </a>
                </li>
            </ul>
            
        </li>
    </ul>

</div>

<!-- MENU jQuery-->


</div>

<div id="recontent-categorias"> 

<div id="content-categorias"><jdoc:include type="modules" name="content-categorias" style="xhtml" /></div>

</div>


<div id="all"> 
  
  <!--aqui ponemos todo lo de 1000px-->
  
  <div id="content-content">
  

    
  <div id="content_menu">
      <div id="menu">
        <jdoc:include type="modules" name="menu" style="xhtml" />
      </div>
     </div> 
    <div id="content_main">
      
      <jdoc:include type="component" />
      
   
  </div>
</div>
<div id="content_pie">
  <div id="content_piedos">
    <div id="pie">
      <jdoc:include type="modules" name="pie" style="xhtml" />
    </div>
 <div id="content_copy">
 <table width="670" style="margin:0px auto;" border="0">
  <tr>
    <td colspan="2" align="center"><div id="copy">Organismo Nacional de Acreditación de Colombia - ONAC      Carrera 7A No. 69-64     PBX: 742 7592 - 321 2938   FAX: Ext. 117
      
      <a href="http://www.creandopaginasweb.com/" target="_blank"></a>
      
    </div></td>
    </tr>
  <tr>
    <td width="494" align="right"><span style="text-align:right;    color: #FFFFFF;
    font-family: arial narrow;
    font-size: 15px;">Bogotá D.C., Colombia      onac@onac.org.co Sitio Web Desarrollado Por </span></td>
    <td width="166" align="center" style="padding-left:10px;" ><div id="logo"><a href="http://www.creandopaginasweb.com/" target="_blank"></a></div></td>
  </tr>
 </table>
    </div>
  </div>
  </div>
</div>
</body>
</html>