
<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );

// Begining of the controller
class GlosarioControllerLista extends JController{


		/**
		* Searchs by key
		*
		*/
		public function searchByKey(){

			// Initialize
			$data = JRequest::getVar( 'data' );
			$clave = $data;
			$response = ( object ) array();

			// validate clave is string
			if( ! is_string( $clave ) ){

				$response->status = 500;
				$response->message = 'Hubo un error al intentar obtener los terminos. Intente de nuevo.';

				echo json_encode( $response );
				die();
			}

			$clave = strip_tags( $clave );

			// get the model
			$model = $this->getModel( 'termino' );

			// Create the condition
			$wheres = array(
				0 => ( object )array(

						'key' => 'clave'
					,	'value' => "'$clave'"
					,	'condition' => ' = '
					,	'glue' => 'AND'
				)
			);
			

			// Get the objects
			$response->status = 200;
			$response->data = $this->dropDuplicates( $model->getObjects( $wheres ) );
			$response->clave = $data;
			$response->message = 'Consulta satisfactoria';

			echo json_encode( $response );
			die();
		}


		/**
		* Searchs by term
		*
		*/
		public function searchByTerm(){

			// Initialize
			$data = JRequest::getVar( 'data' );
			$term = $data[ 'termino' ];
			$response = ( object ) array();

			// validate term is string
			if( ! is_string( $term ) ){

				$response->status = 500;
				$response->message = 'Hubo un error al intentar obtener los terminos. Intente de nuevo.';

				echo json_encode( $response );
				die();
			}

			$term = strip_tags( $term );

			// get the model
			$model = $this->getModel( 'termino' );

			// get objects
			$objects = $model->getObjects();
			$results = array();

			// Compare the string
			foreach ( $objects as $key => $termino ) {
				
				$term_clean = $this->quitar_tildes( $termino->termino );
				$descripcion_clean = $this->quitar_tildes( $termino->descripcion );
				
				if ( preg_match("/" . $term . "/i", $termino->descripcion ) ) {
				    array_push( $results, $termino );
				}

				if ( preg_match("/" . $term . "/i", $termino->termino ) ) {
				    array_push( $results, $termino );
				}
				
				if ( preg_match("/" . $term . "/i", $term_clean ) ) {
					array_push( $results, $termino );
				}
				
				if ( preg_match("/" . $term . "/i", $descripcion_clean ) ) {
					array_push( $results, $termino );
				}
			}

			// Get the objects
			$response->status = 200;
			$response->data = $this->dropDuplicates( $results );
			$response->clave = $data;
			$response->message = 'Consulta satisfactoria';

			echo json_encode( $response );
			die();
		}
		
		/**
		 * Delete duplicates from array of objects
		 * 
		 */
		public function dropDuplicates( $array = NULL ){
			
			$tmp = array();
			
			foreach( $array as $k => $v)
			    $tmp[$k] = $v->termino;
			
			// Find duplicates in temporary array
			$tmp = array_unique( $tmp );
			
			// Remove the duplicates from original array
			foreach($array as $k => $v){
				
			    if (! array_key_exists( $k, $tmp ) )
			        unset( $array[ $k ] );
			}
			
			
			return $array;
		}
		
		/**
		 * Quitar acentos
		 * @param string cadena de texto
		 * @return string
		 */
		function quitar_tildes($cadena) {
			$no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");
			$permitidas= array ("a","e","i","o","u","A","E","I","O","U","n","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E");
			$texto = str_replace($no_permitidas, $permitidas ,$cadena);
			return $texto;
		}
}
?>