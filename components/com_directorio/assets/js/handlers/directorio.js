/**
*
* Event Handler for { Directorio }
*
*
**/

( function( $, window, document, controller, Utilities ){
	
	var DirectorioHandler = function( a ){
		
		// atributes, selector or global vars
		this.advancedSearchButton = '.boton-toggle-busqueda-avanzada';
		this.searchButton = '.submit-search';
		this.nextButton = '.submit-next-page';
		this.prevButton = '.submit-prev-page';
		this.currentPage = '0';
		this.maxPages = 0;

		
	};
	
	DirectorioHandler.prototype ={
			
			/**
			 * Inializes the functions when DOM ready
			 */	
			initialize: function(){
				
				this.toggleAdvancedSearch();
				this.getParams();
				this.searchByClick();
				this.searchByEnter();
				this.searchNext();
				this.searchPrev();
				Utilities.initSelect2( 'select[name="ciudad"]' );

				
			}


			/**
			* Displays or hides the advanced search fields
			*
			*/
		,	toggleAdvancedSearch: function(){

				var _this = this;

				$( this.advancedSearchButton ).click( function( e ){

					e.preventDefault();

					$( '.wrapper-buscador-avanzado' ).toggle();
				});
			}

			/**
			* Get the params from tipo de organismo selected
			*
			*/
		,	getParams: function(){

				var _this = this;

				$( '#select-tipo_organismo' ).change( function( e ){

					var tipoOrganismo = $( this ).find( ':selected' ).val();

					if( tipoOrganismo == '' ){
						$( '.wrapper-buscador-avanzado' ).hide();
						$( '.wrapper-buscador-avanzado' ).html( '<h3>Seleccione un tipo de organismo para cargar parametros.</h3>' );
						return;
					}

					controller.getParams( tipoOrganismo );
				});
			}


			/**
			* Search a term by clave
			*
			*/
		,	searchByClick: function(){

				var _this = this;

				$( 'body' ).delegate(  this.searchButton, 'click', function( e ){

					e.preventDefault();

					_this.currentPage = $( this ).data( 'page' );

					_this.search( _this.currentPage );
				});

			}

			/**
			* Search the next page
			*
			*/
		,	searchNext: function(){

				var _this = this;

				console.log( this.nextButton );

				$( 'body' ).delegate( this.nextButton, 'click', function( e ){

					e.preventDefault();

					var page = ( _this.currentPage + 1 > _this.maxPages ) ? _this.maxPages : _this.currentPage + 1;

					_this.currentPage = _this.currentPage + 1;

					_this.search( page );
				});

			}

			/**
			* Search the next page
			*
			*/
		,	searchPrev: function(){

				var _this = this;

				$( 'body' ).delegate(  this.prevButton, 'click', function( e ){

					e.preventDefault();

					if( _this.currentPage - 1 <= 0 ){
						_this.currentPage = 1;
					} else{
						_this.currentPage = _this.currentPage - 1;
					}
						

					_this.search( _this.currentPage );
				});

			}

			/**
			* Search a term by clave
			*
			*/
		,	searchByEnter: function(){

				var _this = this;

				$( document ).keypress( function( e ){

					var key = e.which;

					if( key == 13 ){

						e.preventDefault();

						_this.currentPage = '0';
						_this.search( '0' );
 
					}
				});

			}

			/**
			* Search a organismo in the db when click enter or any event
			*
			*/
		,	search: function( page ){

				var data = Utilities.formToJson( '#buscador-directorio' )
					,	required = [ ]
					,	errors = [];

				// Validate codigo de acreditacion
				if( data.codigo_acreditacion != '' && Utilities.isCA( data.codigo_acreditacion ) == false ){
					Utilities.showNotification( 'error', 'El código de acreditación no tiene un formato válido.', 0 );
					return;
				}

				// Validate the NIT
				if( data.nit != '' && Utilities.isNIT( data.nit ) == false ){
					Utilities.showNotification( 'error', 'El NIT no tiene un formato válido.', 0 );
					return;
				}

				controller.search( data, page );

			}
		
			
	};
	
	// Use this when the script is loaded in <head> tag
	$( document ).ready( function(){
		// Expose to global scope
		window.DirectorioHandler = new DirectorioHandler();
		window.DirectorioHandler.initialize();
	});
	
})( jQuery, this, this.document, this.DirectorioController, this.Misc, undefined );