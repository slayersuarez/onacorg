/**
*
* File uploading
*
*/

( function( $, window, document ){

	var Uploader = function( a ){

		this.params = {

				element: document.getElementById( 'file-uploader' )
			,   action: 'index.php'
			,   params: {

			    	option: 'com_si',
			    	task: 'informe.uploadFile'
			    }

			,   allowedExtensions: [ 'pdf' ]
			,   debug: false
			,	onSubmit: this.onSubmit
			,	onProgress: this.onProgress
			,	onComplete: this.onComplete
			,   onError: this.onError
		    
		};

	};

	Uploader.prototype = {

			initialize: function(){

				this.createUploader();
			}

		,	createUploader: function(){

				var _this = this;

				var uploader = new qq.FileUploader( _this.params );

			}

		,	onSubmit: function( id, fileName ){

				var li = $( '<li>' )
				,	i = $( '<i>' )
				,	spanName = $( '<span>' )
				,	spanBar = $( '<span>' )
				,	spanPerc = $( '<span>' );

				$( '.pdf-list' ).html( '' );

				i.addClass( 'icon-pdf' );
				spanName.text( fileName );
				spanBar.addClass( 'progress-bar' );
				spanPerc.addClass( 'percentage' );

				i.appendTo( li );
				spanName.appendTo( li );
				spanBar.appendTo( li );
				spanPerc.appendTo( li );

				li.appendTo( $( '.pdf-list' ) );
			}

		,	onProgress: function( id, fileName, loaded, total ){

				var perc = loaded / total * 100;

				$( '.percentage' ).text( perc + '%' );
				$( '.progress-bar' ).width( perc + 'px' );

			}

		,	onComplete: function(id, fileName, responseJSON){

				$( '.qq-upload-list' ).remove();
			}
		,   onError: function(id, fileName, xhr){

			}
	};

	$( document ).ready( function(){
		window.Uploader = new Uploader();
		window.Uploader.initialize();
	});

})( jQuery, this, this.document, undefined );







