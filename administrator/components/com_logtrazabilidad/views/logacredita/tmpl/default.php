<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	com_logtrazabilidad
 */

// No direct access.
defined('_JEXEC') or die;
jimport( 'joomla.environment.uri' );

// Load the tooltip behavior.
//JHtml::_('behavior.tooltip');
//JHtml::_('behavior.multiselect');
//JHtml::_('behavior.modal');
JHtml::_('behavior.framework');
$doc = JFactory::getDocument();

$doc->addScriptDeclaration( '
		var url = "'.JURI::base().'"
	' );

$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));

$canDo = LogTrazabilidadHelper::getActions();
$user = JFactory::getUser();
$loggeduser = JFactory::getUser();

//fb( $this->state );
?>

<form action="<?php echo JRoute::_('index.php?option=com_logtrazabilidad&view=logacredita');?>" method="post" name="adminForm" id="adminForm">
	<fieldset id="filter-bar">
		<div class="filter-search fltlft">
			<label class="filter-search-lbl" for="fecha_creacion">Fecha y hora:</label>
			<input type="text" name="fecha_creacion" id="fecha_creacion" class="plg-datatime" readonly="readonly" value="<?php echo $this->state->get( 'filter.fecha_creacion' );?>">

			<label class="filter-search-lbl" for="nom_user">Nombre de usuario:</label>
			<input type="text" name="nom_user" id="nom_user" value="<?php echo $this->state->get( 'filter.nombre_usuario' );?>">

			<select name="tipo_gestion" id="tipo_gestion">
				<option value="">Tipo de gesti&oacute;n:</option>
				<?php echo JHtml::_('select.options', LogTrazabilidadHelper::getGestiones(), 'value', 'text', $this->state->get('filter.tipo_gestion'));?>
			</select>

			<label class="filter-search-lbl" for="razon_social">Raz&oacute;n social del OEC:</label>
			<input size="70" type="text" name="razon_social" id="razon_social" value="<?php echo $this->state->get( 'filter.razon_social' );?>">				
		</div>
		<div class="clr"> </div>
		<div class="filter-search fltlft">
			<label class="filter-search-lbl" for="nit">Nit OEC:</label>
			<input type="text" name="nit" id="nit" value="<?php echo $this->state->get( 'filter.nit' );?>">	

			<label class="filter-search-lbl" for="descripcion_act">Descripci&oacute;n de la actividad:</label>
			<textarea name="descripcion_act" id="descripcion_act" cols="60" rows="3" ><?php echo $this->state->get( 'filter.descripcion' );?></textarea>	
		</div>
		<div class="filter-search fltrt">
			<button type="submit"><?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?></button>
			<button type="button" class="limpiar-filtros" ><?php echo JText::_('Limpiar'); ?></button>
		</div>
	</fieldset>
	<div class="clr"> </div>

	<table class="adminlist">
		<thead>
			<tr>
				<th class="nowrap">
					<?php echo JHtml::_('grid.sort', 'Id del usuario', 'a.id_usuario', $listDirn, $listOrder); ?>
				</th>
				<th class="nowrap">
					Nombre del usuario
				</th>
				<th class="nowrap">
					<?php echo JHtml::_('grid.sort', 'Tipo de gestión', 'a.tipo_gestion', $listDirn, $listOrder); ?>
				</th>
				<th class="nowrap">
					Descripci&oacute;n de la actividad
				</th>
				<th class="nowrap">
					Raz&oacute;n social del OEC
				</th>
				<th class="nowrap">
					Nit OEC
				</th>
				<th class="nowrap">
					<?php echo JHtml::_('grid.sort', 'Fecha y Hora', 'a.created_on', $listDirn, $listOrder); ?>							
				</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="9">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
		<?php /*fb( $this->items );*/ foreach ($this->items as $i => $item) :

			// si no es super usuario, no puede ver los registros
			if ( $canDo->get('core.admin') ) {
		?>
			<tr class="row<?php echo $i % 2; ?>">
				<td class="center"><?php echo $item->id_usuario?></td>
				<td class="center"><?php echo ucwords($item->nombre)?></td>
				<td class="center"><?php echo ucfirst($item->tipo_gestion)?></td>
				<td class="left"><?php echo $item->descripcion_actividad?></td>
				<td class="center"><?php echo $item->razon_social_oec?></td>
				<td class="center"><?php echo $item->nit_oec?></td>
				<td class="center"><?php echo $item->created_on?></td>
			</tr>
		<?php 
			}
		endforeach; 
		?>
		</tbody>
	</table>

	<div>
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>