<?php 
/*------------------------------------------------------------------------
# mod_jo_accordion - Vertical Accordion Menu for Joomla 1.5 
# ------------------------------------------------------------------------
# author    Roland Soos 
# copyright Copyright (C) 2011 Offlajn.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.offlajn.com
-------------------------------------------------------------------------*/
?>
<?php
// no direct access
defined('_JEXEC') or die('Restricted access'); 
if(!defined('OfflajnTiendaMenu')) {
  define("OfflajnTiendaMenu", null);
  
  if(!is_dir(JPATH_ROOT.DS.'components'.DS.'com_tienda'.DS.'controllers')){
    echo JText::_("This component is not installed!");
    return;
  }
  
  require_once(dirname(__FILE__) . DS .'..'.DS.'..'.DS.'core'.DS.'MenuBase.php');
  require_once (JPATH_SITE.DS.'components'.DS.'com_k2'.DS.'helpers'.DS.'route.php');
  class OfflajnTiendaMenu extends OfflajnMenuBase{
    
    function OfflajnTiendaMenu($module, $params){
      parent::OfflajnMenuBase($module, $params);
    }
    
    function getAllItems(){
      $db = & JFactory::getDBO();
      $categoryid = $this->_params->get('categoryid');
      $query = "SELECT DISTINCT 
        category_id AS id, 
        category_name AS name, "; 
         if($this->_params->get('displaynumprod', 0) != 0){
          $query.= "(SELECT COUNT(*) FROM #__tienda_productcategoryxref AS ax LEFT JOIN #__tienda_products AS bp ON ax.product_id = bp.product_id WHERE ax.category_id = id AND bp.product_enabled=1";
          $query.= ") AS productnum, ";
        }else{
          $query.= "0 AS productnum, ";
        }     
      if(!is_array($categoryid) && $categoryid != 0){
        $query.="IF(parent_id = ".$categoryid.", 0 , IF(parent_id = 0, -1, parent_id)) AS parent, ";
      }elseif(count($categoryid) && is_array($categoryid) && !in_array('0', $categoryid)){
        $query.="IF(id in (".implode(',', $categoryid)."), 0 , IF(parent_id = 0, -1, parent_id)) AS parent, ";
      }else{
        $query.="parent_id AS parent, ";
      }
      $query.="'cat' AS typ ";
      $query.= " FROM #__tienda_categories
                WHERE category_enabled=1 ";
      if ($this->_params->get('elementorder', 0) == 0)
        $query.="ORDER BY ordering ASC, name ASC";
      else if($this->_params->get('elementorder', 0)==1)
        $query.="ORDER BY name ASC";
      else if($this->_params->get('elementorder', 0)==2)
        $query.="ORDER BY name DESC";  

      
      $db->setQuery($query);
      
      $allItems = $db->loadObjectList('id');
      
      if($this->_params->get('showcontents') == 1){
        $query = "
          SELECT DISTINCT b.product_id, concat( a.category_id, '-', a.product_id ) AS id, b.product_name AS name, a.category_id AS parent, 'prod' AS typ, 0 AS productnum
          FROM #__tienda_productcategoryxref AS a
          LEFT JOIN #__tienda_products AS b ON a.product_id = b.product_id
          WHERE product_enabled = 1 ";
          if($this->_params->get('elementorder', 0)==1)
            $query.="ORDER BY name ASC";
          else 
            $query.="ORDER BY name DESC"; 
        $db->setQuery($query);
        $allItems += $db->loadObjectList('id');
      }
      
      return $allItems;
    }
    
    function getActiveItem(){  
      $active = null;
      if(JRequest::getVar('option') == 'com_tienda'){
        $content_id = 0;
        $category_id = 0; 
        if (JRequest::getInt('filter_category') > 0 && JRequest::getInt('id')==0) {
          $category_id = JRequest::getInt('filter_category');        
        } elseif (JRequest::getInt('id') > 0) { 
          $content_id = JRequest::getInt('id');
          $category_id = JRequest::getInt('filter_category');      
        }        
        if($content_id > 0 && $this->_params->get('showcontents')){ 
          $active = new StdClass();         
          $active->id = $category_id."-".$content_id;          
        } elseif ($category_id>0) {
          $active = new StdClass();
          $active->id = $category_id;
        }
      }
      return $active;
    }
    
    function getItemsTree(){     
      return $this->getItems();
    }
    
    function filterItem(&$item){
      $item->nname = stripslashes($item->name);
      if($this->_params->get('displaynumprod', 0) == 1 && $item->typ == 'cat' && $item->productnum > 0){
        $item->nname.= " (".$item->productnum.")"; 
      }elseif($this->_params->get('displaynumprod', 0) == 2 && $item->typ == 'cat'){
        $item->nname.= " (".$item->productnum.")"; 
      }
      $item->nname = '<span>'.$item->nname.'</span>';
      if($item->typ == 'cat'){
        if($this->_params->get('parentlink') == 0 && $item->p){
          $item->nname = '<a>'.$item->nname.'</a>';
        }else{
          $item->nname = '<a href="'.JRoute::_('index.php?option=com_tienda&view=products&filter_category='.$item->id).'">'.$item->nname.'</a>';
        }
      }elseif($item->typ == 'prod') {
        $id = explode("-", $item->id);
        $item->nname = '<a href="'.JRoute::_('index.php?option=com_tienda&view=products&task=view&id='.$id[1].'&filter_category='.$id[0].'&Itemid=').'">'.$item->nname.'</a>';
      }
    }
    
  }
}
?>