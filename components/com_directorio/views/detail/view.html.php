<?php

/**
 * General View for "Directorio" "busqueda" layout
 * 
 */

// Joomla calls and runtimes
defined( '_JEXEC' ) or die();
jimport( 'joomla.application.component.view' );

class DirectorioViewDetail extends JViewLegacy {

	protected $acreditado;
	protected $acreditaciones;
	
	// Function that initializes the view
	function display( $tpl = null ){

		// Gets all the organismo in databse where clave will be A
		$this->getAcreditado();

		parent::display( $tpl );
	
	}

	/**
	* Get all organismo
	*
	*/
	public function getAcreditado(){

		$class = JRequest::getVar( 'type' );
		$id = JRequest::getVar( 'id' );

		if( empty( $class ) || empty( $id ) ){
			echo 'Acceso no autorizado';
			die();
		}
			

		$class = 'DirectorioModel' . $class;

		$model = new $class();
		$model->instance( $id );
		$this->acreditado = $model;

		// Get acreditaciones
		$this->acreditaciones = $model->getAcreditaciones();
	}

}
?>