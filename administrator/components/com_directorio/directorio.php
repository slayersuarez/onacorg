<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
require_once( JPATH_COMPONENT . DS . 'helpers/uploader.php' );
require_once( JPATH_COMPONENT . DS . 'helpers/PHPExcel.php' );
require_once( JPATH_COMPONENT . DS . 'helpers/PHPExcel/IOFactory.php' );
require_once( JPATH_COMPONENT . DS . 'controller.php' );

//unset($_SESSION['seg_file']);
$controller = JControllerLegacy::getInstance('Directorio');
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();
?>