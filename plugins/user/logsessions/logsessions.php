<?php
defined('_JEXEC') or 	die( 'Direct Access to ' . basename( __FILE__ ) . ' is not allowed.' ) ;
/**
 * @author Sainet Ingenieria Ltda. | Desarrollador : Robinson Perdomo Lugo
 * @author http://www.creandopaginasweb.com
 * @package VirtueMart
 * @subpackage vmcustom
 *
 */

//establecer la zona horaria 
date_default_timezone_set( 'America/Bogota' );

if( !class_exists( 'JUserHelper' ) )require( JPATH_ROOT . DS . 'libraries'. DS . 'joomla' . DS . 'user' . DS . 'helper.php');
if( !class_exists( 'JUser' ) )require( JPATH_ROOT . DS . 'libraries'. DS . 'joomla' . DS . 'user' . DS . 'user.php');

class plgUserLogSessions extends JPlugin
{	

	/**
	 * Constructor
	 *
	 * @access      protected
	 * @param       object  $subject The object to observe
	 * @param       array   $config  An array that holds the plugin configuration
	 * @since       1.5
	 */
	public function __construct(& $subject, $config)
	{
		parent::__construct($subject, $config);
		$this->loadLanguage();
	}

	/**
	* funcion que almacena un registro en el momento en que usuario inicia sesión
	* @param { array } // credentials user
	* @param { array } // array option
	* @return void
	* @access public 
	*/
	public function onUserLogin( $user_credentials, $option )
	{
	 	$app = JFactory::getApplication();

		$for_admin = $this->params->get( 'log_session_admin', 1 );
		$for_user = $this->params->get( 'log_session_user', 0 );		

		if( $app->isAdmin() && $for_admin == 1 && $user_credentials[ 'status' ] == 1 ){

			$user = $this->getUser( $user_credentials );

			if( is_object($user) )
				$this->saveRegistration( $user, '1', 'backend' );


		}elseif( $app->isSite() && $for_user == 1 && $user_credentials[ 'status' ] == 1 ){

			$user = $this->getUser( $user_credentials );

			if( is_object($user) )
				$this->saveRegistration( $user, '1', 'frontend' );

		}
	}

	/**
	* funcion que almacena un registro en el momento en que usuario cierra sesión
	* @param { array } // credentials user
	* @param { array } // array option
	* @return void
	* @access public 
	*/
	public function onUserLogout( $user_credentials, $option ){

		$app = JFactory::getApplication();
		$user = JFactory::getUser( $user_credentials[ 'id' ] );

		//fb( $user_credentials ); die;

		$for_admin = $this->params->get( 'log_session_admin', 1 );
		$for_user = $this->params->get( 'log_session_user', 0 );		

		if( $app->isAdmin() && $for_admin == 1 ){

			if( is_object($user) )
				$this->saveRegistration( $user, '2', 'backend' );

		}elseif( $app->isSite() && $for_user == 1 ){

			if( is_object($user) )
				$this->saveRegistration( $user, '2', 'frontend' );
		}
	}


	/**
	* get login user
	* @param { array } // credentials user
	* @return { object | null } 
	* @access private 
	*/
	private function getUser( $user_credentials ){

		$db = JFactory::getDbo();
		$query = $db->getQuery( true );
		$user = NULL;

		$query->select( '*' )
			  ->from( $db->quoteName( '#__users' ) )
			  ->where( $db->quoteName( 'username' ) .' LIKE '. $db->quote( $user_credentials[ 'username' ] ) );

		$db->setQuery( $query );
		$result = $db->loadObject();			 

		if ($result)
		{
			$match = JUserHelper::verifyPassword($user_credentials['password'], $result->password, $result->id);

			if ($match === true)
			{
				$user = JUser::getInstance($result->id);				
			}	 
		} 
		$query->clear();

		return $user;
	}

	/**
	* inserta el registro de sesión en la base de datos
	* @param { object } // object user
	* @param { string | int } // tipo se session
	* @return { 'string' } // tipo de usuario 
	* @access private 
	*/
	private function saveRegistration( $user, $tipoSession, $tipoUser ){

		$db = JFactory::getDbo();
	 	$query = $db->getQuery( true );
	 	$fechaCierre = "0000-00-00 00:00:00";

	 	//fb( $tipoUser ); die;

	 	if( is_object($user) && !empty($user) ){

	 		$cantMinutes = 0;

	 		if( $tipoSession == 2 ){	 			

	 			$query->select( '*' )
	 				  ->from( $db->quoteName( 'log_sessions_user' ) )
	 				  ->where( $db->quoteName( 'tipo_log' ) .' = '. $db->quote( '1' ) .' AND '.  $db->quoteName( 'id_usuario' ) .' = '. $db->quote( $user->id ) .' AND '. $db->quoteName( 'tipo_user' ) .' LIKE '. $db->quote( $tipoUser ) )
	 				  ->order( 'fecha_acceso DESC' );

	 			$db->setQuery( $query );	  
	 			$result = $db->loadObject();

	 			if( is_object($result) && !empty($result) ){

	 				$fechaCierre = date( 'Y-m-d H:i:s' );
	 				$cantMinutes = ( time() - strtotime( $result->fecha_acceso ) )/60;

	 				try{

	 					$db->transactionStart();

		 				$query->clear();

		 				$query->update( 'log_sessions_user' )
			 				  ->set(array(

			 				  		$db->quoteName( 'fecha_cierre' ) .' = '. $db->quote( $fechaCierre ),
			 				  		$db->quoteName( 'duracion' ) .' = '. $db->quote( $cantMinutes ),
			 				  		$db->quoteName( 'tipo_log' ) .' = '. $db->quote( $tipoSession )

			 				  ))->where( $db->quoteName( 'id_log_session' ) .' = '. $db->quote( $result->id_log_session ) );

			 			$db->setQuery( $query ); 
						$db->execute();	

						$db->transactionCommit();

					}catch( Exception $e ){

						// catch any database errors.
					    $db->transactionRollback();
					    JErrorPage::render($e);
					} 	

	 			}
				
				return;	 			
	 		}

			$query->insert( $db->quoteName( 'log_sessions_user' ) , 'id_log_session' )
				  ->columns( 'id_usuario, nombre_usuario, fecha_acceso, fecha_cierre, duracion, tipo_log, tipo_user' )
				  ->values( $db->quote($user->id) .', '. $db->quote($user->name) .', '. $db->quote(date( 'Y-m-d H:i:s' )) .', '. $db->quote($fechaCierre) .', '. $db->quote($cantMinutes) .', '. $db->quote($tipoSession) .', '. $db->quote($tipoUser) );

			$db->setQuery( $query ); 
			$db->execute();	 

			return; 
		}

	}

	public function onUserAuthenticate( $miarray ){
		fb( $miarray ); die;
	}
}
// No closing tag