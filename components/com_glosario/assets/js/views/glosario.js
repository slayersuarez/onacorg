/**
*
* View for { Glosario }
*
*
**/

( function( $, window, document, Utilities ){
	
	var GlosarioView = function( a ){
		
		// atributes, selector or global vars
		this.keyButton = '.alphabet-letter';
		this.tooltip = '.tooltip-descripcion-termino';
		this.listaContent = '.wrapp-lista-content-terminos';
		this.loader = '<span class="loader"><div class="bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div></span>';
		
	};
	
	GlosarioView.prototype ={
			

			/**
			 * Sets the position of the tooltips
			 *
			 */
			setTooltip: function(){
				
				$.each( $( this.tooltip ), function( key, value ){

					// Set the position of the triangle
					var tooltipHeight = $( this ).height();
					$( this ).find( 'span.tooltip-triangle-gray' ).css({ top: ( tooltipHeight + 7 ) + 'px' });

					// set the top of the tooltip
					$( this ).css( 'top', '-' + ( tooltipHeight + 50 ) + 'px' );
				});
			}

		,	onCompleteSearch: function( data ){
				
				if( data.status == 200 ){
					
					var markup = new EJS({ url: urlTemplates  + 'lista.ejs' }).render( data );
					$( this.listaContent ).html( markup );

					if( typeof data.clave != 'object' ){
						$( '.lista-terminos-header' ).find( 'span' ).text( data.clave );
					}else {
						$( '.lista-terminos-header' ).find( 'span' ).text( 'A-Z' );
						$( this.keyButton ).removeClass( 'active' );
					}

					this.setTooltip();
				}
			}

			/**
			* Begin the process, shows a loader and executes a callback after time out passed through
			*
			* @param { string } the string selector render loading on
			* @param { int } milliseconds after execute the callback
			* @param { function } the function that triggers after the time set up previously
			*/
		,	onProcess: function( selector, timeOut, callback ){

				$( selector ).html( this.loader );

				return setTimeout( function(){  callback.call(); }, timeOut );
			}
		
			
	};
	

	// Use this way when script is loaded at the end of the page
	// Expose to global scope
	window.GlosarioView = new GlosarioView();
	
})( jQuery, this, this.document, this.Misc, undefined );