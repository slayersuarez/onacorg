<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	com_logtrazabilidad
 */

// No direct access.
defined('_JEXEC') or die;
jimport( 'joomla.environment.uri' );

$canDo = LogTrazabilidadHelper::getActions();
$user = JFactory::getUser();
$loggeduser = JFactory::getUser();

?>
<table class="adminlist">
	<thead>
		<tr>
			<th class="nowrap">
				Id del usuario
			</th>
			<th class="nowrap">
				Nombre del usuario
			</th>
			<th class="nowrap">
				Fecha y hora &#47; inicio de sesión
			</th>
			<th class="nowrap">
				Fecha y hora &#47; cierre de sesión.
			</th>
			<th class="nowrap">
				Duración de la sesión (Min)
			</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach ($this->servicios as $i => $item) :

		// si no es super usuario, no puede ver los registros
		if ( $canDo->get('core.admin') ) {
	?>
		<tr class="row<?php echo $i % 2; ?>">
			<td class="center"><?php echo $item->id_usuario?></td>
			<td class="center"><?php echo ucwords($item->nombre_usuario)?></td>
			<td class="center"><?php echo $item->fecha_acceso?></td>
			<td class="center"><?php echo $item->fecha_cierre?></td>
			<td class="center"><?php echo $item->duracion?></td>
		</tr>
	<?php 
		}
	endforeach; 
	?>
	</tbody>
</table>