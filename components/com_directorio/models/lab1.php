<?php

/**
 * Model for "Lab1"
 * 
 */

// Joomla calls and runtimes
defined( '_JEXEC' ) or die();
jimport( 'joomla.application.component.model' );

// Initializes the Class
class DirectorioModelLab1 extends JModel {
	
	/**
	 * Object Id
	 * @var int
	 */
	var $id;


	/**
	 * Clave
	 * @var string
	 */
	var $codigo_acreditacion;

	/**
	 * Termino: la palabra del diccionario
	 * @var string
	 */
	var $estado;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $observaciones;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $razon_social;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $nit;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $criterios_acreditacion;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $nombre_establecimiento_comercial;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $numero_matricula_mercantil;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $direccion_sede_principal;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $ciudad;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $numero_unidades_moviles;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $depende_lab_central_acreditado;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $depende_lab_central_no_acreditado;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $depende_otra_entidad_no_lab;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $departamento_correspondencia;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $direccion_correspondencia;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $ciudad_correspondencia;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $pais_correspondencia;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $fijo;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $movil;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $fax;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $pagina_web;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $representante_legal;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $documento_representante_legal;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $correo_representante;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $fijo_representante;


	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $movil_representante;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $fax_representante;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $representante_onac;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $representante_onac_identificacion;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $representante_onac_correo;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $representante_onac_direccion;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $representante_onac_fijo;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $representante_onac_movil;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $representante_onac_fax;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $numero_personas_oec_empleado;


	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $numero_personas_oec_externo;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $denominacion_unidad;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $unidad_departamento;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $unidad_direccion;



	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $unidad_ciudad;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $unidad_responsable;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $unidad_responsable_cargo;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $unidad_responsable_correo;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $unidad_responsable_fijo;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $unidad_responsable_movil;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $unidad_responsable_fax;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $dias_evaluacion_vigencia;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $dias_evaluacion_renovacion;


	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $numero_sitios_cubiertos_acreditacion;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $nombre_sitio_cubierto;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $direccion_sitio_cubierto;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $ciudad_sitio_cubierto;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $fecha_otorgamiento_sitio_cubierto;


	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $fecha_vencimiento_sitio_cubierto;


	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $fecha_renovacion_sitio_cubierto;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $fecha_ultima_modificacion_sitio_cubierto;


	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $fecha_visita_vigilancia_1_sitio_cubierto;


	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $fecha_visita_vigilancia_2_sitio_cubierto;


	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $fecha_visita_vigilancia_3_sitio_cubierto;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $fecha_visita_vigilancia_4_sitio_cubierto;

	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $codigos_sector_general;


	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $codigos_sector_especifico;


	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $nombre_sector_general;


	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $nombre_sector_especifico;


	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $ensayo;


	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $producto_ensayo;


	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $documento_normativo;


	/**
	 * Descripción: la palabra del diccionario
	 * @var string
	 */
	var $PDF;

	
	/**
	 * Get if the object exists
	 * @var bool
	 */
	var $exists = false;
	
	/**
	 * Cache of results, data, queries
	 * @var unknown
	 */
	var $data;

	/**
	 * Table var
	 * @var unknown
	 */
	var $table = '#__organismo_lab1';

	/**
	 * Model Name
	 * @var string
	 */
	var $type = 'Lab1';

	/**
	 * Constant for table
	 * @var string
	 */
	const TABLE = '#__organismo_lab1';

	/**
	 * Constant for filters states
	 * @var string
	 */
	const FILTER_STATE = 'filter.lab1.';

	
	/**
	 * Attributes Map
	 * @var array
	 */
	var $attrs_map = array(
			'id'
		,	'codigo_acreditacion'
		,	'estado'
		,	'observaciones'
		,	'razon_social'
		,	'nit'
		,	'criterios_acreditacion'
		,	'nombre_establecimiento_comercial'
		,	'numero_matricula_mercantil'
		,	'direccion_sede_principal'
		,	'ciudad'
		,	'numero_unidades_moviles'
		,	'depende_lab_central_acreditado'
		,	'depende_lab_central_no_acreditado'
		,	'depende_otra_entidad_no_lab'
		,	'direccion_correspondencia'
		,	'departamento_correspondencia'
		,	'ciudad_correspondencia'
		,	'pais_correspondencia'
		,	'fijo'
		,	'movil'
		,	'fax'
		,	'pagina_web'
		,	'representante_legal'
		,	'documento_representante_legal'
		,	'correo_representante'
		,	'fijo_representante'
		,	'movil_representante'
		,	'fax_representante'
		,	'representante_onac'
		,	'representante_onac_identificacion'
		,	'representante_onac_correo'
		,	'representante_onac_direccion'
		,	'representante_onac_fijo'
		,	'representante_onac_movil'
		,	'representante_onac_fax'
		,	'numero_personas_oec_empleado'
		,	'numero_personas_oec_externo'
		,	'denominacion_unidad'
		,	'unidad_departamento'
		,	'unidad_direccion'
		,	'unidad_ciudad'
		,	'unidad_responsable'
		,	'unidad_responsable_cargo'
		,	'unidad_responsable_correo'
		,	'unidad_responsable_fijo'
		,	'unidad_responsable_movil'
		,	'unidad_responsable_fax'
		,	'dias_evaluacion_vigencia'
		,	'dias_evaluacion_renovacion'
		,	'numero_sitios_cubiertos_acreditacion'
		,	'nombre_sitio_cubierto'
		,	'direccion_sitio_cubierto'
		,	'ciudad_sitio_cubierto'
		,	'fecha_otorgamiento_sitio_cubierto'
		,	'fecha_vencimiento_sitio_cubierto'
		,	'fecha_renovacion_sitio_cubierto'
		,	'fecha_ultima_modificacion_sitio_cubierto'
		,	'fecha_visita_vigilancia_1_sitio_cubierto'
		,	'fecha_visita_vigilancia_2_sitio_cubierto'
		,	'fecha_visita_vigilancia_3_sitio_cubierto'
		,	'fecha_visita_vigilancia_4_sitio_cubierto'
		,	'codigos_sector_general'
		,	'codigos_sector_especifico'
		,	'nombre_sector_general'
		,	'nombre_sector_especifico'
		,	'ensayo'
		,	'producto_ensayo'
		,	'documento_normativo'
		,	'PDF'
	);

	/**
	 * Natural names
	 * @var array
	 */
	var $natural_names = array(
			'codigo_acreditacion' => 'Codigo acreditación'
		,	'estado' => 'Estado'
		,	'observaciones' => 'Observaciones'
		,	'razon_social' => 'Razón social'
		,	'nit' => 'NIT'
		,	'criterios_acreditacion' => 'Criterios de Acreditación'
		,	'direccion_sede_principal' => 'Dirección sede principal '
		,	'ciudad' => 'Ciudad'
		,	'fijo' => 'Fijo'
		,	'fax' => 'Fax'
		,	'pagina_web' => 'Página Web'
		,	'fecha_otorgamiento_sitio_cubierto' => 'Fecha de Otorgamiento'
		,	'fecha_vencimiento_sitio_cubierto' => 'Fecha de Vencimiento'
		,	'nombre_sector_general' => 'Nombre Sector General'
		,	'nombre_sector_especifico' => 'Nombre Sector Espec&iacute;fico'
		,	'ensayo' => 'Ensayo'
		,	'producto_ensayo' => 'Producto o material a ensayar'
		,	'documento_normativo' => 'Documento normativo'
	);

	/**
	* Parametros
	*/
	var $parametros = array(
			'nombre_sector_general'
		,	'nombre_sector_especifico'
		,	'ensayo'
		,	'producto_ensayo'
		,	'documento_normativo'
	);
	
	
	/**
	 * Methods
	 * 
	 */
	
	/**
	 * Constructor
	 * 
	 * @param { array || int } the args to instance the model or the single id
	 * 
	 */
	public function instance( $config = NULL ){
		
		if( is_numeric( $config ) )
			$config = array( 'id' => $config );
		
		if( ! is_array( $config ) )
			return;
		
		// Get existing object if the id was passed through
		return $this->fill( $config );
	}
	
	/**
	 * Fill the model attributes with the passed arguments.
	 *
	 * @param { arr } Object arguments
	 */
	protected function fill( $args = null ){


		if ( ! is_array( $args ) )
			return false;
		
	
		// Get object in DB			
		if ( is_numeric( $args[ 'id' ] ) ){

			$object = $this->getObject( $args[ 'id' ] );

			foreach ( $this->attrs_map as $attr ) {
				
				if ( isset( $object->$attr ) )
					$this->$attr = $object->$attr;
			}

		}

	
		// Merge attributes	when id is not passed through
		foreach ( $this->attrs_map as $attr ) {
			if ( isset( $args[ $attr ] ) )
				$this->$attr = $args[ $attr ];
		}

		// Set exists to true.
		$this->exists = true;
	
	}
	
	
	/**
	 * Get a single Object
	 * 
	 * @param { int } the id or attributes of the Object.
	 * @return { bool/object } the object returned or false otherwise
	 */
	protected function getObject( $id = 1 ){
		
		if( ! is_numeric( $id ) )
			return false;
		
		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );

		$query->select( '*' );
		$query->from( self::TABLE );
		$query->where( 'id = ' . $id );
		$db->setQuery( $query );
		
		return $db->loadObject();
		
	}
	
	/**
	 * Get Objects collection
	 *
	 * @param { int } the id or attributes of the Object.
	 * @return { bool/object } the object returned or false otherwise
	 **/
	public function getObjects( $wheres = NULL, $page = '0' ){
		
		$result = array();
		
		if( ! is_array( $wheres ) )
			$wheres = array();
		
		// Verifies if data already contains a collection
		if( empty( $this->data ) ){
			
			$query = $this->buildQuery( $wheres );
			
			if( $page == 0 ){
				$this->data = $this->_getList( $query );
			}else{
				$this->data = $this->_getList( $query, $page, '20' );
			}
		}

		
		foreach ( $this->data as $obj ){

			$args = array();

			foreach ( $this->attrs_map as $key => $objattr ) {
				
				$args[ $objattr ] = $obj->$objattr;
			}

			$object = new DirectorioModelLab1();
			$object->fill( $args );
			
			array_push( $result, $object );
			
		}
	
		return $result;
	
	}

	/**
	 * Save a new object or update an exist.
	 *
	 * @param { string } the type of the response
	 * ( string returns the error string or "" )
	 * ( bool return false or true )
	 * ( object returns the response with error_message, status true or false )
	 * @return { string  } the query string calling the insert query
	 *
	 */
	public function save( $return = 'string' ){

		// Initialize
		$db = JFactory::getDbo();
		$response = ( object ) array();
		$model = ( object ) array();

		if( ! is_string( $return ) )
			$return = 'string';

		// Fetch the model attributes with the $model's var
		foreach ( $this->attrs_map as $attribute ) {
			$model->$attribute = $this->$attribute;
		}

		// If id exists, update the model
		// If id doesn't exist, insert a new row in database
		if( $model->id == NULL || $model->id == "" ){

			if (! $db->insertObject( self::TABLE, $model ) ) {

				if( $return == 'string' ){
					return "No se pudo guardar el object. " . $db->stderr();
				}

				if( $return == 'bool' ){
					return false;
				}

				if( $return == 'object' ){
					$response->status = false;
					$response->error = "No se pudo guardar el object. " . $db->stderr();
					return $response;
				}
			}

			if( $return == 'string' ){
				return "";
			}

			if( $return == 'bool' ){
				return true;
			}

			if( $return == 'object' ){
				$response->status = true;
				$response->error = "";
				return $response;
			}
		}

		// Update
		if ( ! $db->updateObject( self::TABLE, $model, 'id', false ) ) {
			
			if( $return == 'string' ){
				return "No se pudo actualizar el object. " . $db->stderr();
			}

			if( $return == 'bool' ){
				return false;
			}

			if( $return == 'object' ){
				$response->status = false;
				$response->error = "No se pudo guardar el object. " . $db->stderr();
				return $response;
			}
		}

		if( $return == 'string' ){
			return "";
		}

		if( $return == 'bool' ){
			return true;
		}

		if( $return == 'object' ){
			$response->status = true;
			$response->error = "";
			return $response;
		}

	}
	
	/**
	 * Delete object from database
	 *
	 * @param { array || int } the id of the object or array that contains the id
	 * @return { bool/object } the object returned or false otherwise
	 */
	public function delete(){
	
		if( ! is_numeric( $this->id ) )
			return false;
	
	
		// Delete existing object if the id was passed through

		$query = "DELETE FROM ". self::TABLE ." WHERE id = $this->id";
		$db = JFactory::getDbo();
		$db->setQuery( $query );
	
		return $db->query();
	
	}


	/**
	 * Truncates table from database
	 *
	 * @param { array || int } the id of the object or array that contains the id
	 * @return { bool/object } the object returned or false otherwise
	 */
	public function truncate(){
	
		// Restart the table, clean the rows and restart id = 0
		$query = "TRUNCATE TABLE ". self::TABLE;
		$db = JFactory::getDbo();
		$db->setQuery( $query );
	
		return $db->query();
	
	}

	/**
	 * Get the params from params array definition
	 * @return { array } the array of params and their values
	 */
	public function getParametros(){
	
		if( ! is_array( $this->parametros ) )
			return false;


		$db = JFactory::getDbo();
		$query = $db->getQuery( true );
		$select = '';

		if( sizeof( $this->parametros ) <= 0 ){
			return array();
		}

		foreach ( $this->parametros as $key => $parametro ) {
			
			$select .= $parametro . ', ';

		}
		
		$select .= rtrim( $select, ', ' );
	
		// Delete existing object if the id was passed through
		$query->select( $select );
		$query->from( self::TABLE );
		$db->setQuery( $query );
	
		$list = $db->loadObjectList();

		$parametros = array();

		// Initialize parametros values as array
		foreach ( $this->parametros as $key => $param ) {
			$parametros[ $param ] = array();
		}

		// Recover params values
		foreach ( $list as $key => $params_row ){
			
			foreach ( $params_row as $_key => $_parametro ) {

				//var_dump( $_key );

				array_push( $parametros[ $_key ], $_parametro );
			}
		}


		// Unique values
		foreach ( $parametros as $key => $value) {
			
			$parametros[ $key ] = array_unique( $parametros[ $key ] );
		}


		return $parametros;
	
	}

	// Get natural name for key
	public function getNaturalKey( $key = NULL ){

		if( ! is_string( $key ) ){
			return $key;
		}


		if( ! array_key_exists( $key, $this->natural_names ) ){
			return $key;
		}

		return $this->natural_names[ $key ];
	}

	/**
	* Get all the cities in unique array
	*
	*/
	public function getCities(){

		$db = JFactory::getDbo();
		$query = $db->getQuery( true );

		// Select the cities
		$query->select( 'ciudad' );
		$query->from( self::TABLE );
		$query->where( 'ciudad != ""' );
		$query->order( 'ciudad ASC' );

		$db->setQuery( $query );

		$cities = $db->loadResultArray();

		if( empty( $cities ) ){
			return array();
		}

		$cities = array_unique( $cities );

		return $cities;

	}
	
	// Helpers for the same Model
	// Next methods are utilities no core methods, therefore they all are protected
	
	public function getParams(){

		$mainframe =& JFactory::getApplication();

		foreach ( $this->attrs_map as $key => $param ) {

			$_param = ( object ) array();
			$_param->key = $param;
			$_param->value = $mainframe->getUserState( self::FILTER_STATE . $param );
			$_param->type = gettype( $_param->value );
			array_push( $this->filters, $_param );
		}
	}


	public function setParams(){

		$mainframe =& JFactory::getApplication();

		foreach ( $this->attrs_map as $key => $param ) {

			$mainframe->setUserState( self::FILTER_STATE . $param, JRequest::getVar( $param ) );
		}

		$this->getParams();
	}

	public function cleanParams(){

		$mainframe =& JFactory::getApplication();

		foreach ( $this->attrs_map as $key => $param ) {

			$mainframe->setUserState( self::FILTER_STATE . $param, " " );
		}

		$this->getParams();
	}
	
	/**
	 * Build a query for collection. Filters query are included.
	 *
	 * @param { array } wheres clausule. Clausule must be { key: 'value', value: 'value', condition:'=', glue: 'AND || OR' }
	 * @return { string } the query string calling the collection
	 *
	 */
	protected function buildQuery( $wheres = NULL ){

		// Validation
		if( ! is_array( $wheres ) )
			$wheres = array();

		// Initialize
		$db = JFactory::getDbo();
		$query  = $db->getQuery(true);

		// Get the filters
		//$this->getParams();
		
		// Query base		
		$query->select( "*" );
		$query->from( self::TABLE);

		
		// Wheres appending
		foreach ( $wheres as $key => $clausule ) {
			
			if( ! is_object( $clausule ) )
				break;	

			$query->where( $clausule->key . $clausule->condition . $clausule->value, $clausule->glue );

		}
		
		$query->group( array( 'ciudad', 'codigo_acreditacion' ) );
		$query->order( 'id ASC' );
		
		return $query;
	}

	/**
	 * Count the total elements
	 * 
	 */
	public function setPaginator( $wheres, $limit = 20 ){
		
		$db = JFactory::getDbo();
		$response = ( object )array();
		$query = $db->getQuery( true );
		$query->select( "*" );
		$query->from( self::TABLE);
		
		if( ! is_array( $wheres ) )
			$wheres = array();
		
		
		// Wheres appending
		foreach ( $wheres as $key => $clausule ) {
				
			if( ! is_object( $clausule ) )
				break;
		
			$query->where( $clausule->key . $clausule->condition . $clausule->value, $clausule->glue );
		
		}

		$query->group( array( 'ciudad', 'codigo_acreditacion' ) );
		$query->order( 'id ASC' );
		
		$db->setQuery( $query );
		
		$objects = $db->loadObjectList();
		
		$count = count( $objects );
		
		$pages = ceil( $count / 20 );
		
		$response->pages = $pages;
		$response->count = $count;
		
		return $response;
		
	}

	/**
	* Get if PDF exists or not
	*
	* @return { bool } true if PDF exists, false otherwise
	*/
	public function existPDF( $pdf = NULL ){


		if( ! is_string( $pdf ) )
			$pdf = $this->PDF;

		if( empty( $pdf ) )
			return false;

		$uri = &JURI::getInstance();
		$url = $uri->root();

		if( ! @file_get_contents( $url . 'pdfs/'. strtoupper( $this->type ) . '/' . $pdf . '.pdf' ) ){
			return false;
		}

		return true;
	}

	/**
	* Gets all the acreditation files from this model
	*
	* @return { array } array of documents related to
	*
	*/
	public function getAcreditaciones(){

		// Initialize
		$acreditaciones = array();
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );


		// Query
		$query->select( '*' );
		$query->from( self::TABLE );
		$query->where( 'razon_social = "' . $this->razon_social .'"');

		$db->setQuery( $query );

		$objects = $db->loadObjectList();

		$_current = '';

		foreach ( $objects as $key => $object ) {
			
			$acreditacion = ( object )array();

			if( $_current != $object->codigo_acreditacion ){

				$acreditacion->id = $object->id;
				$acreditacion->codigo = $object->codigo_acreditacion;
				$acreditacion->existPDF = $this->existPDF( $object->PDF );

				array_push( $acreditaciones, $acreditacion );

				$_current = $object->codigo_acreditacion;
			}
			
		}



		return $acreditaciones;

	}

	/**
	 * API of the class
	 * 
	 * @return { void }
	 */
	
	protected function API(){
		
		// Instance an object with defaults
		$Object = new DirectorioModelLab1();
		
		// Instance an object with args not ID
		$args = array(
				'attribue'	=> 'any'
		);
		
		$Object = new DirectorioModelLab1();
		$Object->instance( $args );
		$Object->save(); // saves a new item
		
		//---------------
		// Instance an object with args with the ID
		// Result will be the model from DB merge the fields passed
		$args = array(
					'id' 			=> 1
				,	'termino' 		=> 'sample'
				,	'descripcion'	=> 'example'
				,	'clave' 		=> 'E'
		);
		
		$Object = new DirectorioModelLab1();
		$Object->instance( $args );
		$Object->save(); // It will update the object with the id
		$Object->delete(); // It will delete the object with the id

		// Instance an object with args with the ID		
		$Object = new DirectorioModelLab1( 1 );

		// To get the objects with the table
		$Object = new DirectorioModelLab1();
		$object->getObjects();


		// To get the objects with conditions
		$wheres = array(
			0 => ( object ) array(
					'key' => 'id'
				,	'value' => '3'
				,	'condition' => '='
				,	'glue' => 'AND'
			)
		);

		$Object = new DirectorioModelLab1();
		$Object->getObjects( $wheres );
		
		
	}
}
?>