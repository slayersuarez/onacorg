<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die();

//get the hosts name
jimport('joomla.environment.uri' );
$host = JURI::root();

//add the links to the external files into the head of the webpage (note the 'administrator' in the path, which is not nescessary if you are in the frontend)
$document =& JFactory::getDocument();
$document->addScript( '//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js');
$document->addScript( $host.'administrator/components/com_directorio/assets/js/libs/fileuploader/fileuploader.js');
$document->addStyleSheet($host.'administrator/components/com_directorio/assets/js/libs/fileuploader/fileuploader.css');
$document->addStyleSheet($host.'administrator/components/com_directorio/assets/css/style.css');
$document->addScript( $host.'administrator/components/com_directorio/assets/js/misc/misc.js');
$document->addScript( $host.'administrator/components/com_directorio/assets/js/views/directorio.js');
$document->addScript( $host.'administrator/components/com_directorio/assets/js/models/directorio.js');
$document->addScript( $host.'administrator/components/com_directorio/assets/js/controllers/directorio.js');
$document->addScript( $host.'administrator/components/com_directorio/assets/js/handlers/directorio.js');
$document->addScript( $host.'administrator/components/com_directorio/assets/js/misc/file.js');
?>

<script type="text/javascript">

Joomla.submitbutton = function( task ){
	
	
	if( task == 'termino.save' ){

		termino = document.getElementById("termino");

		if( termino.value.length == 0 ){
			alert("El termino es obligatorio");
			return;
		}
	}

	Joomla.submitform(task, document.getElementById('adminForm'));
}

</script>


<form action="index.php" method="post" name="adminForm" id="adminForm" class="form-validate" enctype="multipart/form-data">

	<fieldset class="adminform">
		<legend>Tipo de organismo</legend>

		<label>Tipo de Organismo: </label>
		<select name="type" id="select-tipo_organismo">
			<option value="Lab1">Laboratorios de Ensayo</option>
			<option value="Lac1">Laboratorios de Calibraci&oacute;n</option>
			<option value="Lcl">Laboratorios Cl&iacute;nico</option>
			<option value="Cda">Centros de Diagn&oacute;stico Automotor</option>
			<option value="Ocp">Organismos de Certificaci&oacute;n de Personas</option>
			<option value="Cpr">Organismos de Certificaci&oacute;n de Producto</option>
			<option value="Csg">Organismos de Certificaci&oacute;n de Sistemas de Gesti&oacute;n</option>
			<option value="Oin">Organismos de Inspecci&oacute;n</option>
			<option value="Cepplus">Centros de Reconocimiento de Conductores</option>
			<option value="Cdf">Organismos de Certificaci&oacute;n de Documentos y Firmas</option>
		</select>
	</fieldset>

	<fieldset class="adminform">
		<legend>Archivo Excel</legend>
		<p>Asegúrese que su archivo de excel conserva el mismo formato, puesto que los campos adicionales que no coincidan con la base de datos no se guardarán.</p>
		<ul class="excel-list">
		</ul>
		<div id="excel-uploader">       
			<noscript>          
			    <p>habilita javascript en tu navegador para poder subir archivos.</p>
			    <!-- or put a simple form for upload here -->
			</noscript>         
		</div>

		<p class="import-label">Cuando todos los archivos indiquen 100%, haga click en "Importar" para comenzar la importación de datos.</p>
		<a href="#" id="importar-datos">Importar datos</a>
		<p class="truncate-label"></p>
		<ul class="excel-parse-list">

		</ul>
	</fieldset>

	<fieldset class="adminform">
		<legend>Archivos PDF</legend>
		<p>Puede que los nombres de archivos PDF que no coincidan con el excel cargado anteriormente no se enlazarán con un registro pero si se subirán al servidor.</p>
		<ul class="pdf-list">
		</ul>
		<div id="pdf-uploader">       
			<noscript>          
			    <p>habilita javascript en tu navegador para poder subir archivos.</p>
			    <!-- or put a simple form for upload here -->
			</noscript>         
		</div>
	</fieldset>

	<input type="hidden" name="option" value="com_directorio" />
	<input type="hidden" name="task" value="organismo.save" />
	<input type="hidden" name="id" value="" />
	
</form>