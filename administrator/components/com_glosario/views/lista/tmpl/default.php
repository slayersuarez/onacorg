<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die();

//get the hosts name
jimport('joomla.environment.uri' );
$host = JURI::root();
$document =& JFactory::getDocument();

$document->addStyleSheet($host.'administrator/components/com_glosario/assets/css/style.css');
$model = new GlosarioModelTermino();
//$model->cleanParams();
?>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<form action="index.php" method="post" name="adminForm">

	<!-- Published Filter -->
    <div>
        <h3>Filtros</h3>
        <table>
	        <tr>
	            <td><label>Termino</label></td>
	            <td><input type="text" name="termino" id="termino" value="<?php 
	            		foreach ( $model->filters as $key => $filter ) {
	            			if( $filter->key == 'termino' )
	            				echo ltrim( $filter->value );
	            		}
	            	?>">
	            </td>
	        	<td><input type="button" onclick="Joomla.submitbutton('lista.buscar');" value="Buscar" ></td>
	            <td><a href="#" onclick="Joomla.submitbutton('lista.limpiar');">Limpiar</a></td>
	        </tr>
        </table>
    </div>
	
	<div class="editcell">
		<table class="adminlist">
			<!-- Encabezado -->
			<thead>
				<tr>
					<th width="20">
						<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $this->terminos ); ?>);" />
					</th>
					<th width="5">ID</th>
					<th>Termino</th>
					<th>Descripción</th>
				</tr>
			</thead>
			
			<!-- Elementos -->
			<tbody>
				<?php
					
					$n = 0;
					
					//loop for the elements
					foreach ( $this->terminos as $termino ):
						
						// Create the checkboxes
						$checked = JHtml::_( 'grid.id', $n, $termino->id );
				?>
				<tr>
					<td><?php echo $checked; ?></td>
					<td><?php echo $termino->id; ?></td>
					<td><a href="index.php?option=com_glosario&render=Edit&id=<?php echo $termino->id; ?>"><?php echo $termino->termino; ?></a></td>
					<td><?php echo $termino->descripcion; ?></td>
				</tr>
				<?php
						$n++;
					endforeach;
				?>
			</tbody>
		</table>
	</div>
	<input type="hidden" name="option" value="com_glosario" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="controller" value="lista" />
</form>