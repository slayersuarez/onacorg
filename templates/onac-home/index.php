<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" >
    <?php JHTML::_('behavior.mootools'); ?>
    <jdoc:include type="head" />
    <link href='http://fonts.googleapis.com/css?family=Cuprum' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css" type="text/css" />
    <link rel="stylesheet" href="css/template.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/template.css" type="text/css" />
    <link href="jquery/css/the-css-file.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>

    <script src="jquery/js/jquery.spritely-0.6.js" type="text/javascript"></script>
    <script src="jquery/js/scriptsheet.js" type="text/javascript"></script>

    <script>
        ( function($, window, document){

        function adjustMobile(){

          var userAgent = navigator.userAgent;

          if ( ( userAgent.match(/iPhone/i) ) || ( userAgent.match( /iPod/i ) )
            || ( userAgent.match( /iPad/i ) )
            || ( userAgent.match( /Android/i ) ) || ( userAgent.match( /Blackberry/i ) )
            || ( userAgent.match( /Blazer/i ) ) || ( userAgent.match( /Bolt/i ) )
            || ( userAgent.match( /GoBrowser/i ) ) || ( userAgent.match( /Windows Phone/i ) ) ) {
           
                $('body').css({width: '1308px'} );
          }
          
        }

        $( document ).ready( function(e){

            adjustMobile();
        });

        })(jQuery, this, this.document, undefined);

    </script>
</head>

<body>
    <header>
        <div class="header-topbar">
            <div class="wrapper-header-topbar-content">
                <div class="clock">
                  <jdoc:include type="modules" name="clock" style="xhtml" />
                </div>

                <div class="wrapper-logo">
                    <div id="logoh1"></div>
                    
                    <a href="index.php" target="_top">
                        <div id="logoh2"></div>
                    </a>
                </div>

                <div class="wrapper-menutop">
                    <ul>
                        <li>
                            <div id="bt1">
                                <a href="index.php" target="_top">
                                    <img src="jquery/img/bg_links.png" width="116" height="33" id="ov1" />
                                    <img src="jquery/img/bt-home.png" width="116" height="33" id="bi1a" />
                                    <img src="jquery/img/bt-homeh.png" width="116" height="33" id="bi2a" />
                                </a>
                            </div>
                        </li>

                        <li>
                            <div id="bt2">
                                <a href="index.php/contactenos" target="_top">
                                    <img src="jquery/img/bg_links.png" width="137" height="33" id="ov2" />
                                    <img src="jquery/img/bt-contacto.png" width="137" height="33" id="bi1b" />
                                    <img src="jquery/img/bt-contactoh.png" width="137" height="33" id="bi2b" />
                                </a>
                            </div>
                        </li>

                        <li>
                            <div id="bt3">
                                <a href="index.php?option=com_directorio&template=onac-component" target="_top">
                                    <img src="jquery/img/bg_links.png" width="161" height="34" id="ov3" />
                                    <img src="jquery/img/bt-directorio-pymes.png" width="161" height="34" id="bi1c" />
                                </a>
                            </div>
                        </li>

                        <li>
                            <div id="btcalendar">
                                <a href="index.php?option=com_jevents&task=month.calendar&Itemid=171" target="_top">
                                    <img src="jquery/img/bg_links.png" width="34" height="33" id="ovcalendar" title="Calendario" />
                                    <img src="jquery/img/bt-calendar.png" width="34" height="33" id="bicalendar" />
                                </a>
                            </div>
                        </li>

                        <li>
                            <div id="bt-sitemap">
                                <a href="index.php/mapa-del-sitio" target="_top">
                                    <img src="jquery/img/bt-sitemap.png" width="34" height="33" title="mapa del sitio" />
                                </a>
                            </div>
                        </li>

                        <li>
                            <div class="menu-topbar">
                                <jdoc:include type="modules" name="header" style="xhtml" />
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

            
        <div id="hheader">
            <!-- <img src="jquery/img/subnavi-shape-bg3.png" width="1285" height="410" id="shape-bg" /> -->
            
            <ul id="big-navi">
                <li id="lia">
                    <a href="#">
                    <img src="jquery/img/bg_links.png" width="215" height="32" id="big-ov1" />
                    <img src="jquery/img/bt-nuestra-corpv.png" width="215" height="32" />
                    <img src="jquery/img/t-nuestra-corpv.png" width="215" height="32" id="txa1" />
                    <img src="jquery/img/t-nuestra-corpv.png" width="215" height="32" id="txa2" />
                    </a>
                    
                    <img src="jquery/img/bg_links.png" width="257" height="36" id="bridgea0" />
                    <img src="jquery/img/bg_links.png" width="257" height="54" id="bridgea1" />
                    <img src="jquery/img/bg_links.png" width="286" height="54" id="bridgea2" />
                    <img src="jquery/img/bg_links.png" width="265" height="54" id="bridgea3" />
                    <img src="jquery/img/bg_links.png" width="266" height="75" id="bridgea4" />
                    <img src="jquery/img/bg_links.png" width="286" height="54" id="bridgea5" />
                    
                    <ul id="subnav1">
                        <li id="sublia1">
                            <a href="index.php?option=com_content&view=article&id=9&Itemid=112" target="_top">
                                <img src="jquery/img/quienes-somosh.png" width="318" height="47" id="subia2" />
                                <img src="jquery/img/quienes-somos.png" width="318" height="47" id="subia1" />
                                <img src="jquery/img/bg_links.png" width="318" height="47" id="subova1" />
                            </a>
                        </li>
                        <li id="sublib1">
                            <a href="index.php?option=com_content&view=article&id=20&Itemid=142" target="_top">
                                <img src="jquery/img/comunidad-onach.png" width="296" height="47" id="subib2" />
                                <img src="jquery/img/comunidad-onac.png" width="296" height="47" id="subib1" />
                                <img src="jquery/img/bg_links.png" width="296" height="47" id="subova2" />
                            </a>
                        </li>
                        <li id="sublic1">
                            <a href="index.php?option=com_content&view=article&id=36&Itemid=155" target="_top">
                                <img src="jquery/img/convocatoriah.png" width="281" height="47" id="subic2" />
                                <img src="jquery/img/convocatoria.png" width="281" height="47" id="subic1" />
                                <img src="jquery/img/bg_links.png" width="281" height="47" id="subova3" />
                            </a>
                        </li>
                        <li id="sublid1">
                            <a href="index.php?option=com_content&view=article&id=32&Itemid=156" target="_top">
                                <img src="jquery/img/contratacionh.png" width="284" height="65" id="subid2" />
                                <img src="jquery/img/contratacion.png" width="284" height="65" id="subid1" />
                                <img src="jquery/img/bg_links.png" width="284" height="65" id="subova4" />
                            </a>
                        </li>
                        <li id="sublie1">
                            <a href="index.php?option=com_jevents&task=month.calendar&Itemid=171" target="_top">
                                <img src="jquery/img/actualidadh.png" width="332" height="47" id="subie2" />
                                <img src="jquery/img/actualidad.png" width="332" height="47" id="subie1" />
                                <img src="jquery/img/bg_links.png" width="332" height="47" id="subova5" />
                            </a>
                        </li>
                    </ul>
                </li>
                <li id="lib">
                    <a href="#">
                    <img src="jquery/img/bg_links.png" width="225" height="32" id="big-ov2" />
                    <img src="jquery/img/bt-serv-acreditacionv.png" width="225" height="32" />
                    <img src="jquery/img/t-serv-acreditacionv.png" width="225" height="32" id="txb1" />
                    <img src="jquery/img/t-serv-acreditacionv.png" width="225" height="32" id="txb2" />
                    </a>
                    
                    <img src="jquery/img/bg_links.png" width="515" height="325" id="bridgeb1" />
                    
                    <ul id="subnav2">
                        <li id="sublia2">
                            <a href="index.php?option=com_content&view=article&id=44&Itemid=163" target="_top">
                                <img src="jquery/img/portafolio-de-servicios-acrh.png" width="329" height="72" id="subia2b" />
                                <img src="jquery/img/portafolio-de-servicios-acr.png" width="329" height="72" id="subia1b" />
                                <img src="jquery/img/bg_links.png" width="329" height="72" id="subovb1" />
                            </a>
                        </li>
                        <li id="sublib2">
                            <a href="index.php?option=com_content&view=article&id=94&Itemid=184" target="_top">
                                <img src="jquery/img/como-acceder-servh.png" width="279" height="57" id="subib2b" />
                                <img src="jquery/img/como-acceder-serv.png" width="279" height="57" id="subib1b" />
                                <img src="jquery/img/bg_links.png" width="279" height="57" id="subovb2" />
                            </a>
                        </li>
                        <li id="sublic2">
                            <a href="index.php?option=com_content&view=article&id=102&Itemid=214" target="_top">
                                <img src="jquery/img/directorio-de-acrh.png" width="262" height="41" id="subic2b" />
                                <img src="jquery/img/directorio-de-acr.png" width="262" height="41" id="subic1b" />
                                <img src="jquery/img/bg_links.png" width="262" height="41" id="subovb3" />
                            </a>
                        </li>
                        <li id="sublid2">
                            <a href="index.php?option=com_content&view=article&id=76&Itemid=190" target="_top">
                                <img src="jquery/img/aspectos-tecnicos-acrh.png" width="314" height="57" id="subid2b" />
                                <img src="jquery/img/aspectos-tecnicos-acr.png" width="314" height="57" id="subid1b" />
                                <img src="jquery/img/bg_links.png" width="314" height="57" id="subovb4" />
                            </a>
                        </li>
                        <li id="sublie2">
                            <a href="index.php?option=com_glosario&template=onac-component" target="_top">
                                <img src="jquery/img/glosarioh.png" width="445" height="38" id="subie2b" />
                                <img src="jquery/img/glosario.png" width="445" height="38" id="subie1b" />
                                <img src="jquery/img/bg_links.png" width="445" height="38" id="subovb5" />
                            </a>
                        </li>
                    </ul>
                </li>
                <li id="lic">
                    <a href="#">
                    <img src="jquery/img/bg_links.png" width="225" height="32" id="big-ov3" />
                    <img src="jquery/img/bt-interactuav.png" width="225" height="32" />
                    <img src="jquery/img/t-interactuav.png" width="225" height="32" id="txc1" />
                    <img src="jquery/img/t-interactuav.png" width="225" height="32" id="txc2" />
                    </a>
                    
                    <img src="jquery/img/bg_links.png" width="505" height="275" id="bridgec1" />
                    
                    <ul id="subnav3">
                        <li id="sublia3">
                            <a href="index.php?option=com_content&view=article&id=97&Itemid=206" target="_top">
                                <img src="jquery/img/contactenos.png" width="342" height="30" id="subia2c" />
                                <img src="jquery/img/contactenosh.png" width="342" height="30" id="subia1c" />
                                <img src="jquery/img/bg_links.png" width="342" height="30" id="subovc1" />
                            </a>
                        </li>
                        <li id="sublib3">
                            <a href="index.php?option=com_content&view=article&id=93&Itemid=208" target="_top">
                                <img src="jquery/img/propuestas-recom.png" width="411" height="28" id="subib2c" />
                                <img src="jquery/img/propuestas-recomh.png" width="411" height="28" id="subib1c" />
                                <img src="jquery/img/bg_links.png" width="411" height="28" id="subovc2" />
                            </a>
                        </li>
                        <li id="sublic3">
                            <a href="index.php?option=com_content&view=article&id=99&Itemid=209" target="_top">
                                <img src="jquery/img/quejas-onac.png" width="454" height="28" id="subic2c" />
                                <img src="jquery/img/quejas-onach.png" width="454" height="28" id="subic1c" />
                                <img src="jquery/img/bg_links.png" width="454" height="28" id="subovc3" />
                            </a>
                        </li>
                        <li id="sublid3">
                            <a href="index.php?option=com_content&view=article&id=98&Itemid=207" target="_top">
                                <img src="jquery/img/quejas-conform.png" width="481" height="29" id="subid2c" />
                                <img src="jquery/img/quejas-conformh.png" width="481" height="29" id="subid1c" />
                                <img src="jquery/img/bg_links.png" width="481" height="29" id="subovc4" />
                            </a>
                        </li>
                        <li id="sublie3">
                            <a href="index.php?option=com_content&view=article&id=100&Itemid=210" target="_top">
                                <img src="jquery/img/consulta-publica.png" width="484" height="30" id="subie2c" />
                                <img src="jquery/img/consulta-publicah.png" width="484" height="30" id="subie1c" />
                                <img src="jquery/img/bg_links.png" width="484" height="30" id="subovc5" />
                            </a>
                        </li>
                        <li id="sublif3">
                            <a href="#">
                                <img src="jquery/img/consulta-tramite.png" width="458" height="30" id="subif2c" />
                                <img src="jquery/img/consulta-tramiteh.png" width="458" height="30" id="subif1c" />
                                <img src="jquery/img/bg_links.png" width="458" height="30" id="subovc6" />
                            </a>
                        </li>
                        <li id="sublig3">
                            <a href="index.php?option=com_content&view=article&id=103&Itemid=215" target="_top">
                                <img src="jquery/img/ayudas-navh.png" width="403" height="34" id="subig2c" />
                                <img src="jquery/img/ayudas-nav.png" width="403" height="34" id="subig1c" />
                                <img src="jquery/img/bg_links.png" width="403" height="34" id="subovc7" />
                            </a>
                        </li>
                    </ul>
                    
                </li>
            </ul>
        </div>

        <div id="logosentence">Organismo Nacional de Acreditación de Colombia</div>
    </header>

    <div id="all"> 
        <!--aqui ponemos todo lo de 1000px-->
        <div id="content-content">
            <div id="content_noticias">
                <div id="destacados">
                    <jdoc:include type="modules" name="destacados" style="xhtml" />
                </div>

                <div id="noticias">
                    <jdoc:include type="modules" name="noticias" style="xhtml" />
                </div>
            </div>
        </div>
    </div>

    <div id="content_pie">
        <div id="content_piedos">
            <div id="pie">
                <jdoc:include type="modules" name="pie" style="xhtml" />
            </div>

            <div id="content_copy">
                <table width="670" style="margin:0px auto;" border="0">
                    <tr>
                        <td colspan="2" align="center">
                            <div id="copy">
                                Organismo Nacional de Acreditación de Colombia - ONAC      Carrera 7A No. 69-64     PBX: 742 7592 - 321 2938   FAX: Ext. 117

                                <a href="http://www.creandopaginasweb.com/" target="_blank"></a>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td width="494" align="right">
                            <span style="text-align:right; color: #FFFFFF; font-family: arial narrow; font-size: 15px;">
                                Bogotá D.C., Colombia onac@onac.org.co Sitio Web Desarrollado Por </span>
                        </td>
                        <td width="166" align="center" style="padding-left:10px;" >
                            <div id="logo">
                                <a href="http://www.creandopaginasweb.com/" target="_blank"></a>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</body>
</html>