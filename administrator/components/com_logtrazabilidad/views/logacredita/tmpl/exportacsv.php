<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	com_logtrazabilidad
 */

// No direct access.
defined('_JEXEC') or die;
jimport( 'joomla.environment.uri' );

// Load the tooltip behavior.
//JHtml::_('behavior.tooltip');
//JHtml::_('behavior.multiselect');
//JHtml::_('behavior.modal');
JHtml::_('behavior.framework');
$doc = JFactory::getDocument();

$doc->addScriptDeclaration( '
		var url = "'.JURI::base().'"
	' );

$canDo = LogTrazabilidadHelper::getActions();
$user = JFactory::getUser();
$loggeduser = JFactory::getUser();

//fb( $this->state );
?>
<table class="adminlist">
	<thead>
		<tr>
			<th class="nowrap">
				Id del usuario
			</th>
			<th class="nowrap">
				Nombre del usuario
			</th>
			<th class="nowrap">
				Tipo de gesti&oacute;n
			</th>
			<th class="nowrap">
				Descripci&oacute;n de la activida
			</th>
			<th class="nowrap">
				Raz&oacute;n social del OEC
			</th>
			<th class="nowrap">
				Nit OEC
			</th>
			<th class="nowrap">
				Fecha y Hora
			</th>
		</tr>
	</thead>
	<tbody>
	<?php /*fb( $this->items );*/ foreach ($this->items as $i => $item) :

		// si no es super usuario, no puede ver los registros
		if ( $canDo->get('core.admin') ) {
	?>
		<tr class="row<?php echo $i % 2; ?>">
			<td class="center"><?php echo $item->id_usuario?></td>
			<td class="center"><?php echo ucwords($item->nombre)?></td>
			<td class="center"><?php echo ucfirst($item->tipo_gestion)?></td>
			<td class="left"><?php echo $item->descripcion_actividad?></td>
			<td class="center"><?php echo $item->razon_social_oec?></td>
			<td class="center"><?php echo $item->nit_oec?></td>
			<td class="center"><?php echo $item->created_on?></td>
		</tr>
	<?php 
		}
	endforeach; 
	?>
	</tbody>
</table>