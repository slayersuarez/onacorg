/**
*
* View for { Directorio }
*
*
**/

( function( $, window, document, Utilities ){
	
	var DirectorioView = function( a ){
		
		// atributes, selector or global vars
		this.keyButton = '.alphabet-letter';
		this.tooltip = '.tooltip-descripcion-termino';
		this.listaContent = '.wrapp-lista-content-organismos';
		this.paginator = '.paginator';
		this.loader = '<span class="loader"><div class="bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div></span><span style="display:inline-block; width:100%; text-align:center;">Cargando...</span>';
		
	};
	
	DirectorioView.prototype ={

			/**
			* Render as many fields as param has been recovered
			* and create an autocomplete field with results
			*
			*/
			onCompleteLoadParams:function( data ){

				$( '.global-notification' ).fadeOut();

				if( $.isArray( data.params ) == false && $.isEmptyObject( data.params ) == false ){
					
					// set the params in unique array
					$.each( data.params, function( key, value ){
						data.params[ key ] = Utilities.arrayUnique( value );
					});

					var markup = new EJS({ url: urlTemplates  + 'params.ejs' }).render( data );
					$( '.wrapper-buscador-avanzado' ).html( markup );
					
					$.each( data.params, function( key, value ){
						Utilities.setAutocomplete( 'input[ name="' + key + '" ]', value );
					});
					

					return;
				} 

				if( data.params.length <= 0){
					
					$( '.wrapper-buscador-avanzado' ).html( '<h3>Este tipo de organismo no contiene parametros.</h3>' );
					return;
				}
			}


		,	onCompleteSearch: function( data ){
			console.log(data);
					
				if( data.status == 200 ){

					if( data.results.length <= 0 ){

						Utilities.showNotification( 'error', 'No hay resultados para mostrar.', 0 );
					}

					var markup = new EJS({ url: urlTemplates  + 'lista.ejs' }).render( data );
					$( this.paginator ).html( '' );

					if( typeof data.paginator != 'undefined' ){

						if(  data.paginator != null ){

							var paginator = new EJS({ url: urlTemplates  + 'paginator.ejs' }).render( data );

							window.DirectorioHandler.maxPages = data.paginator.pages;
							$( this.paginator ).html( paginator );
						}
					}
					

					$( this.listaContent ).html( markup );
					
				}
			}

			/**
			* Begin the process, shows a loader and executes a callback after time out passed through
			*
			* @param { string } the string selector render loading on
			* @param { int } milliseconds after execute the callback
			* @param { function } the function that triggers after the time set up previously
			*/
		,	onProcess: function( selector, timeOut, callback ){

				Utilities.showNotification( 'success', 'Realizando consulta', 0 );

				$( selector ).html( this.loader );

				return setTimeout( function(){

					if( typeof callback != 'undefined' ){
						callback.call();
					}

				}, timeOut );
			}
		
			
	};
	

	// Use this way when script is loaded at the end of the page
	// Expose to global scope
	window.DirectorioView = new DirectorioView();
	
})( jQuery, this, this.document, this.Misc, undefined );