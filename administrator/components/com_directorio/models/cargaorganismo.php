<?php

// Joomla calls and runtimes
defined( '_JEXEC' ) or die();
jimport( 'joomla.application.component.model' );

// Initializes the Class
class DirectorioModelCargaOrganismo extends JModel {
	
	
	/**
	 * Object Id
	 * @var int
	 */
	var $id_log_acredita;

	/**
	 * Attribute
	 * @var string
	 */
	var $tabla_organismo;

	/**
	 * Attribute
	 * @var string
	 */
	var $identificador;

	/**
	 * Attribute
	 * @var int
	 */
	var $id_organismo;

	/**
	 * Attribute
	 * @var int
	 */
	var $id_usuario;

	/**
	 * Attribute
	 * @var string
	 */
	var $tipo_gestion;

	/**
	 * Attribute
	 * @var string
	 */
	var $descripcion_actividad;

	/**
	 * Attribute
	 * @var string
	 */
	var $razon_social_oec;

	/**
	 * Attribute
	 * @var string
	 */
	var $nit_oec;

	/**
	 * Attribute
	 * @var date
	 */
	var $created_on;

	/**
	 * Attribute
	 * @var int
	 */
	var $created_by;

	/**
	 * Attribute
	 * @var date
	 */
	var $modified_on;

	/**
	 * Attribute
	 * @var int
	 */
	var $modified_by;

	/**
	 * Attribute
	 * @var string
	 */
	var $parametros;

	/**
	 * Get if the object exists
	 * @var bool
	 */
	var $exists = false;

	/**
	 * Cache of results, data, queries
	 * @var unknown
	 */
	var $data;

	/**
	 * Filters array
	 * @var unknown
	 */
	var $filters = array();

	/**
	 * Constant for table
	 * @var string
	 */
	const TABLE = 'log_carga_organismos';

	/**
	 * Constant for filters states
	 * @var string
	 */
	const FILTER_STATE = 'filter.cda.';


	/**
	* Models Attrs map
	*
	*/
	var $attrs_map = array(

			'id_log_acredita'
		,	'tabla_organismo'
		, 	'identificador'
		,	'id_organismo'
		,	'id_usuario'
		,	'tipo_gestion'
		,	'descripcion_actividad'
		,	'razon_social_oec'
		,	'nit_oec'
		,	'created_on'
		,	'created_by'
		,	'modified_on'
		,	'modified_by'
		,	'parametros'

	);


	/**
	* Instance
	*
	*/
	public function instance( $args = NULL ){

		if( ! is_array( $args ) )
			return NULL;


		foreach ( $this->attrs_map as $key => $attr ) {
			
			$this->$attr = $args[ $attr ];
		}
	}

	/**
	 * Fill the model attributes with the passed arguments.
	 *
	 * @param { arr } Object arguments
	 */
	protected function fill( $args = NULL ){


		if ( ! is_array( $args ) )
			return false;
		
	
		// Get object in DB			
		if ( is_numeric( $args[ $this->attrs_map[ 0 ] ] ) ){

			$object = $this->getObject( $args[ $this->attrs_map[ 0 ] ] );
			
			if( is_object( $object ) ){
				foreach ( $this->attrs_map as $attr ) {
					
					if ( isset( $object->$attr ) )
						$this->$attr = $object->$attr;
				}
			}

		}

	
		// Merge attributes	when id is not passed through
		foreach ( $this->attrs_map as $attr ) {
			if ( isset( $args[ $attr ] ) )
				$this->$attr = $args[ $attr ];
		}

		// Set exists to true.
		$this->exists = true;
	
	}

	/**
	 * Get a single Object
	 * 
	 * @param { int } the id or attributes of the Object.
	 * @return { bool/object } the object returned or false otherwise
	 */
	public function getObject( $id = 1 ){
		
		if( ! is_numeric( $id ) )
			return false;
		
		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );
		
		$c = get_called_class();  

		$query->select( '*' );
		$query->from( $c::TABLE );
		$query->where( $this->attrs_map[ 0 ].' = ' . $id );
		
		$db->setQuery( $query );
		
		return $db->loadObject();
		
	}
	
	/**
	 * Get Objects collection
	 *
	 * @param { int } the id or attributes of the Object.
	 * @return { bool/object } the object returned or false otherwise
	 **/
	public function getObjects( $wheres = NULL , $prlimits = array() , $params ){
		
		if( !isset( $prlimits[ 'limitstart' ] ) )
			$prlimits[ 'limitstart' ] = $this->getState('list.start');
			
		if( !isset( $prlimits[ 'limit' ] ) )	
			$prlimits[ 'limit' ] = $this->getState('list.limit');
		
		$result = array();
		
		if( ! is_array( $wheres ) )
			$wheres = array();
			
		if( ! is_array( $params ) )	
			$params = array();
		
		$query = $this->buildQuery( $wheres , $params );
		//fb( $query->__toString() );
		$this->data = $this->_getList( $query , $prlimits[ 'limitstart' ] , $prlimits[ 'limit' ] );
		$this->counts = $this->_getListCount($query); 
		
		foreach ( $this->data as $obj ){

			$args = array();
			
			foreach ( $obj as $key => $attr ) {
				
				$args[ $key ] = $obj->$key;
			}
			
			$estaClase = get_class( $this );
	
			$object = new $estaClase();
			$object->fill( $args );
			array_push( $result, $object );
			
		}
	
		return $result;
	
	}

	/**
	 * Save a new object or update an exist.
	 *
	 * @param { string } the type of the response
	 * ( string returns the error string or "" )
	 * ( bool return false or true )
	 * ( object returns the response with error_message, status true or false )
	 * @return { string  } the query string calling the insert query
	 *
	 */
	public function save( $return = 'string' ){

		// Initialize
		$db = JFactory::getDbo();
		$response = ( object ) array();
		$model = ( object ) array();

		if( ! is_string( $return ) )
			$return = 'string';

		// Fetch the model attributes with the $model's var
		foreach ( $this->attrs_map as $attribute ) {
			$model->$attribute = $this->$attribute;
		}
		
		$c = get_called_class(); 
		
		
		$id =  $this->attrs_map[0];

		// If id exists, update the model
		// If id doesn't exist, insert a new row in database
		if( $model->$id == NULL || $model->$id == "" ){

			if (! $db->insertObject( $c::TABLE, $model ) ) {

				if( $return == 'string' ){
					return "No se pudo guardar el registro. " . $db->stderr();
				}

				if( $return == 'bool' ){
					return false;
				}

				if( $return == 'object' ){
					$response->status = false;
					$response->error = "No se pudo guardar el registro. " . $db->stderr();
					return $response;
				}
			}
			$this->insertId = $db->insertid();

			if( $return == 'string' ){
				return "";
			}

			if( $return == 'bool' ){
				return true;
			}

			if( $return == 'object' ){
				$response->status = true;
				$response->error = "";
				return $response;
			}
		}

		// Update
		if ( ! $db->updateObject( $c::TABLE, $model, $this->attrs_map[ 0 ], false ) ) {
			
			if( $return == 'string' ){
				return "No se pudo actualizar el object. " . $db->stderr();
			}

			if( $return == 'bool' ){
				return false;
			}

			if( $return == 'object' ){
				$response->status = false;
				$response->error = "No se pudo guardar el object. " . $db->stderr();
				return $response;
			}
		}

		if( $return == 'string' ){
			return "";
		}

		if( $return == 'bool' ){
			return true;
		}

		if( $return == 'object' ){
			$response->status = true;
			$response->error = "";
			return $response;
		}

	}

	
	/**
	 * Build a query for collection. Filters query are included.
	 *
	 * @param { array } wheres clausule. Clausule must be { key: 'value', value: 'value', condition:'=', glue: 'AND || OR' }
	 * @return { string } the query string calling the collection
	 *
	 */
	protected function buildQuery( $wheres = NULL ){

		// Validation
		if( ! is_array( $wheres ) )
			$wheres = array();

		// Initialize
		$db = JFactory::getDbo();
		$query  = $db->getQuery(true);
		
		// Query base		
		$query->select( "*" );
		$query->from( self::TABLE );

		
		// Wheres appending
		foreach ( $wheres as $key => $clausule ) {
			
			if( ! is_object( $clausule ) )
				continue;

			$query->where( $clausule->key . $clausule->condition . $clausule->value . ' '.trim($clausule->glue).' ' );

		}
		
		return $query;
	}

	/**
	 * API of the class
	 * 
	 * @return { void }
	 */
	protected function API(){
		
	}
}
?>