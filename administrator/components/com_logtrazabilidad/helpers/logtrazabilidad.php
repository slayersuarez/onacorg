<?php
/**
 * @author          Empresa : Sainet Ingenieria Ltda. | Desarrollador : Robinson Perdomo
 * @link            http://www.creandopaginasweb.com
 * @copyright       Copyright © 2014 Sainet Ingenieria Ltda. All Rights Reserved
 */

// No direct access
defined('_JEXEC') or die;

/**
 * LogTrazabilidad helper.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_logtrazabilidad
 * @since		1.6
 */

jimport( 'joomla.environment.request' );

class LogTrazabilidadHelper
{
	
	//public static $extension = JRequest::getString( 'extension', 'com_logtrazabilidad' );
	
	/**
	 * Configure the logTrazabilidadBar.
	 *
	 * @param	string	The name of the active view.
	 * @since	1.6
	 */
	public static function addSubmenu($vName = 'logsession')
	{
		// Groups and Levels are restricted to core.admin
		$canDo = self::getActions();
		
		if ($canDo->get('core.admin'))
		{
			JSubMenuHelper::addEntry(
				'Registros de Inicio/Cierre de Sesión',
				'index.php?option=com_logtrazabilidad&view=logsession',
				$vName == 'logsession'
			);
			JSubMenuHelper::addEntry(
				'Registros de Actividad de Acreditación',
				'index.php?option=com_logtrazabilidad&view=logacredita',
				$vName == 'logacredita'
			);
		}
	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @param	int		The category ID.
	 * @return	JObject
	 * @since	1.6
	 */
	public static function getActions()
	{
		$user	= JFactory::getUser();
		$result	= new JObject;

		$assetName = 'com_logtrazabilidad';
		$level = 'component';
	

		$actions = JAccess::getActions('com_logtrazabilidad', $level);

		foreach ($actions as $action) {
			$result->set($action->name,	$user->authorise($action->name, $assetName));
		}

		return $result;
	}

	/**
	 * Get a list of the user groups for filtering.
	 *
	 * @return  array  An array of JHtmlOption elements.
	 *
	 * @since   1.6
	 */
	public static function getGestiones()
	{
		$db = JFactory::getDbo();
		$db->setQuery(
			"SELECT a.tipo_gestion AS value, a.tipo_gestion AS text" .
			" FROM log_carga_organismos AS a GROUP BY a.tipo_gestion ORDER BY a.tipo_gestion ASC"
		);
		$options = $db->loadObjectList();

		// Check for a database error.
		if ($db->getErrorNum())
		{
			JError::raiseNotice(500, $db->getErrorMsg());
			return null;
		}

		foreach ($options as &$option)
		{
			$option->text = ucfirst($option->text);
		}

		return $options;
	}
}
?>