/**
*
* View for { Directorio }
*
*
**/

( function( $, window, document, Utilities ){
	
	var DirectorioView = function( a ){
		
		// atributes, selector or global vars
		this.attribute = 'my-atribute';
		
	};
	
	DirectorioView.prototype ={
			

			/**
			* Function header when calls the model method and gives the view the response
			*
			* @param {}
			* @return {}
			*/
			onCompleteParse: function( data ){

				var excel = data.excel.replace( /(.xlsx|.xls|.ods)$/gi, '' );

				var _class = ( data.status == 500 ) ? 'error' : 'success';
				$( '#' + excel ).removeClass();
				$( '#' + excel ).text( excel + ': '+ data.message ).addClass( _class );

				if( data.status == 500 ){
					$( '.truncate-label' ).text( data.message ).addClass( 'error' );
					return false;
				}

				return true;
			}


			/**
			* On error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				$( '.truncate-label' ).removeClass('error progress');
				$( '.truncate-label' ).text( textStatus ).addClass( 'error' );

				return;
			}

			/**
			* Render the view before parse is uploaded
			*
			*/
		,	onBeforeParse: function( files ){

				// for each file create an li
				for (var i = 0; i < files.length; i++) {
					
					var li = $( '<li>' );

					li.attr( 'id', files[i].replace( /(.xlsx|.xls|.ods)$/gi, '' ) );
					li.text( files[i] + ': Convirtiendo...' );
					li.addClass( 'progress' );
					
					li.appendTo('.excel-parse-list');
				};
			}

			/**
			* Shows a message on before truncate
			*
			*/
		,	onBeforeTruncate: function(){

				$( '.truncate-label' ).removeClass('error progress');
				$( '.truncate-label' ).text( 'Borrando datos antes de guardar los nuevos. Espere...' ).addClass( 'progress' );
			}

			/**
			* Shows a message when delete temporary files operation starts
			*
			*/
		,	onBeforeDeleteTemp:function(){

				$( '.truncate-label' ).removeClass('error progress');
				$( '.truncate-label' ).text( 'Borrando archivos y carpetas temporales...' ).addClass( 'progress' );
			}

			/**
			* Shows a message when delete temporary files ends
			*
			*/
		,	onCompleteDeleteTemp: function( data ){

				var _class = ( data.status == 500 ) ? 'error' : 'success';
				$( '.truncate-label' ).removeClass('error progress');
				$( '.truncate-label' ).text( data.message ).addClass( _class );
			}

			/**
			* Shows a message on complete truncate
			*
			*/
		,	onCompleteTruncate: function( data ){

				var _class = ( data.status == 500 ) ? 'error' : 'success';
				$( '.truncate-label' ).removeClass('error progress');
				$( '.truncate-label' ).text( data.message ).addClass( _class );
			}


			/**
			* Function header when calls the model method and gives the view the response
			*
			* @param {}
			* @return {}
			*/
		,	onCompleteComparePDF: function( data ){


				var color = ( data.status == 500 ) ? 'red' : 'green';

				var spanName = $( '<span>' );

				spanName.addClass( 'percentage' );
				spanName.text( 'Comprobando archivo...' );

				spanName.appendTo( $( '.pdf-list > li:eq('+data.params.id+')' ) );


				if( typeof data != 'object' ){
					$( '.pdf-list' ).find( ':last-child' ).find( 'span:last-child' ).text( 'Hubo un error en el sistema. Vuelva a intentarlo.' ).css( 'color', 'red' );
				}

				$( '.pdf-list' ).find( 'li:eq('+data.params.id+') > span:last-child' ).text( data.message ).css( { 'color': color, 'display': 'block' } );
			}
		
			
	};
	

	// Use this way when script is loaded at the end of the page
	// Expose to global scope
	window.DirectorioView = new DirectorioView();
	
})( jQuery, this, this.document, this.Misc, undefined );