<?php

/**
 * General View for "empresas directorio" "lista" layout
 * 
 */

// Joomla calls and runtimes
defined( '_JEXEC' ) or die();
jimport( 'joomla.application.component.view' );

class DirectorioViewLista extends JView {

	protected $items;

	// Function that initializes the view
	function display( $tpl = null ){
	
		// Add the toolbar with the CRUD modules defined
		$this->addToolbar();

		// Get all items
		$this->getItems();
	
		parent::display( $tpl );
	
	}
	
	// Show the toolbar with CRUD modules
	protected function addToolbar(){
		
		JToolBarHelper::title( "Lista de empresas en  Directorio de Acreditación", "logo" );
		JToolBarHelper::addNew( "renderEdit" );
	}

	// Get all empresas in directorio
	public function getItems(){

		// search through all models
		$results = array();

		$model_lab1 = new DirectorioModelLab1();
		$model_lac1 = new DirectorioModelLac1();
		$model_cda = new DirectorioModelCda();
		$model_cdf = new DirectorioModelCdf();
		$model_cepplus = new DirectorioModelCepplus();
		$model_cpr = new DirectorioModelCpr();
		$model_csg = new DirectorioModelCsg();
		$model_lcl = new DirectorioModelLcl();
		$model_ocp = new DirectorioModelOcp();
		$model_oin = new DirectorioModelOin();

		$all_model_results = array(

			0 => $model_lab1->getObjects(),
			1 => $model_lac1->getObjects(),
			2 => $model_cda->getObjects(),
			3 => $model_cdf->getObjects(),
			4 => $model_cepplus->getObjects(),
			5 => $model_cpr->getObjects(),
			6 => $model_csg->getObjects(),
			7 => $model_lcl->getObjects(),
			8 => $model_ocp->getObjects(),
			9 => $model_oin->getObjects()
		);

		foreach ( $all_model_results as $key => $array_result ) {
			
			$results = array_merge( $results, $array_result );
		}

		$this->items = $results;
	}

}
?>