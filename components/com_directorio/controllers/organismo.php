
<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );

// Begining of the controller
class DirectorioControllerOrganismo extends JControllerLegacy{


		/**
		* Searchs by key
		*
		*/
		protected function createTables(){

			$model = $this->getModel( 'organismo' );

			$args = array(
					'model_lab1' => $this->getModel( 'Lab1' )
				,	'model_lac1' => $this->getModel( 'Lac1' )
				,	'model_lcl' => $this->getModel( 'Lcl' )
				,	'model_cda' => $this->getModel( 'Cda' )
				,	'model_ocp' => $this->getModel( 'Ocp' )
				,	'model_cpr' => $this->getModel( 'Cpr' )
				,	'model_csg' => $this->getModel( 'Csg' )
				,	'model_oin' => $this->getModel( 'Oin' )
				,	'model_cepplus' => $this->getModel( 'Cepplus' )
				,	'model_cdf' => $this->getModel( 'Cdf' )
			);

			//$model->instance( $args );
		}

		/**
		* Gets the params from organismo selected
		*
		*/
		public function getOrganismoParams( $model = NULL, $format = 'json' ){

			$response = ( object ) array();

			if( $model == NULL )
				$model = JRequest::getVar( 'model' );


			$_model = $this->getModel( $model );

			if( $format == 'json' ){

				$response->model = $_model;
				$response->params = $_model->getParametros();
				$response->natParams = $_model->natural_names;

				echo json_encode( $response );
				die();
			}
		}

		/**
		* Gets the cities
		*
		*/
		public function getCities(){

			$model_lab1 = $this->getModel( 'Lab1' );
			$model_lac1 = $this->getModel( 'Lac1' );
			$model_cda = $this->getModel( 'Cda' );
			$model_cdf = $this->getModel( 'Cdf' );
			$model_cepplus = $this->getModel( 'Cepplus' );
			$model_cpr = $this->getModel( 'Cpr' );
			$model_csg = $this->getModel( 'Csg' );
			$model_lcl = $this->getModel( 'Lcl' );
			$model_ocp = $this->getModel( 'Ocp' );
			$model_oin = $this->getModel( 'Oin' );

			$all_model_cities = array(

				0 => $model_lab1->getCities(),
				1 => $model_lac1->getCities(),
				2 => $model_cda->getCities(),
				3 => $model_cdf->getCities(),
				4 => $model_cepplus->getCities(),
				5 => $model_cpr->getCities(),
				6 => $model_csg->getCities(),
				7 => $model_lcl->getCities(),
				8 => $model_ocp->getCities(),
				9 => $model_oin->getCities()
			);

			$cities = array();

			// get the attributes map of the models
			foreach ( $all_model_cities as $key => $array_cities ) {

				$cities = array_merge( $cities, $array_cities );
			}

			$cities = array_unique( $cities );

			natcasesort( $cities );

			return $cities;
		}


		/**
		* Search terms by criteria in specific model or any
		*
		*/
		public function search(){

			// Initialize
			ini_set('max_execution_time', 300); //300 seconds = 5 minutes
			$response = ( object )array();
			$pdfs = array();

			$criteria = JRequest::getVar( 'criteria' );
			$page = JRequest::getVar( 'page' );

			// search through specific model
			if( $criteria['tipo-organismo'] != '' ){


				$model = $this->getModel( $criteria[ 'tipo-organismo' ] );

				// construct the wheres
				$filter = $this->buildWheres( $model, $criteria );

				$response->wheres = $filter->wheres;
				
				// set paginator
				$response->paginator = $model->setPaginator( $wheres, 20 );

				// search into razon_social with RegExp so the query will be exactly
				$results = $model->getObjects( $filter->wheres, $page );


				$response->noFilterResponse = $results;

				$response->black_list = $filter->black_list;

				// Match in black list
				foreach ( $filter->black_list as $key => $term ) {

					$result = $this->match( $term, $criteria[ $term ], $results );

					if( ! empty( $result ) )
						$results = $result;
					
				}

				// validate if pdfs exists or not
				foreach ( $results as $key => $value ) {
					$value->exists = $value->existPDF();
				}

				$response->status = 200;
				$response->message = 'Búsqueda finalizada.';
				$response->results = $results;
				$response->pdfs = $pdfs;

				echo json_encode( $response );
				die();

			} else {

				// search through all models
				$results = array();

				$model_lab1 = $this->getModel( 'Lab1' );
				$model_lac1 = $this->getModel( 'Lac1' );
				$model_cda = $this->getModel( 'Cda' );
				$model_cdf = $this->getModel( 'Cdf' );
				$model_cepplus = $this->getModel( 'Cepplus' );
				$model_cpr = $this->getModel( 'Cpr' );
				$model_csg = $this->getModel( 'Csg' );
				$model_lcl = $this->getModel( 'Lcl' );
				$model_ocp = $this->getModel( 'Ocp' );
				$model_oin = $this->getModel( 'Oin' );

				$all_model_results = array(

					0 => $this->getObjects( $model_lab1, $criteria ),
					1 => $this->getObjects( $model_lac1, $criteria ),
					2 => $this->getObjects( $model_cda, $criteria ),
					3 => $this->getObjects( $model_cdf, $criteria ),
					4 => $this->getObjects( $model_cepplus, $criteria ),
					5 => $this->getObjects( $model_cpr, $criteria ),
					6 => $this->getObjects(  $model_csg, $criteria ),
					7 => $this->getObjects( $model_lcl, $criteria ),
					8 => $this->getObjects( $model_ocp, $criteria ),
					9 => $this->getObjects( $model_oin, $criteria )
				);

				// get the attributes map of the models
				foreach ( $all_model_results as $key => $array_results ) {

					$results = array_merge( $results, $array_results );
				}

				// validate if pdfs exists or not
				foreach ( $results as $key => $value ) {
					$value->exists = $value->existPDF();
				}

				$response->status = 200;
				$response->message = 'Búsqueda finalizada.';
				$response->results = $results;
				$response->paginator = NULL;
				$response->type = $criteria['tipo-organismo'];

				echo json_encode( $response );
				die();

			}

			$response->status = 500;
			$response->message = 'Parámetros en la búsqueda no válidos.';
			echo json_encode( $response );
			die();
		}

		/**
		* Match and string in key and return the array of results
		*
		* @param { string } the key of the array to look for
		* @return { array } the array filtered
		*
		*/
		public function match( $key = NULL, $term = NULL, $list = NULL ){

			if( is_string( $key ) == false || is_string( $term ) == false || is_array( $list ) == false ){
				return array();
			}

			$results = array();

			foreach ( $list  as $_key => $object ) {

				if( $term != '' ){
					if ( preg_match("/" . $term . "/i", $object->$key ) ) {
				    	array_push( $results, $object );
					}
				}
			}

			return $results;
			
		}

		/**
		* Build the wheres clausules to condition the results
		*
		* @param { stdclass } model to filter
		* @param { array } criteria
		* @return { object } black list and wheres clausules
		*/
		public function buildWheres( $model = NULL, $criteria = NULL ){

			if( $model == NULL || $criteria == NULL )
				return array();

			$black_list = array( 'razon_social' );

			$wheres = array();

			foreach ( $criteria as $key => $clausule ) {
				
				if( $key != 'tipo-organismo' and $clausule != '' ){

					if( ! in_array( $key, $black_list ) ){
						
						$condition = ( object ) array();
						$condition->key = $key;
						$condition->value = '"' . $clausule . '"';
						$condition->condition = ' =';
						$condition->glue = 'AND';
						array_push( $wheres, $condition );
					} else{
						$condition = ( object ) array();
						$condition->key = $key;
						$condition->value = "'%$clausule%'";
						$condition->condition = ' LIKE ';
						$condition->glue = ' AND ';
						array_push( $wheres, $condition );
					}
				}
			}

			$filter = ( object )array();
			$filter->wheres = $wheres;
			$filter->black_list = $black_list;

			return $filter;

		}

		/**
		* Gets the results filtered by model
		*
		*/
		public function getObjects( $model, $criteria ){

			// construct the wheres
			$filter = $this->buildWheres( $model, $criteria );

			// search into razon_social with RegExp so the query will be exactly
			$results = $model->getObjects( $filter->wheres, 0 );

			//var_dump( $results );

			// Match in black list
			/*foreach ( $filter->black_list as $key => $term ) {
				
				$result = $this->match( $term, $criteria[ $term ], $results );

				if( ! empty( $result ) )
					$results = $result;

			}*/

			return $results;
		}

		public function downloadPDF(){

			$id = JRequest::getVar( 'id' );
			$type = JRequest::getVar( 'type' );


			// Instance the right model
			$model = $this->getModel( $type );

			// Instance
			$model->instance( $id );

			if( ! empty( $model->PDF ) ){

				$uri = &JURI::getInstance();
				$url = $uri->root();
				
				// We'll be outputting a PDF
				header('Content-type: application/pdf');
				
				// It will be called downloaded.pdf
				header('Content-Disposition: attachment; filename="' . $model->PDF . '"');
				
				// The PDF source is in original.pdf
				echo file_get_contents( $url . 'pdfs/'. strtoupper( $model->type ) . '/' . $model->PDF . '.pdf' );

				die();
			}

		}
}
?>