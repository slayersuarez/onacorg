	<?php

/**
* Default Template for object
*
*/

// Initialize
defined('_JEXEC') or die;
$app = JFactory::getApplication(); // Joomla application
$uri = &JURI::getInstance(); // base url object
$url = $uri->root(); // url root
$component_images_url = $url . 'components/com_directorio/assets/images/';

$document = JFactory::getDocument();

// styles
$document -> addStyleSheet( $url . 'components/com_directorio/assets/css/style.css' );
$document -> addStyleSheet( $url . 'components/com_directorio/assets/css/loader.css' );
$document -> addStyleSheet( $url . 'components/com_directorio/assets/css/jquery-ui-1.10.3.custom.css' );
$document -> addStyleSheet( $url . 'components/com_directorio/assets/js/libs/select2/select2.css' );

// scripts
$document -> addScript( $url . 'components/com_directorio/assets/js/libs/jquery-1.10.2.min.js');
$document -> addScript( $url . 'components/com_directorio/assets/js/libs/jquery-ui-1.10.3.custom.js');
$document -> addScript( $url . 'components/com_directorio/assets/js/libs/select2/select2.js');
$document -> addScript( $url . 'components/com_directorio/assets/js/libs/select2/select2_locale_es.js');
$document -> addScript( $url . 'components/com_directorio/assets/js/libs/ejs.js');
$document -> addScript( $url . 'components/com_directorio/assets/js/misc/misc.js');
$document -> addScript( $url . 'components/com_directorio/assets/js/views/directorio.js');
$document -> addScript( $url . 'components/com_directorio/assets/js/models/directorio.js');
$document -> addScript( $url . 'components/com_directorio/assets/js/controllers/directorio.js');
$document -> addScript( $url . 'components/com_directorio/assets/js/handlers/directorio.js');
$document -> addScriptDeclaration( 'var url = "' . $url .'"; var urlTemplates = "' . $url . 'components/com_directorio/assets/js/templates/";' );

?>
<div class="wrapper-component-directorio">

	<div class="box-directorio">
		<h2 class="directorio-header">Directorio de Acreditación</h2>

		<div class="wrapper-buscador">
			<h2>BUSCADOR BÁSICO</h2>
			<form class="buscador-directorio" id="buscador-directorio">
				<ul>
					<li><label for="tipo-organismo" class="no-margin">Tipo de Organismo:</label></li>
					<li>
						<select name="tipo-organismo" id="select-tipo_organismo">
							<option value="">---</option>
							<option value="Lab1">Laboratorios de Ensayo</option>
							<option value="Lac1">Laboratorios de Calibraci&oacute;n</option>
							<option value="Lcl">Laboratorios Cl&iacute;nicos</option>
							<option value="Cda">Centros de Diagn&oacute;stico Automotor</option>
							<option value="Ocp">Organismos de Certificaci&oacute;n de Personas</option>
							<option value="Cpr">Organismos de Certificaci&oacute;n de Producto</option>
							<option value="Csg">Organismos de Certificaci&oacute;n de Sistemas de Gesti&oacute;n</option>
							<option value="Oin">Organismos de Inspecci&oacute;n</option>
							<option value="Cepplus">Centros de Reconocimiento de Conductores</option>
							<option value="Cdf">Organismos de Certificaci&oacute;n de Documentos y Firmas</option>
						</select>
					</li>
					<li><label for="razon_social">Razón Social:</label></li>
					<li><input type="text" class="small" name="razon_social"/></li>
					<li><label for="nit" class="auto">Nit:</label></li>
					<li><input type="text" class="small" name="nit"/></li>
					<li><label for="codigo_acreditacion">Código de Acreditación:</label></li>
					<li><input type="text" name="codigo_acreditacion"/></li>
					<li><label for="ciudad" class="no-margin">Ciudad:</label></li>
					<li>
						<select name="ciudad">
							<option value="">---</option>
							<?php foreach ( $this->cities as $key => $city ): ?>
								<option value="<?php echo $city; ?>"><?php echo $city; ?></option>
							<?php endforeach; ?>
						</select>
					</li>
				</ul>

				<a class="boton-toggle-busqueda-avanzada" href="#">+ Búsqueda avanzada</a>

				<div class="wrapper-buscador-avanzado">
					<h3>Elija un tipo de organismo para mostrar los parámetros de búsqueda avanzada.</h3>
				</div>
				<div class="wrapp-submit">
					<a class="submit-search" href="#" data-page="0">
					<img src="<?php echo $component_images_url; ?>lupa.png"/> 
					<span>Buscar</span>
					</a>
				</div>
			</form>
		</div>

		<div class="wrapper-lista-organismos">
			<h2>RESULTADOS</h2>
			<ul class="paginator"></ul>
			<ul class="results-headers">
				<li>Razón Social</li>
				<li>Código Acreditación</li>
				<li>Dirección</li>
				<li>Ciudad</li>
				<li class="cell-small">PDF</li>
			</ul>
			<div class="wrapp-lista-content-organismos">
				<h3>Realice una búsqueda y visualice los resultados aquí.</h3>
			</div>
		</div>
	</div>
</div>