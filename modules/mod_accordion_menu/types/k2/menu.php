<?php 
/*------------------------------------------------------------------------
# mod_jo_accordion - Vertical Accordion Menu for Joomla 1.5 
# ------------------------------------------------------------------------
# author    Roland Soos 
# copyright Copyright (C) 2011 Offlajn.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.offlajn.com
-------------------------------------------------------------------------*/
?>
<?php
// no direct access
defined('_JEXEC') or die('Restricted access'); 
if(!defined('OfflajnK2Menu')) {
  define("OfflajnK2Menu", null);
  
  if(!is_dir(JPATH_SITE.DS.'components'.DS.'com_k2'.DS.'helpers')){
    echo JText::_("This component is not installed!");
    return;
  }
  
  require_once(dirname(__FILE__) . DS .'..'.DS.'..'.DS.'core'.DS.'MenuBase.php');
  require_once (JPATH_SITE.DS.'components'.DS.'com_k2'.DS.'helpers'.DS.'route.php');
  class OfflajnK2Menu extends OfflajnMenuBase{
    
    function OfflajnK2Menu($module, $params){
      parent::OfflajnMenuBase($module, $params);
    }
    
    function getAllItems(){
      $db = & JFactory::getDBO();
      $categoryid = explode("|", $this->_params->get('k2categoryid'));
      $query = "SELECT DISTINCT 
        id AS id, 
        name AS name,
        image as image, ";        
      if(!is_array($categoryid) && $categoryid != 0){
        $query.="IF(parent = ".$categoryid.", 0 , IF(parent = 0, -1, parent)) AS parent, ";
      }elseif(count($categoryid) && is_array($categoryid) && !in_array('0', $categoryid)){
        $query.="IF(id in (".implode(',', $categoryid)."), 0 , IF(parent = 0, -1, parent)) AS parent, ";
      }else{
        $query.="parent AS parent, ";
      }
      $query.="'cat' AS typ ";
      $query.= " FROM #__k2_categories
                WHERE published=1 ";       
      if ($this->_params->get('elementorder', 0) == 0)
        $query.="ORDER BY ordering ASC, name DESC";
      else if($this->_params->get('elementorder', 0)==1)
        $query.="ORDER BY name ASC";
      else if($this->_params->get('elementorder', 0)==2)
        $query.="ORDER BY name DESC";
        
      $db->setQuery($query);
      
      $allItems = $db->loadObjectList('id');
      
      if($this->_params->get('showcontents') == 1){
        $query = "
          SELECT concat(a.catid,'-',a.id) AS id, a.id AS id2,a.title AS name, a.catid AS parent, a.access, a.alias, 'con' AS typ, ";
          
          if($this->_params->get('displaynumprod', 0) != 0){
            $query.= "(SELECT COUNT(*) FROM #__k2_categories AS bp LEFT JOIN #__k2_items AS ax  ON ax.catid = bp.id WHERE bp.id=a.catid AND ax.published = 1 AND ax.trash = 0 ";
            $query.= ") AS productnum";
          }else{
            $query.= "0 AS productnum";
          }

            $query.=" FROM ".$db->nameQuote('#__k2_items')." AS a
            WHERE a.published = 1 AND
            a.trash = 0 ";
      if ($this->_params->get('elementorder', 0) == 0)
        $query.="ORDER BY a.ordering ASC, a.title DESC";
      else if($this->_params->get('elementorder', 0)==1)
        $query.="ORDER BY a.title ASC";
      else if($this->_params->get('elementorder', 0)==2)
        $query.="ORDER BY a.title DESC";
      
      $db->setQuery($query);
      $rows =  $db->loadObjectList('id');
      $cats = array();
      $unset = "";
      $keys = array_keys($rows);
      for ($x = 0; $x<count($keys);++$x) {
        $value = $rows[$keys[$x]];
        if (!isset($cats[$value->parent]))
          $cats[$value->parent] = 0;
        $cats[$value->parent]++;
        if ($cats[$value->parent]>$this->_params->get('maxitemsincat', 20))
          unset($rows[$keys[$x]]);
      }
      $allItems +=$rows;
      }
      return $allItems;
    }
    
    function getActiveItem(){ 
      $db = & JFactory::getDBO(); 
      $active = null;
      if(JRequest::getVar('option') == 'com_k2'){
        $content_id = 0;
        $category_id = 0; 
        if (JRequest::getVar('task') == "category") {
          $category_id = JRequest::getInt('id');        
        } elseif (JRequest::getVar('view')=="item") { 
          $content_id = JRequest::getInt('id');
          $query = "SELECT catid FROM ".$db->nameQuote('#__k2_items')." WHERE id=".$content_id;
          $db->setQuery($query);
          $category_id = $db->loadResult();      
        }        
        if($content_id > 0 && $this->_params->get('showcontents')){ 
          $active = new StdClass();         
          $active->id = $category_id."-".$content_id;          
        } elseif ($category_id>0) {
          $active = new StdClass();
          $active->id = $category_id;
        }
      }
      return $active;
    }
    
    function getItemsTree(){
      $items = $this->getItems();
      if($this->_params->get('displaynumprod', 0) == 2){
        for($i = count($items)-1; $i >= 0; $i--){
          if(!($items[$i]->typ == "con" && $items[$i-1]->typ=="con" && $items[$i]->parent==$items[$i-1]->parent)){
           	$items[$i]->parent->productnum+= $items[$i]->productnum;
          } 
        }
      }
      return $items;
    }
    
    function filterItem(&$item){
      $image = '';
      $item->nname = '<span>'.$item->name.'</span>';
      if ($this->_params->get('menu_images') && $item->image!='') {
        $image = '<img src="'.JUri::Root(false).'/media/k2/categories/'.$item->image.'" '.$imgalign.' />';

        
        switch ($this->_params->get('menu_images_align', 0)){
  				case 0 : 
    				$item->nname = $image.$item->nname;
    				break;
  				case 1 :
    				$item->nname = $item->nname.$image;
    				break;
  				default :
    				$item->nname = $image.$item->nname;
    				break;
  			}
  		}
      $length = "";
      if (strlen($item->productnum) == 1 ) {
        $length = "one";
      } elseif(strlen($item->productnum) >= 2) {
        $length = "more";
      }  		
      if($this->_params->get('displaynumprod', 0) == 1 && $item->typ == 'cat' && $item->productnum > 0){
        $item->nname.= '<span class="productnum '.$length.'">'.$item->productnum.'</span>'; 
      }elseif($this->_params->get('displaynumprod', 0) == 2 && $item->typ == 'cat'){
        $item->nname.= '<span class="productnum '.$length.'">'.$item->productnum.'</span>'; 
      }
            
      if($item->typ == 'cat'){
        if($this->_params->get('parentlink') == 0 && $item->p){
          $item->nname = '<a>'.$item->nname.'</a>';
        }else{
          $item->nname = '<a href="'.JRoute::_(K2HelperRoute::getCategoryRoute($item->id)).'">'.$item->nname.'</a>';
        }
      }elseif($item->typ == 'con') {
        $id = explode("-", $item->id);
        $item->nname = '<a href="'.JRoute::_(K2HelperRoute::getItemRoute($id[1], $id[0])).'">'.$item->nname.'</a>';
      }
    }
    
  }
}
?>